namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Material
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Material()
        {
            TaskMaterials = new HashSet<TaskMaterial>();
            BookmarkedMaterials = new HashSet<BookmarkedMaterial>();
            media_end_time = 0;
            media_start_time = 0;
            details = "";
        }

        public int id { get; set; }

        [Required]
        [StringLength(50)]
        public string type { get; set; }

        [StringLength(500)]
        public string title { get; set; }

        public string body { get; set; }

        public string source_link { get; set; }

        [StringLength(50)]
        public string cc_status { get; set; }

        public int media_start_time { get; set; }

        public int media_end_time { get; set; }

        public string attributes { get; set; }

        public string details { get; set; }

        public DateTime created_date { get; set; }

        public long created_by { get; set; }

        [NotMapped]
        public virtual string Creator { get; set; }

        public DateTime modified_date { get; set; }

        public long modified_by { get; set; }

        public bool is_active { get; set; }
        public bool being_used { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TaskMaterial> TaskMaterials { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BookmarkedMaterial> BookmarkedMaterials { get; set; }

        [NotMapped]
        public virtual bool IsBookmarked { get; set; }
    }
}
