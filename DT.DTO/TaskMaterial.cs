namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TaskMaterial
    {
        public int id { get; set; }

        public int task_id { get; set; }

        public int material_id { get; set; }
        [NotMapped]
        public virtual string title { get; set; }
        [NotMapped]
        public virtual string type { get; set; }

        public int index_order { get; set; }

        public int activation_time { get; set; }

        public string attributes { get; set; }

        public string details { get; set; }

        public DateTime created_date { get; set; }

        public long created_by { get; set; }

        public DateTime modified_date { get; set; }

        public long modified_by { get; set; }

        public bool is_active { get; set; }

        public virtual _Task Task { get; set; }
        public virtual Material Material { get; set; }

        [NotMapped]
        public virtual string TaskId { get; set; }
    }
}
