namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Sequence
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Sequence()
        {
            SequenceTasks = new HashSet<SequenceTask>();
            BookmarkedSequences = new HashSet<BookmarkedSequence>();
        }

        public int id { get; set; }

        [StringLength(250)]
        public string title { get; set; }

        public string attributes { get; set; }

        public string details { get; set; }

        public DateTime created_date { get; set; }
        [NotMapped]
        public virtual string Creator { get; set; }

        public long created_by { get; set; }

        public DateTime modified_date { get; set; }

        public long modified_by { get; set; }

        public bool is_active { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SequenceTask> SequenceTasks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BookmarkedSequence> BookmarkedSequences { get; set; }
        [NotMapped]
        public virtual bool IsBookmarked { get; set; }
        [NotMapped]
        public virtual int tasks_count { get; set; }
        [NotMapped]
        public virtual bool CanEdit { get; set; }
        public override string ToString()
        {
            return title;
        }
    }
}
