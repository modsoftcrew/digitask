﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DT.DTO
{
    public class Log
    {
        public long id { get; set; }
        public string type { get; set; }
        public string priority { get; set; }
        public string admin_notes { get; set; }
        public string session_info { get; set; }
        public string details { get; set; }
        public long created_by { get; set; }
        public DateTime created_date { get; set; }
        public bool is_active { get; set; }
    }
}
