namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ProficiencyLanguage
    {
        public int id { get; set; }

        public int user_id { get; set; }

        public int language_id { get; set; }

        [StringLength(2)]
        public string level { get; set; }

        public virtual Language Language { get; set; }

        public virtual User User { get; set; }
    }
}
