namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Rating
    {
        public int id { get; set; }

        public int task_id { get; set; }

        public int user_id { get; set; }

        [Column("rating")]
        public short point { get; set; }

        public string review { get; set; }

        public bool is_approved { get; set; }

        public virtual _Task Task { get; set; }

        public virtual User User { get; set; }
    }
}
