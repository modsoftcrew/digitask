namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Tag
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Tag()
        {
            TaskTags = new HashSet<TaskTag>();
        }

        public int id { get; set; }

        public string phrase { get; set; }

        
        public virtual ICollection<TaskTag> TaskTags { get; set; }
    }
}
