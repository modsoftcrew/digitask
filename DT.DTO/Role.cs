namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Role
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Role()
        {
            Permissions = new HashSet<Permission>();
            Users = new HashSet<User>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(250)]
        public string title { get; set; }

        public string attributes { get; set; }

        public string details { get; set; }

        public DateTime created_date { get; set; }

        public long created_by { get; set; }

        public DateTime modified_date { get; set; }

        public long modified_by { get; set; }

        public bool is_active { get; set; }

        
        public virtual ICollection<Permission> Permissions { get; set; }

        
        public virtual ICollection<User> Users { get; set; }

        public override string ToString()
        {
            return title;
        }
    }
}
