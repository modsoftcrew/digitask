namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Classes")]
    public partial class _Class
    {

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public _Class()
        {
            ClassStudents = new HashSet<ClassStudent>();
            Clusters = new HashSet<Cluster>();
            SessionTasks = new HashSet<SessionTask>();
        }

        public int id { get; set; }

        [StringLength(50)]
        public string title { get; set; }

        public int teacher_id { get; set; }

        public int max_student_limit { get; set; }

        public string attributes { get; set; }

        public string details { get; set; }

        public DateTime created_date { get; set; }

        public long created_by { get; set; }

        public DateTime modified_date { get; set; }

        public long modified_by { get; set; }

        public bool is_active { get; set; }

        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ClassStudent> ClassStudents { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cluster> Clusters { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SessionTask> SessionTasks { get; set; }

        [NotMapped]
        public virtual int[] students { get; set; }

        [NotMapped]
        public virtual int range { get; set; }

        public override string ToString()
        {
            return $"{title} ({LOCAL.Components.Labels.Capacity} : {max_student_limit})";
        }
    }
}
