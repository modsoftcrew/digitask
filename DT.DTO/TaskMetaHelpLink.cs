namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TaskMetaHelpLink
    {
        public int id { get; set; }

        public int meta_key { get; set; }

        [Required]
        [StringLength(2)]
        public string language { get; set; }

        public string title { get; set; }

        public string description { get; set; }

        public string help_url { get; set; }
    }
}
