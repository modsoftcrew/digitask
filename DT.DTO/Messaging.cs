namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Messaging
    {

        public int id { get; set; }
        public int session_id { get; set; }
        public int user_id { get; set; }
        public string body { get; set; }
        public DateTime datetime { get; set; }
        public virtual SessionTask SessionTask { get; set; }
        public virtual User User { get; set; }

        [NotMapped]
        public virtual string StrDate
        {
            get
            {
                DateTime now = DateTime.Now;
                if (datetime.Year == now.Year && datetime.Month == now.Month && datetime.Day == now.Day)
                    return datetime.ToString("HH:mm");
                else
                    return datetime.ToString("dd.MM.yyyy, HH:mm");

            }
        }
        [NotMapped]
        public virtual string FullDate
        {
            get
            {
                return datetime.ToLongDateString() + Environment.NewLine + datetime.ToLongTimeString();

            }
        }
        public override string ToString()
        {
            return body;
        }
    }
}
