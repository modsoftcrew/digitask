namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SessionTask
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SessionTask()
        {
            SessionTaskParticipants = new HashSet<SessionTaskParticipant>();
            status = nameof(LOCAL.Enums.SessionTaskStates.Created);
        }

        public int id { get; set; }

        public int task_id { get; set; }

        public string assigned_table { get; set; }

        public int? class_id { get; set; }

        public int? cluster_id { get; set; }

        public int? room_id { get; set; }

        public int? sequence_id { get; set; }

        public DateTime planned_start_time { get; set; }

        public DateTime? start_time { get; set; }

        public DateTime planned_end_time { get; set; }

        public DateTime? end_time { get; set; }

        public int? start_count { get; set; }

        public int? end_count { get; set; }

        public string conversations { get; set; }

        [StringLength(50)]
        public string status { get; set; }

        public string attributes { get; set; }

        public string details { get; set; }

        public DateTime created_date { get; set; }

        public long created_by { get; set; }

        public DateTime modified_date { get; set; }

        public long modified_by { get; set; }

        public bool is_active { get; set; }

        public virtual _Class Class { get; set; }

        public virtual Cluster Cluster { get; set; }

        public virtual Room Room { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SessionTaskParticipant> SessionTaskParticipants { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Messaging> Messagings { get; set; }

        public virtual _Task Task { get; set; }
    }
}
