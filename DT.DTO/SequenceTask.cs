namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SequenceTask
    {
        public SequenceTask()
        {
        }
        public SequenceTask(int task_id)
        {
            this.task_id = task_id;
        }
        public int id { get; set; }

        public int sequence_id { get; set; }

        public int task_id { get; set; }

        public int index_order { get; set; }

        public string attributes { get; set; }

        public string details { get; set; }

        public DateTime created_date { get; set; }

        public long created_by { get; set; }

        public DateTime modified_date { get; set; }

        public long modified_by { get; set; }

        public bool is_active { get; set; }

        public virtual Sequence Sequence { get; set; }

        public virtual _Task Task { get; set; }

        [NotMapped]
        public virtual string SequenceId { get; set; }
    }
}
