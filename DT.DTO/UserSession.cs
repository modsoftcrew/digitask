namespace DT.DTO
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("UserSessions")]
    public partial class UserSession
    {
        [Key]
        [Display(AutoGenerateField = false)]
        public int id { get; set; }
        public int user_id { get; set; }
        public DateTime login_time { get; set; }
        public DateTime logout_time { get; set; }
        public string configuration { get; set; }
        public string details { get; set; }
        public long created_by { get; set; }
        public DateTime created_date { get; set; }
    }
}
