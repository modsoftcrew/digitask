namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TaskResults")]
    public partial class TaskResult
    {
        public int id { get; set; }
        public int session_task_id { get; set; }
        [NotMapped]
        public virtual SessionTask SessionTask { get; set; }
        [NotMapped]
        public virtual _Task Task { get; set; }
        public long teacher_id { get; set; }
        [NotMapped]
        public virtual string  Teacher { get; set; }
        public long student_id { get; set; }
        [NotMapped]
        public virtual string Student { get; set; }
        public DateTime start_time { get; set; }
        [NotMapped]
        public virtual string StartDate { get; set; }
        public Nullable<DateTime> finish_time { get; set; }
        [NotMapped]
        public virtual string FinishDate { get; set; }
        public string completion_result { get; set; }
        public string attributes { get; set; }
        public string details { get; set; }
        public DateTime created_date { get; set; }
        public long created_by { get; set; }
        public DateTime modified_date { get; set; }
        public long modified_by { get; set; }
        public bool is_active { get; set; }
    }
}
