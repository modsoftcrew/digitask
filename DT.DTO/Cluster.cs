namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Cluster
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Cluster()
        {
            ClusterStudents = new HashSet<ClusterStudent>();
            SessionTasks = new HashSet<SessionTask>();
        }

        public int id { get; set; }

        [StringLength(50)]
        public string title { get; set; }

        public int class_id { get; set; }

        [StringLength(10)]
        public string color_code { get; set; }

        public string attributes { get; set; }

        public string details { get; set; }

        public DateTime created_date { get; set; }

        public long created_by { get; set; }

        public DateTime modified_date { get; set; }

        public long modified_by { get; set; }

        public bool is_active { get; set; }

        public virtual _Class Class { get; set; }



        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ClusterStudent> ClusterStudents { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SessionTask> SessionTasks { get; set; }
    }
}
