namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Room
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Room()
        {
            RoomStudents = new HashSet<RoomStudent>();
            SessionTasks = new HashSet<SessionTask>();
            current_range = 0;
        }

        public int id { get; set; }

        public string url { get; set; }

        public int group_range { get; set; }

        public int? current_range { get; set; }

        public DateTime? activation_date { get; set; }

        public DateTime? expiration_date { get; set; }

        public bool is_completed { get; set; }

        public string attributes { get; set; }

        public string details { get; set; }

        public DateTime created_date { get; set; }

        public long created_by { get; set; }

        public DateTime modified_date { get; set; }

        public long modified_by { get; set; }

        public bool is_active { get; set; }

        [NotMapped]
        public virtual int TaskId { get; set; }

        public virtual ICollection<RoomStudent> RoomStudents { get; set; }
        public virtual ICollection<SessionTask> SessionTasks { get; set; }
    }
}
