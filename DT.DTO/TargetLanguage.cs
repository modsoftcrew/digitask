namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TargetLanguage
    {
        public TargetLanguage()
        {

        }
        public TargetLanguage(int task_id, int language_id, string proficiency_level)
        {
            this.task_id = task_id;
            this.language_id = language_id;
            this.proficiency_level = proficiency_level;
        }

        public int id { get; set; }

        public int task_id { get; set; }

        public int language_id { get; set; }

        public string proficiency_level { get; set; }

        public virtual Language Language { get; set; }

        public virtual _Task Task { get; set; }
    }
}
