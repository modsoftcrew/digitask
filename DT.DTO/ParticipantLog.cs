namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ParticipantLog
    {
        public int id { get; set; }

        public int session_task_participant_id { get; set; }

        public DateTime login { get; set; }

        public DateTime logout { get; set; }

        [Required]
        public string user_agent { get; set; }

        [Required]
        [StringLength(50)]
        public string IP { get; set; }

        [Required]
        [StringLength(50)]
        public string MAC { get; set; }

        public virtual SessionTaskParticipant SessionTaskParticipant { get; set; }
    }
}
