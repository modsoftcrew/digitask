namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RawPages")]
    public partial class RawPage
    {
        public RawPage()
        {
            role_ids = "[]";
            roles = new List<int>();
        }
        public int id { get; set; }

        public string role_ids { get; set; }

        [NotMapped]
        public virtual List<int> RoleIds => Newtonsoft.Json.JsonConvert.DeserializeObject<List<int>>(role_ids);

        [NotMapped]
        public virtual List<int> roles { get; set; }

        public string name { get; set; }

        public string body { get; set; }

        public string attributes { get; set; }

        public string details { get; set; }

        public DateTime created_date { get; set; }
        [NotMapped]
        public string DateStr
        {
            get
            {
                if (created_date.Year == DateTime.Now.Year &&
                    created_date.Month == DateTime.Now.Month &&
                    created_date.Day == DateTime.Now.Day)
                    return created_date.ToString("HH:mm");
                else
                    return created_date.ToString("dd.MM.yyyy, HH:mm");
            }
        }

        public long created_by { get; set; }

        [NotMapped]
        public virtual User User { get; set; }

        public DateTime modified_date { get; set; }

        public long modified_by { get; set; }

        public bool is_active { get; set; }

        public override string ToString()
        {
            return name.ToStr();
        }
    }
}
