namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ClassStudent
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ClassStudent()
        {
            ClusterStudents = new HashSet<ClusterStudent>();
        }

        public int id { get; set; }

        public int class_id { get; set; }

        public int student_id { get; set; }

        public string attributes { get; set; }

        public string details { get; set; }

        public DateTime created_date { get; set; }

        public long created_by { get; set; }

        public DateTime modified_date { get; set; }

        public long modified_by { get; set; }

        public bool is_active { get; set; }

        public virtual _Class Class { get; set; }

        public virtual User User { get; set; }

        [NotMapped]
        public virtual string Student { 
            get
            {
                if(User != null)
                {
                    return $"{User.ToStr()} [{User.mail.ToStr()}]";
                }
                else
                {
                    return "-";
                }
            }
        }
        public virtual ICollection<ClusterStudent> ClusterStudents { get; set; }
    }
}
