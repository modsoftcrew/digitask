namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ClusterStudent
    {
        public int id { get; set; }

        public int cluster_id { get; set; }

        public int class_student_id { get; set; }

        public virtual ClassStudent ClassStudent { get; set; }

        public virtual Cluster Cluster { get; set; }
    }
}
