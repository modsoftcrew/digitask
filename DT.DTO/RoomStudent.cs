namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RoomStudent
    {
        public int id { get; set; }

        public int? room_id { get; set; }

        public int student_id { get; set; }

        public bool is_active { get; set; }
        public Nullable<DateTime> join_time { get; set; }
        public Nullable<DateTime> left_time { get; set; }

        [NotMapped]
        public virtual string JoinStr { get { return Convert.ToDateTime(join_time).ToString("dd.MM.yyyy, HH:mm"); } }

        public virtual Room Room { get; set; }

        public virtual User User { get; set; }
    }
}
