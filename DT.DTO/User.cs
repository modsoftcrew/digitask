namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Linq;

    public partial class User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            BookmarkedMaterials = new HashSet<BookmarkedMaterial>();
            BookmarkedTasks = new HashSet<BookmarkedTask>();
            BookmarkedSequences = new HashSet<BookmarkedSequence>();
            Classes = new HashSet<_Class>();
            ClassStudents = new HashSet<ClassStudent>();
            ProficiencyLanguages = new HashSet<ProficiencyLanguage>();
            Ratings = new HashSet<Rating>();
            RoomStudents = new HashSet<RoomStudent>();
            SessionTaskParticipants = new HashSet<SessionTaskParticipant>();
            TaskActions = new HashSet<TaskAction>();
            UserTaskMaterials = new List<UserTaskMaterial>();
            TaskEditors = new HashSet<TaskEditor>();
            Tasks = new HashSet<_Task>();
            UserSessions = new HashSet<UserSession>();
        }

        public int id { get; set; }

        public int role_id { get; set; }

        [Required]
        [StringLength(50)]
        public string name { get; set; }

        [Required]
        [StringLength(50)]
        public string lastname { get; set; }

        [Required]
        public string password { get; set; }

        [NotMapped]
        public virtual string confirm_password { get; set; }

        [Required]
        [StringLength(255)]
        public string mail { get; set; }

        [StringLength(2)]
        public string ui_language { get; set; }

        public string proficiency_level { get; set; }

        public Nullable<DateTime> birth { get; set; }

        public bool remember_me { get; set; }

        public bool is_allowed { get; set; }

        public bool is_confirmed { get; set; }

        public bool is_subscribed { get; set; }

        public DateTime confirmation_expiration { get; set; }

        public bool wanna_share_students { get; set; }

        public string institution { get; set; }

        [StringLength(250)]
        public string department { get; set; }

        [StringLength(250)]
        public string branch { get; set; }

        [StringLength(50)]
        public string title { get; set; }

        public bool is_profile_visible { get; set; }

        public string attributes { get; set; }

        public string details { get; set; }

        public DateTime created_date { get; set; }

        public long created_by { get; set; }

        public DateTime modified_date { get; set; }

        public long modified_by { get; set; }

        public bool is_active { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BookmarkedMaterial> BookmarkedMaterials { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BookmarkedSequence> BookmarkedSequences { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BookmarkedTask> BookmarkedTasks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<_Class> Classes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ClassStudent> ClassStudents { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProficiencyLanguage> ProficiencyLanguages { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rating> Ratings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SessionTaskParticipant> SessionTaskParticipants { get; set; }

        public virtual Role Role { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RoomStudent> RoomStudents { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TaskAction> TaskActions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TaskEditor> TaskEditors { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<_Task> Tasks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserSession> UserSessions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Messaging> Messagings { get; set; }

        [NotMapped]
        public virtual string ConnectionId { get; set; }

        [NotMapped]
        public virtual bool IsReady { get; set; }
        [NotMapped]
        public virtual bool HasAnswered { get; set; }
        [NotMapped]
        public virtual bool HasApprovedFinish { get; set; }
        [NotMapped]
        public virtual List<UserTaskMaterial> UserTaskMaterials { get; set; }        

        [NotMapped]
        public virtual int Age
        {
            get
            {
                if (birth == null)
                    return 0;
                else
                    return DateTime.Now.Year - Convert.ToDateTime(birth).Year;
            }
        }
        public override string ToString()
        {
            return $"{name.ToStr(LOCAL.Objects.Columns.name)} {lastname.ToStr(LOCAL.Objects.Columns.lastname)}";
        }
        public string SysStr()
        {
            return $"{this.ToStr("UNKNOWN USER")}[{id}]";
        }

        [NotMapped]
        public virtual string FullName
        {
            get
            {
                return $"{name.ToStr()} {lastname.ToStr()}";
            }
        }

        [NotMapped]
        public virtual string SelectLabel
        {
            get
            {
                return $"{name.ToStr()} {lastname.ToStr()} ({mail.ToStr()})";
            }
        }

    }

    public class UserTaskMaterial
    {
        public UserTaskMaterial(int activation, List<TaskMaterial> taskMaterials)
        {
            this.ActivationTime = activation;
            this.TaskMaterials = taskMaterials;
        }
        public int ActivationTime { get; set; }
        public List<TaskMaterial> TaskMaterials { get; set; }
    }
}
