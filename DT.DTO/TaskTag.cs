namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TaskTag
    {
        public int id { get; set; }

        public int task_id { get; set; }

        public int tag_id { get; set; }

        public virtual Tag Tag { get; set; }

        public virtual _Task Task { get; set; }
    }
}
