namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Language
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Language()
        {
            ProficiencyLanguages = new HashSet<ProficiencyLanguage>();
            TargetLanguages = new HashSet<TargetLanguage>();
        }

        public int id { get; set; }

        [StringLength(50)]
        public string name { get; set; }

        [StringLength(2)]
        public string iso_code { get; set; }

        [StringLength(10)]
        public string locales { get; set; }

        public bool rtl { get; set; }

        public bool has_ui { get; set; }

        public virtual ICollection<ProficiencyLanguage> ProficiencyLanguages { get; set; }

        
        public virtual ICollection<TargetLanguage> TargetLanguages { get; set; }

        public override string ToString()
        {
            return name; 
        }
    }
}
