﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DT.DTO
{
    internal static class DTOExtensions
    {
        public static string ToStr(this object obj, string defaultvalue = "-")
        {
            if (obj != null)
                return obj.ToString();
            else
                return defaultvalue;
        }
    }
}
