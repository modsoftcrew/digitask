﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DT.DTO
{
    [Table("Caches")]
    public class Cache
    {
        public Cache()
        {
            created_date = DateTime.Now;
            modified_date = DateTime.Now;
        }
        [Key]
        public int id { get; set; }
        public string key { get; set; }
        public string value { get; set; }


        public DateTime created_date { get; set; }

        public int created_by { get; set; }

        public DateTime modified_date { get; set; }

        public int modified_by { get; set; }

        public bool is_active { get; set; }
    }
}
