namespace DT.DTO
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Tasks")]
    public partial class _Task
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public _Task()
        {
            Ratings = new HashSet<Rating>();
            BookmarkedTasks = new HashSet<BookmarkedTask>();
            SequenceTasks = new HashSet<SequenceTask>();
            SessionTasks = new HashSet<SessionTask>();
            TargetLanguages = new HashSet<TargetLanguage>();
            TaskActions = new HashSet<TaskAction>();
            TaskEditors = new HashSet<TaskEditor>();
            TaskMaterials = new List<TaskMaterial>();
            TaskTags = new HashSet<TaskTag>();
            status = nameof(LOCAL.Objects.StatusType.Draft);
            expiration_date = new DateTime(2099, 12, 31);
            publish_date = DateTime.Now;
            target_min_age = 8;
            target_max_age = 20;
            is_open = true;
            is_active = true;
            can_preview = true;
            can_edit = true;
            co_designers = "[]";
            _CoDesigners = new List<int>();
            CoDesignerUsers = new List<User>();
        }

        public int id { get; set; }

        [Display(ResourceType = typeof(LOCAL.Objects.Columns), Name = nameof(LOCAL.Objects.Columns.type), Order = -1)]
        public string type { get; set; }

        [Display(ResourceType = typeof(LOCAL.Objects.Columns), Name = nameof(LOCAL.Objects.Columns.information_gap), Order = -1)]
        public string information_gap { get; set; }

        // Olu�turuken se�ilmeyecek, editlenirken se�ilebilir.
        [Display(ResourceType = typeof(LOCAL.Objects.Columns), Name = nameof(LOCAL.Objects.Columns.status), Order = -1)]
        public string status { get; set; }

        [Display(ResourceType = typeof(LOCAL.Objects.Columns), Name = nameof(LOCAL.Objects.Columns.task_title), Order = -1)]
        public string title { get; set; }

        // Kullan�c� taraf�ndan girilmeyecek, sistem generate edecek
        public string smartcode { get; set; }

        // Kullan�c� taraf�ndan girilmeyecek, sistem generate edecek
        public string qr_code { get; set; }

        // url
        public string task_instructions { get; set; }

        [Display(ResourceType = typeof(LOCAL.Objects.Columns), Name = nameof(LOCAL.Objects.Columns.can_preview), Order = -1)]
        public bool can_preview { get; set; }

        [Display(ResourceType = typeof(LOCAL.Objects.Columns), Name = nameof(LOCAL.Objects.Columns.can_edit), Order = -1)]
        public bool can_edit { get; set; }

        // Login olmu� user'�n idsi Girilmeyecek otomatik gelecek
        public int author_id { get; set; }

        [NotMapped]
        public virtual string Author { get; set; }

        public string co_designers { get; set; }

        [NotMapped]
        public virtual List<int> CoDesigners
        {
            get
            {
                if (co_designers == null || !co_designers.ToStr().Trim().StartsWith("["))
                    return new List<int>();
                return Newtonsoft.Json.JsonConvert.DeserializeObject<List<int>>(co_designers);
            }
        }

        [NotMapped]
        public virtual List<int> _CoDesigners { get; set; }
        [NotMapped]
        public virtual List<User> CoDesignerUsers { get; set; }


        public int original_id { get; set; }

        // Teacher'�n institution'� default gelecek, isterse de�i�terebilecek
        public string institution { get; set; }

        public string target_skill { get; set; }
        [NotMapped]
        public virtual string TargetSkill { get; set; }
        public string target_subject { get; set; }
        [NotMapped]
        public virtual string target_languages_str { get; set; }

        public int target_min_age { get; set; }

        public int target_max_age { get; set; }

        public string target_proficiency_level { get; set; }

        [NotMapped]
        public string[] target_proficiency_levels 
        {
            get
            {
                if (string.IsNullOrEmpty(target_proficiency_level) || string.IsNullOrWhiteSpace(target_proficiency_level))
                    return new string[0];
                else if (!target_proficiency_level.StartsWith("["))
                {
                    return new string[1] { target_proficiency_level };
                }
                else
                    return JsonConvert.DeserializeObject<string[]>(target_proficiency_level);
            }
            set
            {
                if (value == null)
                    target_proficiency_level = "[]";
                else
                    target_proficiency_level = JsonConvert.SerializeObject(value);
            }
        }

        // Dakika olarak girilecek
        public int estimated_duration { get; set; }

        // Taski ka� ki�i yapacak
        public int group_range { get; set; }

        // TextArea
        public string objectives { get; set; }
        // Textarea
        public string further_info { get; set; }

        public bool is_open { get; set; }
        public string answerers { get; set; }
        public bool is_collabrative_writing { get; set; }


        #region Completation Options
        /// <summary>
        /// Completation Options
        /// - Exact Value
        /// - Minimum Word
        /// - Maximum Word
        /// - Minimum And Maximum Word
        /// </summary>
        /// 
        public string completion_type { get; set; }
        public string exact_value { get; set; }

        public int minimum_word { get; set; }

        public int maximum_word { get; set; }

        public int time_relevant { get; set; }
        #endregion

        // E�er girmezse �ok ileri bir tarih ata �rnek : 01.01.2099
        public DateTime expiration_date { get; set; }

        // Girmezse, DateTime.Now ata ve status : nameof(LOCAL.Object.StatusType.Published)
        public DateTime publish_date { get; set; }
        [NotMapped]
        public virtual string PublishDate { get => publish_date.Year != 2099 ? publish_date.ToString("dd.MM.yyyy, HH:mm") : "-"; }

        public string attributes { get; set; }

        public string details { get; set; }

        public DateTime created_date { get; set; }

        [NotMapped]
        public virtual string CreatedDate { get => created_date.ToString("dd.MM.yyyy, HH:mm"); }

        // Create olurken User ID
        public long created_by { get; set; }

        [NotMapped]
        public virtual string Creator { get; set; }

        public DateTime modified_date { get; set; }

        // Create ve Modify olurken User ID
        public long modified_by { get; set; }

        public bool is_active { get; set; }
        public bool being_used { get; set; }

        [NotMapped]
        public virtual bool UserCanEdit { get; set; }


        public virtual ICollection<Rating> Ratings { get; set; }
        public virtual ICollection<SequenceTask> SequenceTasks { get; set; }
        public virtual ICollection<SessionTask> SessionTasks { get; set; }


        public virtual ICollection<TargetLanguage> TargetLanguages { get; set; }


        public virtual ICollection<TaskAction> TaskActions { get; set; }


        public virtual ICollection<TaskEditor> TaskEditors { get; set; }


        public virtual ICollection<TaskMaterial> TaskMaterials { get; set; }
        public virtual ICollection<BookmarkedTask> BookmarkedTasks { get; set; }

        public virtual User User { get; set; }


        public virtual ICollection<TaskTag> TaskTags { get; set; }

        [NotMapped]
        public virtual string tags { get; set; }

        [NotMapped]
        public virtual int[] target_languages { get; set; }

        [NotMapped]
        public virtual bool IsBookmarked { get; set; }

        [NotMapped]
        public virtual string Readable
        {
            get
            {
                TimeSpan duration = TimeSpan.FromSeconds(this.estimated_duration);
                return $"{title} ({LOCAL.Objects.Columns.estimated_duration_abbr} : {duration.ToString()})";
            }
        }

        public override string ToString()
        {
            return title;
        }
    }
}
