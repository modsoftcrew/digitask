﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DT.DTO
{
    public class DataAction
    {
        [Key]
        [Display(AutoGenerateField = false, Order = -1)]
        public long id { get; set; }
        public string title { get; set; }
        public string table_name { get; set; }
        public string table_id { get; set; }
        public string property { get; set; }
        public string old_value { get; set; }
        public string new_value { get; set; }
        public long created_by { get; set; }
        public DateTime created_date { get; set; }
    }
}
