namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Permission
    {
        public int id { get; set; }

        [StringLength(50)]
        public string key { get; set; }

        [StringLength(500)]
        public string desc { get; set; }

        public int role_id { get; set; }

        public virtual Role Role { get; set; }
    }
}
