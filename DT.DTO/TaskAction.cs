namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TaskAction
    {
        public int id { get; set; }

        public int task_id { get; set; }

        public int user_id { get; set; }

        [Required]
        [StringLength(50)]
        public string action { get; set; }

        public string attributes { get; set; }

        public string details { get; set; }

        public DateTime created_date { get; set; }

        public long created_by { get; set; }

        public DateTime modified_date { get; set; }

        public long modified_by { get; set; }

        public bool is_active { get; set; }

        public virtual _Task Task { get; set; }

        public virtual User User { get; set; }
    }
}
