namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Setting
    {
        public int id { get; set; }

        [Required]
        [StringLength(50)]
        public string key { get; set; }

        public string value { get; set; }
        public string details { get; set; }
    }
}
