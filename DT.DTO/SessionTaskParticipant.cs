namespace DT.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SessionTaskParticipant
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SessionTaskParticipant()
        {
            ParticipantLogs = new HashSet<ParticipantLog>();
            status = nameof(LOCAL.Enums.SessionTaskParticipantStates.Created);
            Materials = new List<TaskMaterial>();
        }

        public int id { get; set; }

        public int session_task_id { get; set; }

        public int participant_id { get; set; }

        [StringLength(50)]
        public string status { get; set; }

        public string attributes { get; set; }

        public string details { get; set; }

        public DateTime created_date { get; set; }

        public long created_by { get; set; }

        public DateTime modified_date { get; set; }

        public long modified_by { get; set; }

        public bool is_active { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ParticipantLog> ParticipantLogs { get; set; }

        public virtual SessionTask SessionTask { get; set; }

        public virtual User User { get; set; }

        [NotMapped]
        public virtual List<TaskMaterial> Materials { get; set; }
    }
}
