﻿
namespace DT.Modifier
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.informationgapDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.titleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.smartcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qrcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taskinstructionsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.canpreviewDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.caneditDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.authoridDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.authorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.originalidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.institutionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.targetskillDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.targetsubjectDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.targetlanguagesstrDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.targetminageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.targetmaxageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.targetproficiencylevelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estimateddurationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grouprangeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.objectivesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.furtherinfoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isopenDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.answerersDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iscollabrativewritingDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.completiontypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.exactvalueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.minimumwordDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maximumwordDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timerelevantDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.expirationdateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.publishdateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.publishDateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.attributesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.detailsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createddateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createdDateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createdbyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.creatorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modifieddateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modifiedbyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isactiveDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.beingusedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.userCanEditDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ratingsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sequenceTasksDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sessionTasksDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.targetLanguagesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taskActionsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taskEditorsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taskMaterialsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bookmarkedTasksDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taskTagsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tagsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ısBookmarkedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.readableDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taskBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.taskBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.typeDataGridViewTextBoxColumn,
            this.informationgapDataGridViewTextBoxColumn,
            this.statusDataGridViewTextBoxColumn,
            this.titleDataGridViewTextBoxColumn,
            this.smartcodeDataGridViewTextBoxColumn,
            this.qrcodeDataGridViewTextBoxColumn,
            this.taskinstructionsDataGridViewTextBoxColumn,
            this.canpreviewDataGridViewCheckBoxColumn,
            this.caneditDataGridViewCheckBoxColumn,
            this.authoridDataGridViewTextBoxColumn,
            this.authorDataGridViewTextBoxColumn,
            this.originalidDataGridViewTextBoxColumn,
            this.institutionDataGridViewTextBoxColumn,
            this.targetskillDataGridViewTextBoxColumn,
            this.targetsubjectDataGridViewTextBoxColumn,
            this.targetlanguagesstrDataGridViewTextBoxColumn,
            this.targetminageDataGridViewTextBoxColumn,
            this.targetmaxageDataGridViewTextBoxColumn,
            this.targetproficiencylevelDataGridViewTextBoxColumn,
            this.estimateddurationDataGridViewTextBoxColumn,
            this.grouprangeDataGridViewTextBoxColumn,
            this.objectivesDataGridViewTextBoxColumn,
            this.furtherinfoDataGridViewTextBoxColumn,
            this.isopenDataGridViewCheckBoxColumn,
            this.answerersDataGridViewTextBoxColumn,
            this.iscollabrativewritingDataGridViewCheckBoxColumn,
            this.completiontypeDataGridViewTextBoxColumn,
            this.exactvalueDataGridViewTextBoxColumn,
            this.minimumwordDataGridViewTextBoxColumn,
            this.maximumwordDataGridViewTextBoxColumn,
            this.timerelevantDataGridViewTextBoxColumn,
            this.expirationdateDataGridViewTextBoxColumn,
            this.publishdateDataGridViewTextBoxColumn,
            this.publishDateDataGridViewTextBoxColumn1,
            this.attributesDataGridViewTextBoxColumn,
            this.detailsDataGridViewTextBoxColumn,
            this.createddateDataGridViewTextBoxColumn,
            this.createdDateDataGridViewTextBoxColumn1,
            this.createdbyDataGridViewTextBoxColumn,
            this.creatorDataGridViewTextBoxColumn,
            this.modifieddateDataGridViewTextBoxColumn,
            this.modifiedbyDataGridViewTextBoxColumn,
            this.isactiveDataGridViewCheckBoxColumn,
            this.beingusedDataGridViewCheckBoxColumn,
            this.userCanEditDataGridViewCheckBoxColumn,
            this.ratingsDataGridViewTextBoxColumn,
            this.sequenceTasksDataGridViewTextBoxColumn,
            this.sessionTasksDataGridViewTextBoxColumn,
            this.targetLanguagesDataGridViewTextBoxColumn,
            this.taskActionsDataGridViewTextBoxColumn,
            this.taskEditorsDataGridViewTextBoxColumn,
            this.taskMaterialsDataGridViewTextBoxColumn,
            this.bookmarkedTasksDataGridViewTextBoxColumn,
            this.userDataGridViewTextBoxColumn,
            this.taskTagsDataGridViewTextBoxColumn,
            this.tagsDataGridViewTextBoxColumn,
            this.ısBookmarkedDataGridViewCheckBoxColumn,
            this.readableDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.taskBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(13, 13);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(775, 353);
            this.dataGridView1.TabIndex = 0;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // typeDataGridViewTextBoxColumn
            // 
            this.typeDataGridViewTextBoxColumn.DataPropertyName = "type";
            this.typeDataGridViewTextBoxColumn.HeaderText = "type";
            this.typeDataGridViewTextBoxColumn.Name = "typeDataGridViewTextBoxColumn";
            this.typeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // informationgapDataGridViewTextBoxColumn
            // 
            this.informationgapDataGridViewTextBoxColumn.DataPropertyName = "information_gap";
            this.informationgapDataGridViewTextBoxColumn.HeaderText = "information_gap";
            this.informationgapDataGridViewTextBoxColumn.Name = "informationgapDataGridViewTextBoxColumn";
            this.informationgapDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // statusDataGridViewTextBoxColumn
            // 
            this.statusDataGridViewTextBoxColumn.DataPropertyName = "status";
            this.statusDataGridViewTextBoxColumn.HeaderText = "status";
            this.statusDataGridViewTextBoxColumn.Name = "statusDataGridViewTextBoxColumn";
            this.statusDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // titleDataGridViewTextBoxColumn
            // 
            this.titleDataGridViewTextBoxColumn.DataPropertyName = "title";
            this.titleDataGridViewTextBoxColumn.HeaderText = "title";
            this.titleDataGridViewTextBoxColumn.Name = "titleDataGridViewTextBoxColumn";
            this.titleDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // smartcodeDataGridViewTextBoxColumn
            // 
            this.smartcodeDataGridViewTextBoxColumn.DataPropertyName = "smartcode";
            this.smartcodeDataGridViewTextBoxColumn.HeaderText = "smartcode";
            this.smartcodeDataGridViewTextBoxColumn.Name = "smartcodeDataGridViewTextBoxColumn";
            this.smartcodeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // qrcodeDataGridViewTextBoxColumn
            // 
            this.qrcodeDataGridViewTextBoxColumn.DataPropertyName = "qr_code";
            this.qrcodeDataGridViewTextBoxColumn.HeaderText = "qr_code";
            this.qrcodeDataGridViewTextBoxColumn.Name = "qrcodeDataGridViewTextBoxColumn";
            this.qrcodeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // taskinstructionsDataGridViewTextBoxColumn
            // 
            this.taskinstructionsDataGridViewTextBoxColumn.DataPropertyName = "task_instructions";
            this.taskinstructionsDataGridViewTextBoxColumn.HeaderText = "task_instructions";
            this.taskinstructionsDataGridViewTextBoxColumn.Name = "taskinstructionsDataGridViewTextBoxColumn";
            this.taskinstructionsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // canpreviewDataGridViewCheckBoxColumn
            // 
            this.canpreviewDataGridViewCheckBoxColumn.DataPropertyName = "can_preview";
            this.canpreviewDataGridViewCheckBoxColumn.HeaderText = "can_preview";
            this.canpreviewDataGridViewCheckBoxColumn.Name = "canpreviewDataGridViewCheckBoxColumn";
            this.canpreviewDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // caneditDataGridViewCheckBoxColumn
            // 
            this.caneditDataGridViewCheckBoxColumn.DataPropertyName = "can_edit";
            this.caneditDataGridViewCheckBoxColumn.HeaderText = "can_edit";
            this.caneditDataGridViewCheckBoxColumn.Name = "caneditDataGridViewCheckBoxColumn";
            this.caneditDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // authoridDataGridViewTextBoxColumn
            // 
            this.authoridDataGridViewTextBoxColumn.DataPropertyName = "author_id";
            this.authoridDataGridViewTextBoxColumn.HeaderText = "author_id";
            this.authoridDataGridViewTextBoxColumn.Name = "authoridDataGridViewTextBoxColumn";
            this.authoridDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // authorDataGridViewTextBoxColumn
            // 
            this.authorDataGridViewTextBoxColumn.DataPropertyName = "Author";
            this.authorDataGridViewTextBoxColumn.HeaderText = "Author";
            this.authorDataGridViewTextBoxColumn.Name = "authorDataGridViewTextBoxColumn";
            this.authorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // originalidDataGridViewTextBoxColumn
            // 
            this.originalidDataGridViewTextBoxColumn.DataPropertyName = "original_id";
            this.originalidDataGridViewTextBoxColumn.HeaderText = "original_id";
            this.originalidDataGridViewTextBoxColumn.Name = "originalidDataGridViewTextBoxColumn";
            this.originalidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // institutionDataGridViewTextBoxColumn
            // 
            this.institutionDataGridViewTextBoxColumn.DataPropertyName = "institution";
            this.institutionDataGridViewTextBoxColumn.HeaderText = "institution";
            this.institutionDataGridViewTextBoxColumn.Name = "institutionDataGridViewTextBoxColumn";
            this.institutionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // targetskillDataGridViewTextBoxColumn
            // 
            this.targetskillDataGridViewTextBoxColumn.DataPropertyName = "target_skill";
            this.targetskillDataGridViewTextBoxColumn.HeaderText = "target_skill";
            this.targetskillDataGridViewTextBoxColumn.Name = "targetskillDataGridViewTextBoxColumn";
            this.targetskillDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // targetsubjectDataGridViewTextBoxColumn
            // 
            this.targetsubjectDataGridViewTextBoxColumn.DataPropertyName = "target_subject";
            this.targetsubjectDataGridViewTextBoxColumn.HeaderText = "target_subject";
            this.targetsubjectDataGridViewTextBoxColumn.Name = "targetsubjectDataGridViewTextBoxColumn";
            this.targetsubjectDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // targetlanguagesstrDataGridViewTextBoxColumn
            // 
            this.targetlanguagesstrDataGridViewTextBoxColumn.DataPropertyName = "target_languages_str";
            this.targetlanguagesstrDataGridViewTextBoxColumn.HeaderText = "target_languages_str";
            this.targetlanguagesstrDataGridViewTextBoxColumn.Name = "targetlanguagesstrDataGridViewTextBoxColumn";
            this.targetlanguagesstrDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // targetminageDataGridViewTextBoxColumn
            // 
            this.targetminageDataGridViewTextBoxColumn.DataPropertyName = "target_min_age";
            this.targetminageDataGridViewTextBoxColumn.HeaderText = "target_min_age";
            this.targetminageDataGridViewTextBoxColumn.Name = "targetminageDataGridViewTextBoxColumn";
            this.targetminageDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // targetmaxageDataGridViewTextBoxColumn
            // 
            this.targetmaxageDataGridViewTextBoxColumn.DataPropertyName = "target_max_age";
            this.targetmaxageDataGridViewTextBoxColumn.HeaderText = "target_max_age";
            this.targetmaxageDataGridViewTextBoxColumn.Name = "targetmaxageDataGridViewTextBoxColumn";
            this.targetmaxageDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // targetproficiencylevelDataGridViewTextBoxColumn
            // 
            this.targetproficiencylevelDataGridViewTextBoxColumn.DataPropertyName = "target_proficiency_level";
            this.targetproficiencylevelDataGridViewTextBoxColumn.HeaderText = "target_proficiency_level";
            this.targetproficiencylevelDataGridViewTextBoxColumn.Name = "targetproficiencylevelDataGridViewTextBoxColumn";
            this.targetproficiencylevelDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // estimateddurationDataGridViewTextBoxColumn
            // 
            this.estimateddurationDataGridViewTextBoxColumn.DataPropertyName = "estimated_duration";
            this.estimateddurationDataGridViewTextBoxColumn.HeaderText = "estimated_duration";
            this.estimateddurationDataGridViewTextBoxColumn.Name = "estimateddurationDataGridViewTextBoxColumn";
            this.estimateddurationDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // grouprangeDataGridViewTextBoxColumn
            // 
            this.grouprangeDataGridViewTextBoxColumn.DataPropertyName = "group_range";
            this.grouprangeDataGridViewTextBoxColumn.HeaderText = "group_range";
            this.grouprangeDataGridViewTextBoxColumn.Name = "grouprangeDataGridViewTextBoxColumn";
            this.grouprangeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // objectivesDataGridViewTextBoxColumn
            // 
            this.objectivesDataGridViewTextBoxColumn.DataPropertyName = "objectives";
            this.objectivesDataGridViewTextBoxColumn.HeaderText = "objectives";
            this.objectivesDataGridViewTextBoxColumn.Name = "objectivesDataGridViewTextBoxColumn";
            this.objectivesDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // furtherinfoDataGridViewTextBoxColumn
            // 
            this.furtherinfoDataGridViewTextBoxColumn.DataPropertyName = "further_info";
            this.furtherinfoDataGridViewTextBoxColumn.HeaderText = "further_info";
            this.furtherinfoDataGridViewTextBoxColumn.Name = "furtherinfoDataGridViewTextBoxColumn";
            this.furtherinfoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // isopenDataGridViewCheckBoxColumn
            // 
            this.isopenDataGridViewCheckBoxColumn.DataPropertyName = "is_open";
            this.isopenDataGridViewCheckBoxColumn.HeaderText = "is_open";
            this.isopenDataGridViewCheckBoxColumn.Name = "isopenDataGridViewCheckBoxColumn";
            this.isopenDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // answerersDataGridViewTextBoxColumn
            // 
            this.answerersDataGridViewTextBoxColumn.DataPropertyName = "answerers";
            this.answerersDataGridViewTextBoxColumn.HeaderText = "answerers";
            this.answerersDataGridViewTextBoxColumn.Name = "answerersDataGridViewTextBoxColumn";
            this.answerersDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // iscollabrativewritingDataGridViewCheckBoxColumn
            // 
            this.iscollabrativewritingDataGridViewCheckBoxColumn.DataPropertyName = "is_collabrative_writing";
            this.iscollabrativewritingDataGridViewCheckBoxColumn.HeaderText = "is_collabrative_writing";
            this.iscollabrativewritingDataGridViewCheckBoxColumn.Name = "iscollabrativewritingDataGridViewCheckBoxColumn";
            this.iscollabrativewritingDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // completiontypeDataGridViewTextBoxColumn
            // 
            this.completiontypeDataGridViewTextBoxColumn.DataPropertyName = "completion_type";
            this.completiontypeDataGridViewTextBoxColumn.HeaderText = "completion_type";
            this.completiontypeDataGridViewTextBoxColumn.Name = "completiontypeDataGridViewTextBoxColumn";
            this.completiontypeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // exactvalueDataGridViewTextBoxColumn
            // 
            this.exactvalueDataGridViewTextBoxColumn.DataPropertyName = "exact_value";
            this.exactvalueDataGridViewTextBoxColumn.HeaderText = "exact_value";
            this.exactvalueDataGridViewTextBoxColumn.Name = "exactvalueDataGridViewTextBoxColumn";
            this.exactvalueDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // minimumwordDataGridViewTextBoxColumn
            // 
            this.minimumwordDataGridViewTextBoxColumn.DataPropertyName = "minimum_word";
            this.minimumwordDataGridViewTextBoxColumn.HeaderText = "minimum_word";
            this.minimumwordDataGridViewTextBoxColumn.Name = "minimumwordDataGridViewTextBoxColumn";
            this.minimumwordDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // maximumwordDataGridViewTextBoxColumn
            // 
            this.maximumwordDataGridViewTextBoxColumn.DataPropertyName = "maximum_word";
            this.maximumwordDataGridViewTextBoxColumn.HeaderText = "maximum_word";
            this.maximumwordDataGridViewTextBoxColumn.Name = "maximumwordDataGridViewTextBoxColumn";
            this.maximumwordDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // timerelevantDataGridViewTextBoxColumn
            // 
            this.timerelevantDataGridViewTextBoxColumn.DataPropertyName = "time_relevant";
            this.timerelevantDataGridViewTextBoxColumn.HeaderText = "time_relevant";
            this.timerelevantDataGridViewTextBoxColumn.Name = "timerelevantDataGridViewTextBoxColumn";
            this.timerelevantDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // expirationdateDataGridViewTextBoxColumn
            // 
            this.expirationdateDataGridViewTextBoxColumn.DataPropertyName = "expiration_date";
            this.expirationdateDataGridViewTextBoxColumn.HeaderText = "expiration_date";
            this.expirationdateDataGridViewTextBoxColumn.Name = "expirationdateDataGridViewTextBoxColumn";
            this.expirationdateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // publishdateDataGridViewTextBoxColumn
            // 
            this.publishdateDataGridViewTextBoxColumn.DataPropertyName = "publish_date";
            this.publishdateDataGridViewTextBoxColumn.HeaderText = "publish_date";
            this.publishdateDataGridViewTextBoxColumn.Name = "publishdateDataGridViewTextBoxColumn";
            this.publishdateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // publishDateDataGridViewTextBoxColumn1
            // 
            this.publishDateDataGridViewTextBoxColumn1.DataPropertyName = "PublishDate";
            this.publishDateDataGridViewTextBoxColumn1.HeaderText = "PublishDate";
            this.publishDateDataGridViewTextBoxColumn1.Name = "publishDateDataGridViewTextBoxColumn1";
            this.publishDateDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // attributesDataGridViewTextBoxColumn
            // 
            this.attributesDataGridViewTextBoxColumn.DataPropertyName = "attributes";
            this.attributesDataGridViewTextBoxColumn.HeaderText = "attributes";
            this.attributesDataGridViewTextBoxColumn.Name = "attributesDataGridViewTextBoxColumn";
            this.attributesDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // detailsDataGridViewTextBoxColumn
            // 
            this.detailsDataGridViewTextBoxColumn.DataPropertyName = "details";
            this.detailsDataGridViewTextBoxColumn.HeaderText = "details";
            this.detailsDataGridViewTextBoxColumn.Name = "detailsDataGridViewTextBoxColumn";
            this.detailsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // createddateDataGridViewTextBoxColumn
            // 
            this.createddateDataGridViewTextBoxColumn.DataPropertyName = "created_date";
            this.createddateDataGridViewTextBoxColumn.HeaderText = "created_date";
            this.createddateDataGridViewTextBoxColumn.Name = "createddateDataGridViewTextBoxColumn";
            this.createddateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // createdDateDataGridViewTextBoxColumn1
            // 
            this.createdDateDataGridViewTextBoxColumn1.DataPropertyName = "CreatedDate";
            this.createdDateDataGridViewTextBoxColumn1.HeaderText = "CreatedDate";
            this.createdDateDataGridViewTextBoxColumn1.Name = "createdDateDataGridViewTextBoxColumn1";
            this.createdDateDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // createdbyDataGridViewTextBoxColumn
            // 
            this.createdbyDataGridViewTextBoxColumn.DataPropertyName = "created_by";
            this.createdbyDataGridViewTextBoxColumn.HeaderText = "created_by";
            this.createdbyDataGridViewTextBoxColumn.Name = "createdbyDataGridViewTextBoxColumn";
            this.createdbyDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // creatorDataGridViewTextBoxColumn
            // 
            this.creatorDataGridViewTextBoxColumn.DataPropertyName = "Creator";
            this.creatorDataGridViewTextBoxColumn.HeaderText = "Creator";
            this.creatorDataGridViewTextBoxColumn.Name = "creatorDataGridViewTextBoxColumn";
            this.creatorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // modifieddateDataGridViewTextBoxColumn
            // 
            this.modifieddateDataGridViewTextBoxColumn.DataPropertyName = "modified_date";
            this.modifieddateDataGridViewTextBoxColumn.HeaderText = "modified_date";
            this.modifieddateDataGridViewTextBoxColumn.Name = "modifieddateDataGridViewTextBoxColumn";
            this.modifieddateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // modifiedbyDataGridViewTextBoxColumn
            // 
            this.modifiedbyDataGridViewTextBoxColumn.DataPropertyName = "modified_by";
            this.modifiedbyDataGridViewTextBoxColumn.HeaderText = "modified_by";
            this.modifiedbyDataGridViewTextBoxColumn.Name = "modifiedbyDataGridViewTextBoxColumn";
            this.modifiedbyDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // isactiveDataGridViewCheckBoxColumn
            // 
            this.isactiveDataGridViewCheckBoxColumn.DataPropertyName = "is_active";
            this.isactiveDataGridViewCheckBoxColumn.HeaderText = "is_active";
            this.isactiveDataGridViewCheckBoxColumn.Name = "isactiveDataGridViewCheckBoxColumn";
            this.isactiveDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // beingusedDataGridViewCheckBoxColumn
            // 
            this.beingusedDataGridViewCheckBoxColumn.DataPropertyName = "being_used";
            this.beingusedDataGridViewCheckBoxColumn.HeaderText = "being_used";
            this.beingusedDataGridViewCheckBoxColumn.Name = "beingusedDataGridViewCheckBoxColumn";
            this.beingusedDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // userCanEditDataGridViewCheckBoxColumn
            // 
            this.userCanEditDataGridViewCheckBoxColumn.DataPropertyName = "UserCanEdit";
            this.userCanEditDataGridViewCheckBoxColumn.HeaderText = "UserCanEdit";
            this.userCanEditDataGridViewCheckBoxColumn.Name = "userCanEditDataGridViewCheckBoxColumn";
            this.userCanEditDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // ratingsDataGridViewTextBoxColumn
            // 
            this.ratingsDataGridViewTextBoxColumn.DataPropertyName = "Ratings";
            this.ratingsDataGridViewTextBoxColumn.HeaderText = "Ratings";
            this.ratingsDataGridViewTextBoxColumn.Name = "ratingsDataGridViewTextBoxColumn";
            this.ratingsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sequenceTasksDataGridViewTextBoxColumn
            // 
            this.sequenceTasksDataGridViewTextBoxColumn.DataPropertyName = "SequenceTasks";
            this.sequenceTasksDataGridViewTextBoxColumn.HeaderText = "SequenceTasks";
            this.sequenceTasksDataGridViewTextBoxColumn.Name = "sequenceTasksDataGridViewTextBoxColumn";
            this.sequenceTasksDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sessionTasksDataGridViewTextBoxColumn
            // 
            this.sessionTasksDataGridViewTextBoxColumn.DataPropertyName = "SessionTasks";
            this.sessionTasksDataGridViewTextBoxColumn.HeaderText = "SessionTasks";
            this.sessionTasksDataGridViewTextBoxColumn.Name = "sessionTasksDataGridViewTextBoxColumn";
            this.sessionTasksDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // targetLanguagesDataGridViewTextBoxColumn
            // 
            this.targetLanguagesDataGridViewTextBoxColumn.DataPropertyName = "TargetLanguages";
            this.targetLanguagesDataGridViewTextBoxColumn.HeaderText = "TargetLanguages";
            this.targetLanguagesDataGridViewTextBoxColumn.Name = "targetLanguagesDataGridViewTextBoxColumn";
            this.targetLanguagesDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // taskActionsDataGridViewTextBoxColumn
            // 
            this.taskActionsDataGridViewTextBoxColumn.DataPropertyName = "TaskActions";
            this.taskActionsDataGridViewTextBoxColumn.HeaderText = "TaskActions";
            this.taskActionsDataGridViewTextBoxColumn.Name = "taskActionsDataGridViewTextBoxColumn";
            this.taskActionsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // taskEditorsDataGridViewTextBoxColumn
            // 
            this.taskEditorsDataGridViewTextBoxColumn.DataPropertyName = "TaskEditors";
            this.taskEditorsDataGridViewTextBoxColumn.HeaderText = "TaskEditors";
            this.taskEditorsDataGridViewTextBoxColumn.Name = "taskEditorsDataGridViewTextBoxColumn";
            this.taskEditorsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // taskMaterialsDataGridViewTextBoxColumn
            // 
            this.taskMaterialsDataGridViewTextBoxColumn.DataPropertyName = "TaskMaterials";
            this.taskMaterialsDataGridViewTextBoxColumn.HeaderText = "TaskMaterials";
            this.taskMaterialsDataGridViewTextBoxColumn.Name = "taskMaterialsDataGridViewTextBoxColumn";
            this.taskMaterialsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bookmarkedTasksDataGridViewTextBoxColumn
            // 
            this.bookmarkedTasksDataGridViewTextBoxColumn.DataPropertyName = "BookmarkedTasks";
            this.bookmarkedTasksDataGridViewTextBoxColumn.HeaderText = "BookmarkedTasks";
            this.bookmarkedTasksDataGridViewTextBoxColumn.Name = "bookmarkedTasksDataGridViewTextBoxColumn";
            this.bookmarkedTasksDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // userDataGridViewTextBoxColumn
            // 
            this.userDataGridViewTextBoxColumn.DataPropertyName = "User";
            this.userDataGridViewTextBoxColumn.HeaderText = "User";
            this.userDataGridViewTextBoxColumn.Name = "userDataGridViewTextBoxColumn";
            this.userDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // taskTagsDataGridViewTextBoxColumn
            // 
            this.taskTagsDataGridViewTextBoxColumn.DataPropertyName = "TaskTags";
            this.taskTagsDataGridViewTextBoxColumn.HeaderText = "TaskTags";
            this.taskTagsDataGridViewTextBoxColumn.Name = "taskTagsDataGridViewTextBoxColumn";
            this.taskTagsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tagsDataGridViewTextBoxColumn
            // 
            this.tagsDataGridViewTextBoxColumn.DataPropertyName = "tags";
            this.tagsDataGridViewTextBoxColumn.HeaderText = "tags";
            this.tagsDataGridViewTextBoxColumn.Name = "tagsDataGridViewTextBoxColumn";
            this.tagsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ısBookmarkedDataGridViewCheckBoxColumn
            // 
            this.ısBookmarkedDataGridViewCheckBoxColumn.DataPropertyName = "IsBookmarked";
            this.ısBookmarkedDataGridViewCheckBoxColumn.HeaderText = "IsBookmarked";
            this.ısBookmarkedDataGridViewCheckBoxColumn.Name = "ısBookmarkedDataGridViewCheckBoxColumn";
            this.ısBookmarkedDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // readableDataGridViewTextBoxColumn
            // 
            this.readableDataGridViewTextBoxColumn.DataPropertyName = "Readable";
            this.readableDataGridViewTextBoxColumn.HeaderText = "Readable";
            this.readableDataGridViewTextBoxColumn.Name = "readableDataGridViewTextBoxColumn";
            this.readableDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // taskBindingSource
            // 
            this.taskBindingSource.DataSource = typeof(DT.DTO._Task);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(635, 376);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(152, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Düzenle";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(13, 376);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(616, 23);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 2;
            this.progressBar1.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 411);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.taskBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn informationgapDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn smartcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn qrcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn taskinstructionsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn canpreviewDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn caneditDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn authoridDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn authorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn originalidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn institutionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn targetskillDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn targetsubjectDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn targetlanguagesstrDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn targetminageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn targetmaxageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn targetproficiencylevelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn estimateddurationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn grouprangeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn objectivesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn furtherinfoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isopenDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn answerersDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn iscollabrativewritingDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn completiontypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn exactvalueDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn minimumwordDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maximumwordDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn timerelevantDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn expirationdateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn publishdateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn publishDateDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn attributesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn detailsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn createddateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn createdDateDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn createdbyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn creatorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modifieddateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modifiedbyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isactiveDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn beingusedDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn userCanEditDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ratingsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sequenceTasksDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sessionTasksDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn targetLanguagesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn taskActionsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn taskEditorsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn taskMaterialsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bookmarkedTasksDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn userDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn taskTagsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tagsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ısBookmarkedDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn readableDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource taskBindingSource;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}

