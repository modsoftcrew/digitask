﻿using DT.DAL;
using DT.DTO;
using MS.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DT.Modifier
{
    public partial class Form1 : Form
    {
        List<_Task> Tasks = new List<_Task>();
        public Form1()
        {
            InitializeComponent();
        }
        protected async override void OnLoad(EventArgs e)
        {
            progressBar1.Visible = true;
               await Task.Run(() =>
            {
                using (Repository<_Task> Repository = new Repository<_Task>())
                {
                    Tasks = Repository.SelectByAll().ToList();
                }
            });
            dataGridView1.DataSource = Tasks;
            progressBar1.Visible = false;
        }
        private async void button1_Click(object sender, EventArgs e)
        {

            progressBar1.Visible = true;
            await Task.Run(() =>
            {
                foreach (_Task task in Tasks.Where(x=>x.exact_value != null))
                {
                    if (!task.exact_value.StartsWith("["))
                    {
                        var values = task.exact_value.Split(',');
                        List<TagifyValueModel> tags = new List<TagifyValueModel>();
                        foreach (string value in values)
                        {
                            tags.Add(new TagifyValueModel { value = value });
                        }

                        using (Repository<_Task> Repository = new Repository<_Task>())
                        {
                            _Task updated = Repository.SingleSelectByQuery(t => t.id == task.id);
                            updated.exact_value = tags.ToJson();
                            updated = Repository.Updated(updated);
                        }
                    }
                }
                using (Repository<_Task> Repository = new Repository<_Task>())
                {
                    Tasks = Repository.SelectByAll().ToList();
                }
            });
            dataGridView1.DataSource = Tasks;
            progressBar1.Visible = false;
        }
    }
    public class TagifyValueModel
    {
        public string value { get; set; }
    }
}
