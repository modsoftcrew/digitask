﻿namespace DT.DAL
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using DT.DTO;
    using Newtonsoft.Json;
    using global::MS.DAL;
    using global::MS.BLL;
    using System.Data.SqlClient;
    using System.Web;

    public partial class Ctx : DataContext
    {
        public long user_id { get; set; }
        //public Ctx() : base("name=qcs")
        //{
        //    this.user_id = Credentials.Session.CurrentUser != null ? ((User)Credentials.Session.CurrentUser).id : 1;
        //    Configuration.LazyLoadingEnabled = false;
        //    Configuration.AutoDetectChangesEnabled = false;
        //    Configuration.ProxyCreationEnabled = false;
        //}
        public Ctx(long user_id = -1,bool ll=false) : base(Credentials.ServerCredentials().ToConnectionString("db"), ll)
        {
            if (user_id != -1)
                this.user_id = user_id;
            else
                this.user_id = Credentials.Session.CurrentUser != null ? ((User)Credentials.Session.CurrentUser).id : 1;
            Configuration.AutoDetectChangesEnabled = false;
            Configuration.ProxyCreationEnabled = true;
        }
        public static int Execute(string query)
        {
            using (SqlConnection connection = new SqlConnection(
               Credentials.ServerCredentials().ToConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query,connection);
                int result = command.ExecuteNonQuery();
                connection.Close();
                return result;
            }
        }

        public virtual DbSet<BookmarkedMaterial> BookmarkedMaterials { get; set; }
        public virtual DbSet<BookmarkedSequence> BookmarkedSequences { get; set; }
        public virtual DbSet<BookmarkedTask> BookmarkedTasks { get; set; }
        public virtual DbSet<Cache> Caches { get; set; }
        public virtual DbSet<_Class> Classes { get; set; }
        public virtual DbSet<ClassStudent> ClassStudents { get; set; }
        public virtual DbSet<Cluster> Clusters { get; set; }
        public virtual DbSet<ClusterStudent> ClusterStudents { get; set; }
        public virtual DbSet<DataAction> DataActions { get; set; }
        public virtual DbSet<Language> Languages { get; set; }
        public virtual DbSet<Log> Logs { get; set; }
        public virtual DbSet<Material> Materials { get; set; }
        public virtual DbSet<Messaging> Messagings { get; set; }
        public virtual DbSet<ParticipantLog> ParticipantLogs { get; set; }
        public virtual DbSet<Permission> Permissions { get; set; }
        public virtual DbSet<ProficiencyLanguage> ProficiencyLanguages { get; set; }
        public virtual DbSet<Rating> Ratings { get; set; }
        public virtual DbSet<RawPage> RawPages { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Room> Rooms { get; set; }
        public virtual DbSet<RoomStudent> RoomStudents { get; set; }
        public virtual DbSet<Sequence> Sequences { get; set; }
        public virtual DbSet<SequenceTask> SequenceTasks { get; set; }
        public virtual DbSet<UserSession> Sessions { get; set; }
        public virtual DbSet<SessionTaskParticipant> SessionTaskParticipants { get; set; }
        public virtual DbSet<SessionTask> SessionTasks { get; set; }
        public virtual DbSet<Setting> Settings { get; set; }
        public virtual DbSet<Tag> Tags { get; set; }
        public virtual DbSet<TargetLanguage> TargetLanguages { get; set; }
        public virtual DbSet<TaskAction> TaskActions { get; set; }
        public virtual DbSet<TaskEditor> TaskEditors { get; set; }
        public virtual DbSet<TaskMaterial> TaskMaterials { get; set; }
        public virtual DbSet<TaskMetaHelpLink> TaskMetaHelpLinks { get; set; }
        public virtual DbSet<_Task> Tasks { get; set; }
        public virtual DbSet<TaskResult> TaskResults { get; set; }
        public virtual DbSet<TaskTag> TaskTags { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<_Class>()
                .HasMany(e => e.ClassStudents)
                .WithRequired(e => e.Class)
                .HasForeignKey(e => e.class_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<_Class>()
                .HasMany(e => e.Clusters)
                .WithRequired(e => e.Class)
                .HasForeignKey(e => e.class_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<_Class>()
                .HasMany(e => e.SessionTasks)
                .WithOptional(e => e.Class)
                .HasForeignKey(e => e.class_id);

            modelBuilder.Entity<ClassStudent>()
                .HasMany(e => e.ClusterStudents)
                .WithRequired(e => e.ClassStudent)
                .HasForeignKey(e => e.class_student_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cluster>()
                .HasMany(e => e.ClusterStudents)
                .WithRequired(e => e.Cluster)
                .HasForeignKey(e => e.cluster_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cluster>()
                .HasMany(e => e.SessionTasks)
                .WithOptional(e => e.Cluster)
                .HasForeignKey(e => e.cluster_id);

            modelBuilder.Entity<Language>()
                .HasMany(e => e.ProficiencyLanguages)
                .WithRequired(e => e.Language)
                .HasForeignKey(e => e.language_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Language>()
                .HasMany(e => e.TargetLanguages)
                .WithRequired(e => e.Language)
                .HasForeignKey(e => e.language_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Material>()
                .HasMany(e => e.BookmarkedMaterials)
                .WithRequired(e => e.Material)
                .HasForeignKey(e => e.material_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Material>()
                .HasMany(e => e.TaskMaterials)
                .WithRequired(e => e.Material)
                .HasForeignKey(e => e.material_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.Permissions)
                .WithRequired(e => e.Role)
                .HasForeignKey(e => e.role_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.Role)
                .HasForeignKey(e => e.role_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Room>()
                .HasMany(e => e.RoomStudents)
                .WithOptional(e => e.Room)
                .HasForeignKey(e => e.room_id);

            modelBuilder.Entity<Room>()
                .HasMany(e => e.SessionTasks)
                .WithOptional(e => e.Room)
                .HasForeignKey(e => e.room_id);

            modelBuilder.Entity<Sequence>()
                .HasMany(e => e.BookmarkedSequences)
                .WithRequired(e => e.Sequence)
                .HasForeignKey(e => e.sequence_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Sequence>()
                .HasMany(e => e.SequenceTasks)
                .WithRequired(e => e.Sequence)
                .HasForeignKey(e => e.sequence_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SessionTask>()
                .HasMany(e => e.Messagings)
                .WithRequired(e => e.SessionTask)
                .HasForeignKey(e => e.session_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SessionTaskParticipant>()
                .HasMany(e => e.ParticipantLogs)
                .WithRequired(e => e.SessionTaskParticipant)
                .HasForeignKey(e => e.session_task_participant_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SessionTask>()
                .HasMany(e => e.SessionTaskParticipants)
                .WithRequired(e => e.SessionTask)
                .HasForeignKey(e => e.session_task_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Tag>()
                .HasMany(e => e.TaskTags)
                .WithRequired(e => e.Tag)
                .HasForeignKey(e => e.tag_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<_Task>()
                .HasMany(e => e.BookmarkedTasks)
                .WithRequired(e => e.Task)
                .HasForeignKey(e => e.task_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<_Task>()
                .HasMany(e => e.Ratings)
                .WithRequired(e => e.Task)
                .HasForeignKey(e => e.task_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<_Task>()
                .HasMany(e => e.SequenceTasks)
                .WithRequired(e => e.Task)
                .HasForeignKey(e => e.task_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<_Task>()
                .HasMany(e => e.SessionTasks)
                .WithRequired(e => e.Task)
                .HasForeignKey(e => e.task_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<_Task>()
                .HasMany(e => e.TargetLanguages)
                .WithRequired(e => e.Task)
                .HasForeignKey(e => e.task_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<_Task>()
                .HasMany(e => e.TaskActions)
                .WithRequired(e => e.Task)
                .HasForeignKey(e => e.task_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<_Task>()
                .HasMany(e => e.TaskEditors)
                .WithRequired(e => e.Task)
                .HasForeignKey(e => e.task_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<_Task>()
                .HasMany(e => e.TaskMaterials)
                .WithRequired(e => e.Task)
                .HasForeignKey(e => e.task_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<_Task>()
                .HasMany(e => e.TaskTags)
                .WithRequired(e => e.Task)
                .HasForeignKey(e => e.task_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BookmarkedMaterials)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BookmarkedSequences)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BookmarkedTasks)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Classes)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.teacher_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ClassStudents)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.student_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ProficiencyLanguages)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Ratings)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.RoomStudents)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.student_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.SessionTaskParticipants)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.participant_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.TaskActions)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.TaskEditors)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.editor_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Tasks)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.author_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Messagings)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.session_id)
                .WillCascadeOnDelete(false);
        }

        public void Create()
        {
            this.Database.CreateIfNotExists();
            this.Database.ExecuteSqlCommand(Scripts.Functions);
            this.Database.ExecuteSqlCommand(Scripts.StoredProcedures);
            this.Database.ExecuteSqlCommand(Scripts.Views);
        }


        public override int SaveChanges()
        {
            try
            {
                if (user_id != 0)
                {
                    var modifiedEntities = ChangeTracker.Entries().ToList();

                    foreach (var change in modifiedEntities)
                    {
                        var entityName = change.Entity.GetType().Name.Split('_')[0];
                        var primaryKey = change.Entity.GetType().GetProperty("id").GetValue(change.Entity);

                        if (change.State != EntityState.Added)
                        {
                            if (change.State != EntityState.Deleted)
                            {
                                foreach (var prop in change.CurrentValues.PropertyNames)
                                {
                                    if (prop == "modified_date" || prop == "created_date" || prop == "created_by" || prop == "modified_by")
                                        continue;
                                    try
                                    {
                                        var originalValue = change.State == EntityState.Added ? "-" : (change.OriginalValues[prop] == null ? "-" : change.OriginalValues[prop].ToString());


                                        var currentValue = change.CurrentValues[prop] == null ? "-" : change.CurrentValues[prop].ToString();
                                        if (originalValue != currentValue)
                                        {
                                            DataAction action = new DataAction();
                                            action.title = change.State.ToString();
                                            action.table_name = entityName;
                                            action.table_id = primaryKey.ToString();
                                            action.property = prop;
                                            action.old_value = originalValue;
                                            action.new_value = currentValue;
                                            action.created_by = user_id;
                                            action.created_date = Zaman.Simdi;
                                            DataActions.Add(action);
                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }
                            }
                            else
                            {
                                DataAction action = new DataAction();
                                action.title = change.State.ToString();
                                action.table_name = entityName;
                                action.table_id = primaryKey.ToString();
                                action.property = "-";
                                action.old_value = change.Entity.ToJson();
                                action.new_value = "-";
                                action.created_by = user_id;
                                action.created_date = Zaman.Simdi;
                                DataActions.Add(action);
                            }
                        }
                        else
                        {
                            Dictionary<string, object> values = new Dictionary<string, object>();
                            foreach (var prop in change.CurrentValues.PropertyNames)
                            {
                                var currentValue = change.CurrentValues[prop] == null ? "-" : change.CurrentValues[prop].ToString();
                                values.Add(prop, currentValue);
                            }

                            try
                            {
                                if (entityName.Equals("User"))
                                {
                                    string mail = values["mail"].ToStr().ToLower().Trim();
                                    var user = Users.FirstOrDefault(x => x.mail.ToLower().Trim().Equals(mail));
                                    if (user != null)
                                    {
                                        Logs.Add(new Log
                                        {
                                            admin_notes = "Existing User",
                                            details = values.ToJson(),
                                            priority = Logger.getPriorityString(LogPriority.Important),
                                            type = Logger.getTypeString(LogType.UserMistake),
                                            created_by = user_id,
                                            created_date = DateTime.Now,
                                            is_active = true,
                                            session_info = HttpUtility.HtmlEncode(StaticDB.ThisSession.Current.ToJson())
                                        });
                                        return 0;
                                    }
                                }
                            }
                            catch (Exception)
                            {

                            }
                            base.SaveChanges();
                            DataAction action = new DataAction();
                            action.title = "Added";
                            action.table_name = entityName;
                            action.table_id = "-";
                            action.property = "-";
                            action.old_value = "-";
                            action.new_value = values.ToJson();
                            action.created_by = user_id;
                            action.created_date = Zaman.Simdi;
                            DataActions.Add(action);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Save("Save Changes Error", ex);
            }
            return base.SaveChanges();
        }
    }
}
