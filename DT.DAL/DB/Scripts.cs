﻿using System;
using System.Data.Entity;
using System.Linq;

namespace DT.DAL
{
    public static class Scripts
    {
        public static void Generate(this DbContext Ctx)
        {
            try
            {
                Ctx.Database.ExecuteSqlCommand(Functions);
            }
            catch (Exception e)
            {

            }
            try
            {
                Ctx.Database.ExecuteSqlCommand(StoredProcedures);
            }
            catch (Exception e)
            {

            }
            try
            {
                Ctx.Database.ExecuteSqlCommand(Views);
            }
            catch (Exception e)
            {

            }
        }
        public static string Functions
        {
            get
            {
                string query = string.Empty;
                return query;
            }
        }
        public static string Views
        {
            get
            {
                string query = string.Empty;
                return query;
            }
        }
        public static string StoredProcedures
        {
            get
            {
                string query = string.Empty;
                return query;
            }
        }

        public static string CreateTable
        {
            get
            {
                return "CREATE TABLE {{table_name}}([id] [bigint] IDENTITY(1,1) NOT NULL, {{columns}} CONSTRAINT [PK_dbo.QCS_{{table_name}}] PRIMARY KEY CLUSTERED ([id] ASC))";
            }
        }
        public static string InsertQuery
        {
            get
            {
                return "INSERT INTO {{TABLE}} ({{COLUMNS}}) VALUES ({{INSERT_VALUES}}); " + Environment.NewLine;
            }
        }
        public static string AddOrUpdate
        {
            get
            {
                return "IF exists(SELECT * from {{TABLE}} where (1=0)) " +     
	                        "BEGIN " +
                                "UPDATE {{TABLE}} SET {{UPDATE_VALUES}} where {{CONDITION}} " +
	                        "END "+            
                        "else "+
                            "BEGIN " +
                                "INSERT INTO {{TABLE}} ({{COLUMNS}}) VALUES ({{INSERT_VALUES}}) " +
                            "END " + Environment.NewLine;
            }
        }
    }
}
