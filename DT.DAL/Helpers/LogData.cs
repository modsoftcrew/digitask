﻿using System;

namespace DT.DAL
{
    public class LogData
    {
        public LogData(Exception error, string priority, string type, string message)
        {
            Error = error;
            Priority = priority;
            Type = type;
            Message = message;
        }

        public Exception Error { get; set; }
        public string Priority { get; set; }
        public string Type { get; set; }
        public string Message { get; set; }
    }
}
