﻿using System;
using System.Linq;

namespace DT.DAL
{
    public enum LogPriority
    {
        Standard,
        NotClassified,
        LessImportant,
        Important,
        HighImportant,
        VeryHighImportant
    }
    public enum LogType
    {
        ConnectionProblem,
        CRUD,
        DBMistake,
        ApplicationMistake,
        UserMistake,
        AlgorithmMistake,
        UXMistake,
        UIMistake,
        OutsourceFileMistake,
        NetworkMistake,
        MissingLibrary,
        SettingsMistake,
        UnauthorizedRequest,
        Unknown,
        None
    }
}
