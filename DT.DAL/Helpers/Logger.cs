﻿using MS.BLL;
using DT.DAL;
using DT.DTO;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Web;

namespace DT.DAL
{
    public static class Logger
    {
        public static LogData LastCreated;

        /// <summary>
        /// Logların kaydedilmesine yarar.
        /// </summary>
        /// <param name="e">Exception değişkeni</param>
        /// <param name="priority">Hata önceliği : 0-Az, 1-Normal, 2-Çok, 3-En çok Önemli</param>
        /// <param name="type">Hata türün : 0-Kullanıcı, 1-Kod, 2-UX, 3-UI, 4-Bilimez Hatası</param>
        /// <param name="adminNotes">Yazılımcı Yorumu</param>
        public static async void Add(Exception e, LogPriority priority = LogPriority.Standard, LogType type = LogType.None, string adminNotes = "")
        {
            Credentials.Session.ProcessEndTime = DateTime.Now;
            await CreateLog(e, priority, type, adminNotes);
            LastCreated = new LogData(e, getPriorityString(priority), getTypeString(type), adminNotes);
        }
        public static void Save(string adminNotes, Exception e)
        {
            try
            {
                Log Log = new Log();
                Log.type = LogPriority.Standard.ToString();
                Log.priority = LogType.None.ToString();
                Log.admin_notes = HttpUtility.HtmlEncode(adminNotes);
                Log.session_info = HttpUtility.HtmlEncode(StaticDB.ThisSession.Current.ToJson());
                Log.details = e != null ? HttpUtility.HtmlEncode(e.ToJson()) : null;
                Log.created_date = Zaman.Simdi;
                Log.is_active = true;
                if (Credentials.Session.CurrentUser != null)
                    Log.created_by = ((User)Credentials.Session.CurrentUser).id;
                string query =
                        "INSERT INTO Logs " +
                        "           ([type] " +
                        "           ,[priority] " +
                        "           ,[admin_notes] " +
                        "           ,[session_info] " +
                        "           ,[details] " +
                        "           ,[created_by] " +
                        "           ,[created_date] " +
                        "           ,[is_active]) " +
                        "     VALUES " +
                        $"           ('{Log.type}' " +
                        $"           ,'{Log.priority}' " +
                        $"           ,'{Log.admin_notes}' " +
                        $"           ,'{Log.session_info}' " +
                        $"           ,'{Log.details}' " +
                        $"           ,{Log.created_by} " +
                        $"           ,'{Log.created_date.ToString("yyyy-MM-dd HH:mm:ss.fff")}' " +
                        $"           ,1) ";
                using (Ctx Ctx = new Ctx())
                {
                    Ctx.Database.ExecuteSqlCommand(query);
                }
            }
            catch (Exception)
            {

            }
        }
        /// <summary>
        /// Logların kaydedilmesine yarar.
        /// </summary>
        /// <param name="priority">Hata önceliği : 0-Az, 1-Normal, 2-Çok, 3-En çok Önemli</param>
        /// <param name="type">Hata türün : 0-Kullanıcı, 1-Kod, 2-UX, 3-UI, 4-Bilimez Hatası</param>
        /// <param name="adminNotes">Yazılımcı Yorumu</param>
        public static async void Add(string adminNotes, LogPriority priority = LogPriority.Standard, LogType type = LogType.None)
        {
            Credentials.Session.ProcessEndTime = DateTime.Now;
            await CreateLog(null, priority, type, adminNotes);
            LastCreated = new LogData(null, getPriorityString(priority), getTypeString(type), adminNotes);
        }
        public enum Action
        {
            INSERT,
            UPDATE,
            DELETE,
            SOFTDELETE
        }
        public static void RepositoryAction(object entity, Action action)
        {
            string strEntity = entity.GetType().Name;
            if (strEntity.Equals("Session"))
                return;
            else if (strEntity.Equals("DataAction"))
                return;
            else if (strEntity.Equals("Log"))
                return;
            Add($"{entity.GetType().Name} {action.ToString()}", LogPriority.Standard, LogType.None);
        }

        private static async Task CreateLog(Exception e, LogPriority priorty, LogType type, string adminNotes)
        {
            try
            {

                Log Log = new Log();
                Log.type = type.ToString();
                Log.priority = priorty.ToString();
                Log.admin_notes = HttpUtility.HtmlEncode(adminNotes);
                Log.session_info = HttpUtility.HtmlEncode(StaticDB.ThisSession.Current.ToJson());
                Log.details = e != null ? HttpUtility.HtmlEncode(e.ToJson()) : null;
                Log.created_date = Zaman.Simdi;
                Log.is_active = true;
                if (Credentials.Session.CurrentUser != null)
                    Log.created_by = ((User)Credentials.Session.CurrentUser).id;

                if (Utilities.ConnectionState != System.Data.ConnectionState.Open)
                {
                    await WriteToFile(Log);
                    return;
                }
                string query =
                        "INSERT INTO Logs " +
                        "           ([type] " +
                        "           ,[priority] " +
                        "           ,[admin_notes] " +
                        "           ,[session_info] " +
                        "           ,[details] " +
                        "           ,[created_by] " +
                        "           ,[created_date] " +
                        "           ,[is_active]) " +
                        "     VALUES " +
                        $"           ('{Log.type}' " +
                        $"           ,'{Log.priority}' " +
                        $"           ,'{Log.admin_notes}' " +
                        $"           ,'{Log.session_info}' " +
                        $"           ,'{Log.details}' " +
                        $"           ,{Log.created_by} " +
                        $"           ,'{Log.created_date.ToString("yyyy-MM-dd HH:mm:ss.fff")}' " +
                        $"           ,1) ";
                try
                {
                    await Task.Run(() =>
                            {
                                using (Ctx Ctx = new Ctx())
                                {
                                    Ctx.Database.ExecuteSqlCommand(query);
                                }
                            });
                }
                catch (Exception)
                {
                    await WriteToFile(Log);
                }
            }
            catch (Exception ex)
            {

            }
        }
        public static void Write(string msg)
        {
            string folderPath = Path.Combine(System.IO.Path
                .GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)
                + "\\Logs\\");
            Directory.CreateDirectory(folderPath);
            using (var sw = File.AppendText(Path.Combine(folderPath, "log_" + DateTime.Now.ToString("yyyyMMdd_HHmm") + ".txt")))
            {
                string log = $"{Zaman.Simdi.ToString("yyyy-MM-dd HH:mm:ss,FFF")}\t[{HttpContext.Current.Request.UserHostName}]\t{msg}";
                sw.WriteLine(log);
            }
        }
        public static void Write(string msg,string folderPath)
        {
            try
            {
                Directory.CreateDirectory(folderPath);
                using (var sw = File.AppendText(Path.Combine(folderPath, "log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt")))
                {
                    string log = $"{Zaman.Simdi.ToString("yyyy-MM-dd HH:mm:ss,FFF")}\t[{HttpContext.Current.Request.UserHostName}]\t{msg}";
                    sw.WriteLine(log);
                }
            }
            catch (Exception)
            {

            }
        }
        public static async Task WriteToFile(object log)
        {
            string details = $"{Zaman.Simdi.ToString("yyyy-MM-dd HH:mm:ss,FFF")}\t[{HttpContext.Current.Request.UserHostName}]\n{log.ToJson()}";
            string folderPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\DMSLogs\\");
            Directory.CreateDirectory(folderPath);
            using (var sw = new StreamWriter(Path.Combine(folderPath, "log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".json")))
            {
                await sw.WriteAsync(details);
            }
        }
        public static async Task WriteToFile(LogData log)
        {
            string details = $"{Zaman.Simdi.ToString("yyyy-MM-dd HH:mm:ss,FFF")}\t[{HttpContext.Current.Request.UserHostName}]\n{log.ToJson()}";
            string folderPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\DMSLogs\\");
            Directory.CreateDirectory(folderPath);
            using (var sw = new StreamWriter(Path.Combine(folderPath, "log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".json")))
            {
                await sw.WriteAsync(details);
            }
        }
        public static string getPriorityString(LogPriority priority)
        {
            switch (priority)
            {
                case LogPriority.Standard:
                    return "Standard";
                case LogPriority.NotClassified:
                    return "Not Classified";
                case LogPriority.LessImportant:
                    return "Less Important";
                case LogPriority.Important:
                    return "Important";
                case LogPriority.HighImportant:
                    return "High Important";
                case LogPriority.VeryHighImportant:
                    return "Very High Important";
                default:
                    return priority.ToString();
            }
        }

        public static string getTypeString(LogType type)
        {
            switch (type)
            {
                case LogType.DBMistake:
                    return "DB Mistake";
                case LogType.ApplicationMistake:
                    return "Application Mistake";
                case LogType.UserMistake:
                    return "User Mistake";
                case LogType.AlgorithmMistake:
                    return "Algorithm Mistake";
                case LogType.UXMistake:
                    return "UX Mistake";
                case LogType.UIMistake:
                    return "UI Mistake";
                case LogType.OutsourceFileMistake:
                    return "Outsource File Mistake";
                case LogType.NetworkMistake:
                    return "Network Mistake";
                case LogType.Unknown:
                    return "Unknown";
                case LogType.None:
                    return "None";
                default:
                    return type.ToString();
            }
        }
    }
}
