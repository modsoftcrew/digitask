﻿using DT.DAL;
using DT.DTO;
using MS.BLL;
using Newtonsoft.Json;
using System;
using System.Web;

namespace DT
{
    public static class Cacher
    {
        public static User CurrentUser
        {
            get
            {
                HttpCookie cookie = HttpContext.Current.Request.Cookies["DT_User"];
                if (cookie != null)
                    return JsonConvert.DeserializeObject<User>(HttpUtility.UrlDecode(cookie.Value));
                return null;
            }
        }
        public static void Save(string key, object entity)
        {
            using (Repository<Cache> Repository = new Repository<Cache>())
            {
                try
                {
                    Cache cache = Repository.SingleSelectByQuery(c => c.key == key && c.created_by == CurrentUser.id);
                    if (cache != null)
                    {
                        cache.value = entity.ToJson();
                        cache.modified_by = CurrentUser.id;
                        Repository.Updated(cache);
                    }
                    else
                    {
                        cache = new Cache();
                        cache.key = key;
                        cache.created_by = cache.modified_by = CurrentUser.id;
                        cache.value = entity.ToJson();
                        Repository.Inserted(cache);
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }
        public static T Get<T>(string key)
        {
            try
            {
                using (Repository<Cache> Repository = new Repository<Cache>())
                {
                    Cache cache = Repository.SingleSelectByQuery(c => c.key == key && c.created_by == CurrentUser.id);
                    if (cache != null)
                    {
                        return JsonConvert.DeserializeObject<T>(cache.value);
                    }
                    else
                    {
                        return default(T);
                    }
                }
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }
        public static void SetStr(string key, string value)
        {
            using (Repository<Cache> Repository = new Repository<Cache>())
            {
                try
                {
                    Cache cache = Repository.SingleSelectByQuery(c => c.key == key && c.created_by == CurrentUser.id);
                    if (cache != null)
                    {
                        cache.value = value;
                        cache.modified_by = CurrentUser.id;
                        Repository.Updated(cache);
                    }
                    else
                    {
                        cache = new Cache();
                        cache.key = key;
                        cache.created_by = cache.modified_by = CurrentUser.id;
                        cache.value = value;
                        Repository.Inserted(cache);
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }
        public static string GetStr(string key)
        {
            try
            {
                using (Repository<Cache> Repository = new Repository<Cache>())
                {
                    Cache cache = Repository.SingleSelectByQuery(c => c.key == key && c.created_by == CurrentUser.id);
                    if (cache != null)
                    {
                        return cache.value;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public static void Delete(params string[] keys)
        {
            string query = "";
            using (Ctx ctx = new Ctx())
            {
                foreach (string key in keys)
                {
                    query += $"DELETE FROM Caches WHERE [key]='{key}' AND created_by={CurrentUser.id};" + Environment.NewLine;
                }
                ctx.Database.ExecuteSqlCommand(query);
            }
        }
    }
}
