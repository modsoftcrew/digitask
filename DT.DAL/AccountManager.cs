﻿using MS.BLL;
using DT.DAL;
using DT.DTO;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DT.DAL
{
    public class AccountManager
    {
        public enum State
        {
            Success,
            InvalidUsername,
            InvalidPassword,
            InvalidMail,
            UserNotConfirmed,
            UserNotAllowed,
            ConnectionBroken,
            ServerError,
            PasswordSent,
            PasswordNotSent,
            UserNotFound
        }
        public static async Task<State> Login(string username, string password)
        {
            if (Utilities.ConnectionState == System.Data.ConnectionState.Closed)
                return State.ServerError;
            if (Utilities.ConnectionState == System.Data.ConnectionState.Broken)
                return State.ConnectionBroken;
            if (string.IsNullOrEmpty(username))
                return State.InvalidUsername;
            if (string.IsNullOrEmpty(password))
                return State.InvalidPassword;
            return await Task.Run(() =>
            {
                using (Ctx ctx = new Ctx())
                {
                    try
                    {
                        var users = ctx.Users.AsEnumerable();
                        if ((users = users.Where(u => u.mail.Equals(username))).Count() == 0)
                            return State.InvalidUsername;
                        else if ((users = users.Where(u => u.password.Equals(password))).Count() == 0)
                            return State.InvalidPassword;
                        else if ((users = users.Where(u => u.is_allowed)).Count() == 0)
                            return State.UserNotAllowed;
                        else if (users.Count() <= 0)
                            return State.UserNotFound;
                        else if ((Credentials.Session.CurrentUser = users.First()) == null)
                            return State.UserNotFound;
                        else
                        {
                            User user = users.First();
                            user.Role = ctx.Roles.Find(user.role_id);
                            Credentials.Session.CurrentUser = user;
                            return State.Success;
                        }
                    }
                    catch (Exception ex)
                    {
                        Credentials.Session.Exception = ex;
                        return State.ServerError;
                    }
                }
            });
        }
        public static async Task<State> SendPassword(string mail = null)
        {
            if (mail == null)
                return State.InvalidMail;
            return await Task.Run(() =>
            {
                using (Repository<User> Repository = new Repository<User>())
                {
                    try
                    {
                        var users = Repository.SelectByAll();
                        if ((users = users.Where(u => u.mail.Equals(mail))).Count() == 0)
                            return State.InvalidMail;
                        else if ((users = users.Where(u => u.is_allowed)).Count() == 0)
                            return State.UserNotAllowed;
                        else if (users.Count() <= 0)
                            return State.UserNotFound;
                        else if ((Credentials.Session.CurrentUser = users.First()) == null)
                            return State.UserNotFound;
                        else
                            return State.Success;
                    }
                    catch (Exception ex)
                    {
                        Credentials.Session.Exception = ex;
                        return State.ServerError;
                    }
                }
            });
        }
    }
}
