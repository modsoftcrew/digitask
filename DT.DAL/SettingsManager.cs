﻿using DT.DAL;
using DT.DTO;
using System;
using System.Linq;

namespace DT.DAL
{
    public static class SettingsManager
    {
        public static string Get(string key)
        {
            try
            {
                if (StaticDB.Settings == null)
                    StaticDB.SetProperty("Setting");
                if (StaticDB.Settings.Count > 0)
                {
                    Setting setting = StaticDB.Settings.FirstOrDefault(s => s.key == key);
                    if (setting == null)
                        return "";
                    return setting.value;
                }
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                Logger.Add(ex);
                return string.Empty;
            }
        }

        public static void Set(string key, string value, string details = "")
        {
            Setting setting = StaticDB.Settings.FirstOrDefault(s => s.key == key);
            using (Repository<Setting> Repository = new Repository<Setting>())
            {
                if (setting == null)
                {
                    setting = new Setting();
                    setting.key = key;
                    setting.value = value;
                    setting.details = details;
                }
                else
                {
                    setting.value = value;
                    setting.details = !string.IsNullOrEmpty(setting.details) ? setting.details : details;
                }
            }
        }

        public static void Delete(string key)
        {
            using (Repository<Setting> Repository = new Repository<Setting>())
            {
                Setting setting = Repository.SingleSelectByQuery(s => s.key == key);
                if (setting != null)
                    Repository.Deleted(setting);
            }
        }
    }
}