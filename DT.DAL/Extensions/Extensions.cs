﻿using DT.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DT.DAL
{
    public static class Extensions
    {
        public static Language GetByIso(this List<Language> languages, string iso)
        {
            Language language = StaticDB.Languages.FirstOrDefault(x => x.iso_code.Equals(iso));
            if (language == null)
                return StaticDB.Languages[0];
            else
                return language;
        }
    }
}
