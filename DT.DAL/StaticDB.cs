﻿using MS.BLL;
using DT.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DT.DAL
{
    public static class StaticDB
    {
        public static class ThisSession
        {
            public static UserSession Current { get; set; }
            public static User User { get; set; }
            public static List<Role> UserRoles { get; set; }
            public static List<Permission> Permissions { get; set; }
            public static Exception Exception { get; set; }
        }
        public static class WebSession
        {
            public static Dictionary<long, UserSession> Current { get; set; }
            public static Dictionary<long, List<Permission>> Permissions { get; set; }
            public static Dictionary<long, List<Role>> UserRoles { get; set; }
        }

        private static List<BookmarkedMaterial> _BookmarkedMaterials { get; set; }
        private static List<BookmarkedTask> _BookmarkedTasks { get; set; }
        private static List<BookmarkedSequence> _BookmarkedSequences { get; set; }
        private static List<_Class> _Classes { get; set; }
        private static List<ClassStudent> _ClassStudents { get; set; }
        private static List<Cluster> _Clusters { get; set; }
        private static List<ClusterStudent> _ClusterStudents { get; set; }
        private static List<Language> _Languages { get; set; }
        private static List<Material> _Materials { get; set; }
        private static List<Material> _DeletedMaterials { get; set; }
        private static List<Permission> _Permissions { get; set; }
        private static List<ProficiencyLanguage> _ProficiencyLanguages { get; set; }
        private static List<RawPage> _RawPages { get; set; }
        private static List<Rating> _Ratings { get; set; }
        private static List<Role> _Roles { get; set; }
        private static List<Room> _Rooms { get; set; }
        private static List<RoomStudent> _RoomStudents { get; set; }
        private static List<Sequence> _Sequences { get; set; }
        private static List<SequenceTask> _SequenceTasks { get; set; }
        private static List<SessionTask> _SessionTasks { get; set; }
        private static List<Setting> _Settings { get; set; }
        private static List<Tag> _Tags { get; set; }
        private static List<TargetLanguage> _TargetLanguages { get; set; }
        private static List<TaskAction> _TaskActions { get; set; }
        private static List<TaskEditor> _TaskEditors { get; set; }
        private static List<TaskMaterial> _TaskMaterials { get; set; }
        private static List<TaskMetaHelpLink> _TaskMetaHelpLinks { get; set; }
        private static List<_Task> _Tasks { get; set; }
        private static List<_Task> _DeletedTasks { get; set; }
        private static List<TaskTag> _TaskTags { get; set; }
        private static List<User> _Users { get; set; }

        public static List<BookmarkedMaterial> BookmarkedMaterials { get { if (_BookmarkedMaterials == null) { SetProperty(nameof(BookmarkedMaterial)); } return _BookmarkedMaterials; } set { _BookmarkedMaterials = value; } }

        public static List<BookmarkedTask> BookmarkedTasks { get { if (_BookmarkedTasks == null) { SetProperty(nameof(BookmarkedTask)); } return _BookmarkedTasks; } set { _BookmarkedTasks = value; } }

        public static List<BookmarkedSequence> BookmarkedSequences { get { if (_BookmarkedSequences == null) { SetProperty(nameof(BookmarkedSequence)); } return _BookmarkedSequences; } set { _BookmarkedSequences = value; } }

        public static List<_Class> Classes { get { if (_Classes == null) { SetProperty(nameof(_Class)); } return _Classes; } set { _Classes = value; } }
        public static List<ClassStudent> ClassStudents { get { if (_ClassStudents == null) { SetProperty(nameof(ClassStudent)); } return _ClassStudents; } set { _ClassStudents = value; } }
        public static List<Cluster> Clusters { get { if (_Clusters == null) { SetProperty(nameof(Cluster)); } return _Clusters; } set { _Clusters = value; } }
        public static List<ClusterStudent> ClusterStudents { get { if (_ClusterStudents == null) { SetProperty(nameof(ClusterStudent)); } return _ClusterStudents; } set { _ClusterStudents = value; } }
        public static List<Language> Languages { get { if (_Languages == null) { SetProperty(nameof(Language)); } return _Languages; } set { _Languages = value; } }
        public static List<Material> AllMaterials { get { if (_Materials == null || _DeletedMaterials == null) { SetProperty(nameof(Material)); } var list = new List<Material>(); list.AddRange(_Materials); list.AddRange(_DeletedMaterials); return list; } }
        public static List<Material> Materials { get { if (_Materials == null) { SetProperty(nameof(Material)); } return _Materials; } set { _Materials = value; } }
        public static List<Material> DeletedMaterials { get { if (_DeletedMaterials == null) { SetProperty(nameof(Material)); } return _DeletedMaterials; } set { _DeletedMaterials = value; } }
        public static List<Permission> Permissions { get { if (_Permissions == null) { SetProperty(nameof(Permission)); } return _Permissions; } set { _Permissions = value; } }
        public static List<ProficiencyLanguage> ProficiencyLanguages { get { if (_ProficiencyLanguages == null) { SetProperty(nameof(ProficiencyLanguage)); } return _ProficiencyLanguages; } set { _ProficiencyLanguages = value; } }
        public static List<Rating> Ratings { get { if (_Ratings == null) { SetProperty(nameof(Rating)); } return _Ratings; } set { _Ratings = value; } }
        public static List<RawPage> RawPages { get { if (_RawPages == null) { SetProperty(nameof(RawPage)); } return _RawPages; } set { _RawPages = value; } }
        public static List<Role> Roles { get { if (_Roles == null) { SetProperty(nameof(Role)); } return _Roles; } set { _Roles = value; } }
        public static List<Room> Rooms { get { if (_Rooms == null) { SetProperty(nameof(Room)); } return _Rooms; } set { _Rooms = value; } }
        public static List<RoomStudent> RoomStudents { get { if (_RoomStudents == null) { SetProperty(nameof(RoomStudent)); } return _RoomStudents; } set { _RoomStudents = value; } }
        public static List<Sequence> Sequences { get { if (_Sequences == null) { SetProperty(nameof(Sequence)); } return _Sequences; } set { _Sequences = value; } }
        public static List<SequenceTask> SequenceTasks { get { if (_SequenceTasks == null) { SetProperty(nameof(SequenceTask)); } return _SequenceTasks; } set { _SequenceTasks = value; } }
        public static List<SessionTask> SessionTasks { get { if (_SessionTasks == null) { SetProperty(nameof(SessionTask)); } return _SessionTasks; } set { _SessionTasks = value; } }
        public static List<Setting> Settings { get { if (_Settings == null) { SetProperty(nameof(Setting)); } return _Settings; } set { _Settings = value; } }
        
        public static List<Tag> Tags { get { if (_Tags == null) { SetProperty(nameof(Tag)); } return _Tags; } set { _Tags = value; } }
        public static List<TargetLanguage> TargetLanguages { get { if (_TargetLanguages == null) { SetProperty(nameof(TargetLanguage)); } return _TargetLanguages; } set { _TargetLanguages = value; } }
        public static List<TaskAction> TaskActions { get { if (_TaskActions == null) { SetProperty(nameof(TaskAction)); } return _TaskActions; } set { _TaskActions = value; } }
        public static List<TaskEditor> TaskEditors { get { if (_TaskEditors == null) { SetProperty(nameof(TaskEditor)); } return _TaskEditors; } set { _TaskEditors = value; } }
        public static List<TaskMaterial> TaskMaterials { get { if (_TaskMaterials == null) { SetProperty(nameof(TaskMaterial)); } return _TaskMaterials; } set { _TaskMaterials = value; } }
        public static List<TaskMetaHelpLink> TaskMetaHelpLinks { get { if (_TaskMetaHelpLinks == null) { SetProperty(nameof(TaskMetaHelpLink)); } return _TaskMetaHelpLinks; } set { _TaskMetaHelpLinks = value; } }
        public static List<_Task> AllTasks { get { if (_Tasks == null || _DeletedTasks == null) { SetProperty(nameof(_Task)); } var list = new List<_Task>(); list.AddRange(_Tasks);list.AddRange(_DeletedTasks);return list; } }
        public static List<_Task> Tasks { get { if (_Tasks == null) { SetProperty(nameof(_Task)); } return _Tasks; } set { _Tasks = value; } }
        public static List<_Task> DeletedTasks { get { if (_DeletedTasks == null) { SetProperty(nameof(_Task)); } return _DeletedTasks; } set { _DeletedTasks = value; } }
        public static List<TaskTag> TaskTags { get { if (_TaskTags == null) { SetProperty(nameof(TaskTag)); } return _TaskTags; } set { _TaskTags = value; } }
        public static List<User> Users { get { if (_Users == null) { SetProperty(nameof(User)); } return _Users; } set { _Users = value; } }

        public static void SetProperty(string type)
        {
            try
            {
                switch (type)
                {
                    case nameof(BookmarkedMaterial):
                        using (Repository<BookmarkedMaterial> Repository = new Repository<BookmarkedMaterial>() )
                        {
                            BookmarkedMaterials = Repository.MultiSelectByQuery(x => x.is_active).ToList();
                        }
                        break;
                    case nameof(BookmarkedTask):
                        using (Repository<BookmarkedTask> Repository = new Repository<BookmarkedTask>())
                        {
                            BookmarkedTasks = Repository.MultiSelectByQuery(x => x.is_active).ToList();
                        }
                        break;
                    case nameof(BookmarkedSequence):
                        using (Repository<BookmarkedSequence> Repository = new Repository<BookmarkedSequence>())
                        {
                            BookmarkedSequences = Repository.MultiSelectByQuery(x => x.is_active).ToList();
                        }
                        break;
                    case nameof(_Class):
                        using (Repository<_Class> Repository = new Repository<_Class>() )
                        {
                            Classes = Repository.MultiSelectByQuery(x => x.is_active).ToList();
                        }
                        break;
                    case nameof(Cluster):
                        using (Repository<Cluster> Repository = new Repository<Cluster>())
                        {
                            Clusters = Repository.MultiSelectByQuery(x => x.is_active).ToList();
                        }
                        break;
                    case nameof(ClusterStudent):
                        using (Repository<ClusterStudent> Repository = new Repository<ClusterStudent>())
                        {
                            ClusterStudents = Repository.SelectByAll().ToList();
                        }
                        break;
                    case nameof(ClassStudent):
                        using (Repository<ClassStudent> Repository = new Repository<ClassStudent>())
                        {
                            ClassStudents = Repository.MultiSelectByQuery(x => x.is_active).ToList();
                        }
                        break;
                    case nameof(Setting):
                        using (Repository<Setting> Repository = new Repository<Setting>())
                        {
                            Settings = Repository.SelectByAll().ToList();
                        }
                        break;
                    case nameof(Language):
                        using (Repository<Language> Repository = new Repository<Language>())
                        {
                            Languages = Repository.SelectByAll().ToList();
                        }
                        break;
                    case nameof(Material):
                        using (Repository<Material> Repository = new Repository<Material>())
                        {
                            Materials = Repository.MultiSelectByQuery(x => x.is_active).ToList();
                            DeletedMaterials = Repository.MultiSelectByQuery(x => !x.is_active).ToList();
                        }
                        break;
                    case nameof(Permission):
                        using (Repository<Permission> Repository = new Repository<Permission>())
                        {
                            Permissions = Repository.SelectByAll().ToList();
                        }
                        break;
                    case nameof(ProficiencyLanguage):
                        using (Repository<ProficiencyLanguage> Repository = new Repository<ProficiencyLanguage>())
                        {
                            ProficiencyLanguages = Repository.SelectByAll().ToList();
                        }
                        break;
                    case nameof(Rating):
                        using (Repository<Rating> Repository = new Repository<Rating>())
                        {
                            Ratings = Repository.SelectByAll().ToList();
                        }
                        break;
                    case nameof(RawPage):
                        using (Repository<RawPage> Repository = new Repository<RawPage>())
                        {
                            RawPages = Repository.SelectByAll().ToList();
                        }
                        break;
                    case nameof(Role):
                        using (Repository<Role> Repository = new Repository<Role>())
                        {
                            Roles = Repository.MultiSelectByQuery(x => x.is_active).ToList();
                        }
                        break;
                    case nameof(Room):
                        using (Repository<Room> Repository = new Repository<Room>())
                        {
                            Rooms = Repository.MultiSelectByQuery(x => x.is_active).ToList();
                        }
                        Rooms.ForEach(r => r.RoomStudents = RoomStudents.Where(x => x.room_id == r.id).ToList());
                        Rooms.ForEach(r => r.SessionTasks = SessionTasks.Where(x => x.room_id == r.id && x.is_active).ToList());
                        break;
                    case nameof(RoomStudent):
                        using (Repository<RoomStudent> Repository = new Repository<RoomStudent>())
                        {
                            RoomStudents = Repository.MultiSelectByQuery(x => x.is_active).ToList();
                        }
                        RoomStudents.ForEach(x => x.Room = Rooms.GetById(x.room_id));
                        RoomStudents.ForEach(x => x.User = Users.GetById(x.student_id));
                        break;
                    case nameof(Sequence):
                        using (Repository<Sequence> Repository = new Repository<Sequence>())
                        {
                            Sequences = Repository.MultiSelectByQuery(x => x.is_active).ToList();
                        }
                        break;
                    case nameof(SequenceTask):
                        using (Repository<SequenceTask> Repository = new Repository<SequenceTask>())
                        {
                            SequenceTasks = Repository.MultiSelectByQuery(x => x.is_active).ToList();
                        }
                        break;
                    case nameof(SessionTask):
                        using (Repository<SessionTask> Repository = new Repository<SessionTask>())
                        {
                            SessionTasks = Repository.MultiSelectByQuery(x => x.is_active).ToList();
                        }
                        break;
                    case nameof(Tag):
                        using (Repository<Tag> Repository = new Repository<Tag>())
                        {
                            Tags = Repository.SelectByAll().ToList();
                        }
                        break;
                    case nameof(TargetLanguage):
                        using (Repository<TargetLanguage> Repository = new Repository<TargetLanguage>())
                        {
                            TargetLanguages = Repository.SelectByAll().ToList();
                        }
                        TargetLanguages.ForEach(x => x.Language = Languages.GetById(x.language_id));
                        break;
                    case nameof(TaskAction):
                        using (Repository<TaskAction> Repository = new Repository<TaskAction>())
                        {
                            TaskActions = Repository.MultiSelectByQuery(x => x.is_active).ToList();
                        }
                        break;
                    case nameof(TaskEditor):
                        using (Repository<TaskEditor> Repository = new Repository<TaskEditor>())
                        {
                            TaskEditors = Repository.MultiSelectByQuery(x => x.is_active).ToList();
                        }
                        break;
                    case nameof(TaskMaterial):
                        using (Repository<TaskMaterial> Repository = new Repository<TaskMaterial>())
                        {
                            TaskMaterials = Repository.MultiSelectByQuery(x => x.is_active).ToList();
                        }
                        break;
                    case nameof(TaskMetaHelpLink):
                        using (Repository<TaskMetaHelpLink> Repository = new Repository<TaskMetaHelpLink>())
                        {
                            TaskMetaHelpLinks = Repository.SelectByAll().ToList();
                        }
                        break;
                    case nameof(_Task):
                        using (Repository<_Task> Repository = new Repository<_Task>())
                        {
                            Tasks = Repository.MultiSelectByQuery(x => x.is_active).ToList();
                            DeletedTasks = Repository.MultiSelectByQuery(x => !x.is_active).ToList();
                        }
                        break;
                    case nameof(TaskTag):
                        using (Repository<TaskTag> Repository = new Repository<TaskTag>())
                        {
                            TaskTags = Repository.SelectByAll().ToList();
                        }
                        break;
                    case nameof(User):
                        using (Repository<User> Repository = new Repository<User>())
                        {
                            Users = Repository.MultiSelectByQuery(x => x.is_active).ToList();
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {

            }
        }
        public static List<T> Actives<T>(this List<T> list)
        {
            return list.Where(e => e.GetType().GetProperty("is_active").GetValue(e).ToString().ToLower().Equals("true")).ToList();
        }
        public static T GetById<T>(this List<T> list, long? id)
        {
            if (list == null)
                return default(T);
            T obj = list.FirstOrDefault(t => t.GetType().GetProperty("id").GetValue(t).ToString().Equals(id.ToString()));
            return obj;
        }
        public static T GetByForeignId<T>(this List<T> list, string foreign_key, long? id)
        {
            T obj = list.FirstOrDefault(t => t.GetType().GetProperty(foreign_key).GetValue(t).ToString().Equals(id.ToString()));
            return obj;
        }
        public static List<T> GetMultiByForeignId<T>(this List<T> list, string foreign_key, long? id)
        {
            List<T> obj = list.Where(t => t.GetType().GetProperty(foreign_key).GetValue(t).ToString().Equals(id.ToString())).ToList();
            return obj;
        }
        public static int LicenceCheck()
        {
            try
            {
                DateTime setupDate = DateTime.Parse(Credentials.SystemCredentials.SetupDate);
                DateTime expiration = setupDate.AddYears(1);
                return (int)(expiration - DateTime.Now).TotalDays;
            }
            catch (Exception)
            {

            }
            return -30;
        }

    }
}
