﻿using MS.BLL;
using DT.DTO;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using Newtonsoft.Json;
using System.Runtime.CompilerServices;

namespace DT.DAL
{
    public class Repository<T> : DataRepository<T>, IDisposable where T : class
    {
        private bool LoadEntities;
        public User CurrentUser { get; }
        public Repository(long user_id = -1, bool ll = false) : base(new Ctx(user_id, ll))
        {
            try
            {
#if DEBUG
                Console.WriteLine("Mode=Debug");
#else
                if (HttpContext.Current.Request != null)
                {
                    HttpCookie cookie = HttpContext.Current.Request.Cookies["DT_User"];
                    if (cookie != null)
                        CurrentUser = JsonConvert.DeserializeObject<User>(HttpUtility.UrlDecode(cookie.Value));
                }
#endif

            }
            catch (Exception ex)
            {

            }
            StaticDB.ThisSession.Exception = null;
        }

        public override T Inserted(T entity)
        {
            try
            {
                if (CurrentUser != null)
                    entity = entity.CreatedBy(CurrentUser.id);
                entity = base.Inserted(entity.CheckCreatedDate().CheckModifiedDate().CheckIsActive());
                LoadEntities = true;
                return entity;
            }
            catch (Exception ex)
            {
                StaticDB.ThisSession.Exception = ex;
                return entity;
            }

        }
        public override void Inserting(T entity)
        {
            if (CurrentUser != null)
                entity = entity.CreatedBy(CurrentUser.id);
            base.Inserting(entity.CheckCreatedDate().CheckModifiedDate().CheckIsActive());
            LoadEntities = true;
        }
        public override T Updated(T entity)
        {
            try
            {
                if (CurrentUser != null)
                    entity = entity.ModifiedBy(CurrentUser.id);
                entity = base.Updated(entity.CheckModifiedDate());
                LoadEntities = true;
                return entity;
            }
            catch (Exception ex)
            {
                StaticDB.ThisSession.Exception = ex;
                return entity;
            }
        }

        public override void Updating(T entity)
        {
            if (CurrentUser != null)
                entity = entity.ModifiedBy(CurrentUser.id);
            LoadEntities = true;
            base.Updating(entity.CheckModifiedDate());
        }

        public override void SoftDeleting(T entity)
        {
            try
            {
                base.SoftDeleting(entity);
                LoadEntities = true;
            }
            catch (Exception ex)
            {
                StaticDB.ThisSession.Exception = ex;
                Logger.Add(ex, LogPriority.HighImportant, LogType.DBMistake);
            }
        }

        public override void Deleting(T entity)
        {
            base.Deleting(entity);
            LoadEntities = true;
        }

        public override void Deleting(Expression<Func<T, bool>> query)
        {
            T entity = base.SingleSelectByQuery(query);
            base.Deleting(query);
            LoadEntities = true;
        }
        public override T Deleted(T entity)
        {
            try
            {
                entity = base.Deleted(entity);
                LoadEntities = true;
                return entity;
            }
            catch (Exception ex)
            {
                StaticDB.ThisSession.Exception = ex;
            }
            return entity;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (LoadEntities)
                StaticDB.SetProperty(typeof(T).Name);
        }
    }
}
