﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DT.LOCAL.Objects {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class DataTypes {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal DataTypes() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("DT.LOCAL.Objects.DataTypes", typeof(DataTypes).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ondalık Sayı.
        /// </summary>
        public static string _float {
            get {
                return ResourceManager.GetString("_float", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tamsayı.
        /// </summary>
        public static string _int {
            get {
                return ResourceManager.GetString("_int", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dosya.
        /// </summary>
        public static string binary {
            get {
                return ResourceManager.GetString("binary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Renk.
        /// </summary>
        public static string color {
            get {
                return ResourceManager.GetString("color", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to İlişkisel.
        /// </summary>
        public static string connected {
            get {
                return ResourceManager.GetString("connected", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tarih.
        /// </summary>
        public static string date {
            get {
                return ResourceManager.GetString("date", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tarih-Saat.
        /// </summary>
        public static string datetime {
            get {
                return ResourceManager.GetString("datetime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Görsel.
        /// </summary>
        public static string image {
            get {
                return ResourceManager.GetString("image", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Uzun Yazı.
        /// </summary>
        public static string longtext {
            get {
                return ResourceManager.GetString("longtext", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to E-Posta Adresi.
        /// </summary>
        public static string mail {
            get {
                return ResourceManager.GetString("mail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kaydedilmiş Ölçüm.
        /// </summary>
        public static string record_value {
            get {
                return ResourceManager.GetString("record_value", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Seçilebilir.
        /// </summary>
        public static string select {
            get {
                return ResourceManager.GetString("select", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tablo Verisi.
        /// </summary>
        public static string table_data {
            get {
                return ResourceManager.GetString("table_data", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yazı.
        /// </summary>
        public static string text {
            get {
                return ResourceManager.GetString("text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Saat.
        /// </summary>
        public static string time {
            get {
                return ResourceManager.GetString("time", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Eşsiz Kimlik.
        /// </summary>
        public static string uniqueidentifier {
            get {
                return ResourceManager.GetString("uniqueidentifier", resourceCulture);
            }
        }
    }
}
