﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DT.LOCAL.Components {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class TaskWizard {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal TaskWizard() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("DT.LOCAL.Components.TaskWizard", typeof(TaskWizard).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Basic Information.
        /// </summary>
        public static string _BasicInformation {
            get {
                return ResourceManager.GetString("_BasicInformation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Completion Options.
        /// </summary>
        public static string _CompletionOptions {
            get {
                return ResourceManager.GetString("_CompletionOptions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Further Information.
        /// </summary>
        public static string _FurtherInformation {
            get {
                return ResourceManager.GetString("_FurtherInformation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Information Gap.
        /// </summary>
        public static string _InformationGap {
            get {
                return ResourceManager.GetString("_InformationGap", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Preview And Confirm.
        /// </summary>
        public static string _PreviewAndConfirm {
            get {
                return ResourceManager.GetString("_PreviewAndConfirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Task Design.
        /// </summary>
        public static string _TaskDesign {
            get {
                return ResourceManager.GetString("_TaskDesign", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Task Instructions.
        /// </summary>
        public static string _TaskInstructions {
            get {
                return ResourceManager.GetString("_TaskInstructions", resourceCulture);
            }
        }
    }
}
