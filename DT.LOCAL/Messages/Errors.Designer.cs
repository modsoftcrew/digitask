﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DT.LOCAL.Messages {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Errors {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Errors() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("DT.LOCAL.Messages.Errors", typeof(Errors).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sunucu bilgiler doğru. Ancak bu isimde bir veritabanı bulunamadı. Yeni bir veritabanı oluşturmak istiyor musunuz?.
        /// </summary>
        public static string CatalogIsWrong {
            get {
                return ResourceManager.GetString("CatalogIsWrong", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sunucu bağlantısı kurulamadı..
        /// </summary>
        public static string ConnectionInvalid {
            get {
                return ResourceManager.GetString("ConnectionInvalid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Veritabanı malesef oluşturulamadı.s.
        /// </summary>
        public static string DatabaseNotCreated {
            get {
                return ResourceManager.GetString("DatabaseNotCreated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Veritabanı başarıyla oluşturuldu. Ancak ilk veriler yüklenemedi..
        /// </summary>
        public static string DatabaseNotInitialized {
            get {
                return ResourceManager.GetString("DatabaseNotInitialized", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ERROR.
        /// </summary>
        public static string DialogTitle {
            get {
                return ResourceManager.GetString("DialogTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bir hata oluştu. Sistem yöneticiniz ile irtibata geçiniz!.
        /// </summary>
        public static string ErrorHasBeenOccured {
            get {
                return ResourceManager.GetString("ErrorHasBeenOccured", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Login is not successful. Please check your e-mail and password..
        /// </summary>
        public static string LoginNotSuccessful {
            get {
                return ResourceManager.GetString("LoginNotSuccessful", resourceCulture);
            }
        }
    }
}
