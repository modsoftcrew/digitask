﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DT.LOCAL.Messages {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class CompletionMessages {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal CompletionMessages() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("DT.LOCAL.Messages.CompletionMessages", typeof(CompletionMessages).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The designers determined specific keyword(s) for task completion. You will see Task Completed upon entering the correct value or Revise Your Answer upon entering wrong or incomplete values..
        /// </summary>
        public static string E {
            get {
                return ResourceManager.GetString("E", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Exact Value: The designers determined specific keyword(s) for task completion. You will see Task Completed upon entering the correct value or Revise Your Answer upon entering wrong or incomplete values. Also note that you have {0} minutes to submit the Exact Value and complete the task..
        /// </summary>
        public static string ET {
            get {
                return ResourceManager.GetString("ET", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click on Complete Anyway once you feel like you are ready to finalize the task..
        /// </summary>
        public static string N {
            get {
                return ResourceManager.GetString("N", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You have {0} minutes to complete the task, your response by the end of the allocated time will be saved as your Task Answer..
        /// </summary>
        public static string T {
            get {
                return ResourceManager.GetString("T", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The designers determined minimum/maximum words for task completion. You can observe the word count on the Answer Box and submit your Task Answer once the word counter is green-colored which means you reached the instructed word limit..
        /// </summary>
        public static string W {
            get {
                return ResourceManager.GetString("W", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Word Limit: The designers determined minimum/maximum words for task completion. You can observe the word count on the Answer Box and submit your Task Answer once the word counter is green-colored which means you reached the instructed word limit. Also note that you have {0} minutes to reach the limit and complete the task..
        /// </summary>
        public static string WT {
            get {
                return ResourceManager.GetString("WT", resourceCulture);
            }
        }
    }
}
