﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using DT.DAL;
using DT.DTO;
using DT.WEB.Models;

namespace DT.WEB.Controllers
{
    public class UserManagementController : BaseController
    {
        // GET: UserManagement
        public ActionResult Index(int role_id = 0)
        {
            ViewBag.Title = LOCAL.Objects.Tables.Users;
            ViewBag.SubDirectory = LOCAL.Objects.Tables.Users;
            ViewBag.role_id = role_id;
            List<User> users = new List<User>();
            if (CurrentUser.role_id == 1 || CurrentUser.role_id == 2)
            {
                using (Repository<User> Repository = new Repository<User>())
                {
                    users = Repository.SelectByAll().ToList();
                }
                if (role_id != 0)
                    users = users.Where(x => x.role_id == role_id).ToList();
                return View(users);
            }
            else if (CurrentUser.role_id == 3)
            {
                using (Repository<User> Repository = new Repository<User>())
                {
                    users = Repository.MultiSelectByQuery(t => t.is_active &&
                    t.created_by == CurrentUser.id).ToList();
                }
                if (role_id != 0)
                    users = users.Where(x => x.role_id == role_id).ToList();
                return View(users);
            }
            else
                return RedirectToAction("Index", "Home");
        }
        public ActionResult AddUpdate(int id = 0,int role_id=0)
        {
            User user = new User();
            user.role_id = role_id;
            if (id != 0)
                using (Repository<User> Repository = new Repository<User>())
                {
                    user = Repository.SingleSelectByQuery(x => x.id == id && x.is_active); 
                }
            return PartialView("_AddUpdatePartial",user);
        }

        public ActionResult GetTeachers(string search = "")
        {
            string s = "";
            if (!string.IsNullOrEmpty(search))
                s = search.ToLower().Trim();
            var list = from u in StaticDB.Users
                       where u.role_id == 3
                       where u.name.ToLower().Contains(s) ||
                        u.lastname.ToLower().Contains(s) ||
                        u.mail.ToLower().Contains(s)
                       select new
                       {
                           id = u.id,
                           text = $"{u.name} {u.lastname} ({u.mail})"
                       };
            return Json(new { results = list, pagination = new { more = false } }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AddUpdate(User user)
        {
            using (Repository<User> Repository = new Repository<User>())
            {
                var us = Repository.SingleSelectByQuery (x => x.id != user.id && x.mail.Trim().ToLower().Equals(user.mail.Trim().ToLower()));

                if (us == null)
                {
                    user.confirmation_expiration = DateTime.Now;
                    user.is_allowed = true;
                    user.is_confirmed = true;
                    user.is_subscribed = true;
                    user.is_profile_visible = true;
                    user.is_active = true;
                    user.ui_language = "en";
                    if (user.id == 0)
                        user = Repository.Inserted(user);
                    else
                        user = Repository.Updated(user);
                }
                else
                {
                    return Json(new { Result = false, Message="This mail exists!"});
                }

            }
            return Json(new { Result = true, User = user });
        }

        public ActionResult Details(int id)
        {

            User user = new User();
            using (Repository<User> Repository = new Repository<User>())
            {
                Repository.Include("ClassStudents");
                Repository.Include("SessionTaskParticipants");
                user = Repository.SingleSelectByQuery(x => x.id == id && x.is_active);
            }
            if (user == null)
                return RedirectToAction("Index");
            ViewBag.Title = "Profile";
            ViewBag.SubDirectory = "User Details";
            List<TimelineModel> timelineItems = new List<TimelineModel>();
            using (Ctx ctx = new Ctx())
            {
                var taskTimelines = from x in ctx.Tasks.Where(x => x.created_by == id && x.is_active)
                                    select new TimelineModel
                                    {
                                        Table = "Task",
                                        Icon = "fas fa-tasks",
                                        Color = "bg-blue",
                                        Object = x,
                                        CreatedDate = x.created_date,
                                        Details = x.target_subject
                                    };
                timelineItems.AddRange(taskTimelines.ToList());


                var userTimelines = from x in ctx.Users.Where(x => x.created_by == id && x.is_active)
                                    select new TimelineModel
                                    {
                                        Table = "User",
                                        Icon = "fas fa-students",
                                        Color = "bg-cyan",
                                        Object = x,
                                        CreatedDate = x.created_date,
                                        Details = x.details
                                    };
                timelineItems.AddRange(userTimelines.ToList());


                var materialTimeLines = from x in ctx.Materials.Where(x => x.created_by == id && x.is_active)
                                    select new TimelineModel
                                    {
                                        Table = "Material",
                                        Icon = "fas fa-photo-video",
                                        Color = "bg-bubbles",
                                        Object = x,
                                        CreatedDate = x.created_date,
                                        Details = x.details
                                    };
                timelineItems.AddRange(materialTimeLines.ToList());


                var sequenceTimeLines = from x in ctx.Sequences.Where(x => x.created_by == id && x.is_active)
                                       select new TimelineModel
                                       {
                                           Table = "Sequence",
                                           Icon = "fas fa-calendar-alt",
                                           Color = "bg-blue",
                                           Object = x,
                                           CreatedDate = x.created_date,
                                           Details = x.details
                                       };
                timelineItems.AddRange(sequenceTimeLines.ToList());
            }

            ViewData["TimelineItems"] = timelineItems.OrderByDescending(x => x.CreatedDate).ToList();
            return View(user);
        }
        public ActionResult Delete(int id)
        {
            using (Repository<User> Repository = new Repository<User>())
            {
                User user = Repository.SingleSelectByQuery(x => x.id == id && x.is_active);
                Repository.SoftDeleting(user);
            }
            return RedirectToAction("Index");
        }

        public ActionResult ShowPassword(int user_id)
        {
            User user = StaticDB.Users.GetById(user_id);
            return PartialView("PartialShowPassword", user);
        }
        public ActionResult SetProp(int user_id, string property, bool value)
        {
            User user = StaticDB.Users.GetById(user_id);
            if (user == null)
                return Json(new { Result = false }, JsonRequestBehavior.AllowGet);
            else
            {

                using (Repository<User> Repository = new Repository<User>())
                {
                    user = Repository.SingleSelectByQuery(x => x.id == user_id);
                    PropertyInfo propertyInfo = typeof(User).GetProperty(property);
                    propertyInfo.SetValue(user, value);
                    Repository.Updating(user);
                }
                return Json(new { Result = true }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}