﻿using MS.BLL;
using MS.BLL.Helpers;
using DT.DTO;
using DT.WEB.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using DT.DAL;

namespace DT.WEB.Controllers
{
    [NoCache]
    public class RoomController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddUpdate()
        {
            return PartialView("_CreateRoomPartial", new Room());
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddUpdate(DT.DTO.Room room)
        {
            try
            {
                using (Repository<DT.DTO.Room> Repository = new Repository<DT.DTO.Room>())
                {
                    if (room.id == 0)
                        room = Repository.Inserted(room);
                    else
                        room = Repository.Updated(room);
                }

                _Task task = new _Task();
                using (Repository<_Task> Repository = new Repository<_Task>())
                {
                    task = Repository.SingleSelectByQuery(t => t.id == room.TaskId);
                }
                SessionTask sessionTask = new SessionTask();
                if (room.activation_date != null)
                    sessionTask.planned_start_time = Convert.ToDateTime(room.activation_date);
                if (room.expiration_date != null)
                sessionTask.planned_end_time = Convert.ToDateTime(room.expiration_date);
                sessionTask.room_id = room.id;
                sessionTask.task_id = room.TaskId;
                using (Repository<SessionTask> Repository = new Repository<SessionTask>())
                {
                    sessionTask = Repository.Inserted(sessionTask);
                }
                using (Repository<Room> Repository = new Repository<Room>())
                {
                    room = Repository.SingleSelectByQuery(r => r.id == room.id);
                    room.group_range = task.group_range;
                    room = Repository.Updated(room);
                }
                room.SessionTasks.Add(sessionTask);
                return Json(new { Result = true, Room = room });
            }
            catch (Exception ex)
            {
                return Json(new { Result = false, Message = ex });
            }
        }

        public ActionResult TaskDetails(int id)
        {
            SessionTask sessionTask = null;
            using (Repository<SessionTask> Repository = new Repository<SessionTask>())
            {
                sessionTask = Repository.SingleSelectByQuery(s => s.room_id == id && s.is_active);
            }
            if (sessionTask == null)
                return PartialView("~/Views/Shared/NullModal.cshtml");

            _Task task = null;
            using (Repository<_Task> Repository = new Repository<_Task>())
            {
                task = Repository.SingleSelectByQuery(x => x.id == sessionTask.task_id && x.is_active);
            }
            return PartialView("~/Views/TaskManagement/_DetailsPartial.cshtml", task);
        }
    }
}