﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DT.DAL;
using DT.DTO;
using DT.WEB.Models;
using MS.BLL;

namespace DT.WEB.Controllers
{
    public class TaskAssignmentController : BaseController
    {
        public ActionResult AssignTaskModal(int id, int assign_id, string assign_table)
        {
            SessionTask sessionTask = new SessionTask();
            sessionTask.task_id = id;
            sessionTask.planned_start_time = Zaman.Simdi;
            sessionTask.assigned_table = assign_table;
            if (assign_table.Equals("Class"))
                sessionTask.class_id = assign_id;
            else if (assign_table.Equals(nameof(Cluster)))
                sessionTask.cluster_id = assign_id;
            else if (assign_table.Equals(nameof(Room)))
                sessionTask.room_id = assign_id;
            return PartialView("_AssignTaskModal", sessionTask);
        }
        public ActionResult AssignSequenceModal(int id, int assign_id, string assign_table)
        {
            List<SessionTask> sessionTasks = new List<SessionTask>();
            int index = 1;
            foreach (SequenceTask  sequenceTask in StaticDB.SequenceTasks.Where(x=>x.is_active && x.sequence_id == id).OrderBy(s=>s.index_order))
            {
                SessionTask sessionTask = new SessionTask();
                sessionTask.task_id = sequenceTask.task_id;
                sessionTask.assigned_table = assign_table;
                if (assign_table.Equals("Class"))
                    sessionTask.class_id = assign_id;
                else if (assign_table.Equals(nameof(Cluster)))
                    sessionTask.cluster_id = assign_id;
                else if (assign_table.Equals(nameof(Room)))
                    sessionTask.room_id = assign_id;
                sessionTask.id = index++;
                sessionTask.sequence_id = id;
                sessionTask.Task = StaticDB.Tasks.GetById(sessionTask.task_id);
                sessionTasks.Add(sessionTask);
            }
            return PartialView("_AssignSequenceModal", sessionTasks);
            //ViewBag.sequence_id = id;
            //ViewBag.assigned_table = assign_table;
            //ViewBag.assign_id = assign_id;
            //List<SequenceTask> sequenceTasks = StaticDB.SequenceTasks.Where(x => x.is_active && x.sequence_id == id).OrderBy(s => s.index_order).ToList();
            //sequenceTasks.ForEach(x => x.Task = StaticDB.Tasks.GetById(x.task_id));
            //return PartialView("_AssignSequenceModal", sequenceTasks);
        }
        [HttpPost]
        public ActionResult AssignTask(SessionTask sessionTask, bool send_mail)
        {
            try
            {
                List<SessionTask> sessionTasks = Clusterization(sessionTask);
                List<SessionTask> assignedSessionTasks = new List<SessionTask>();
                foreach (SessionTask st in sessionTasks)
                {
                    SessionTask createdST = SaveAndSendSessionTask(st);
                    if (send_mail)
                    {
                        List<User> users = createdST.SessionTaskParticipants.Select(x => x.User).ToList();
                        for (int i = 0; i < users.Count; i++)
                        {
                            User user = users[i];
                            DTHelper.SendTaskAssignmentMail(user, createdST);
                        }
                    }
                    assignedSessionTasks.Add(createdST);
                }
                return Json(new { Result = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        private static List<SessionTask> Clusterization(SessionTask sessionTask)
        {
            List<SessionTask> sessionTasks = new List<SessionTask>();
            if (sessionTask.assigned_table.Equals("Class"))
            {
                List<Cluster> clusters = StaticDB.Clusters.Where(c => c.is_active && c.class_id == sessionTask.class_id).ToList();
                if (clusters == null || clusters.Count == 0)
                    sessionTasks.Add(sessionTask);
                else
                {
                    SessionTask st = sessionTask.Clone();
                    foreach (Cluster cluster in clusters)
                    {
                        st.assigned_table = nameof(Cluster);
                        st.class_id = null;
                        st.cluster_id = cluster.id;
                        sessionTasks.Add(st);
                    }
                }
            }
            return sessionTasks;
        }

        [HttpPost]
        public ActionResult AssignSequence(List<SessionTask> sessionTasks, bool send_mail)
        {
            try
            {
                List<SessionTask> clusterizedSessionTasks = new List<SessionTask>();
                List<SessionTask> savedSessionTasks = new List<SessionTask>();
                foreach (SessionTask sessionTask in sessionTasks)
                {
                    clusterizedSessionTasks.AddRange(Clusterization(sessionTask));
                }
                foreach (SessionTask sessionTask in clusterizedSessionTasks)
                {
                    SessionTask task = SaveAndSendSessionTask(sessionTask);
                    task.id = 0;
                    savedSessionTasks.Add(task);
                }
                if (send_mail && savedSessionTasks.Count > 0)
                {
                    List<User> users = savedSessionTasks[0].SessionTaskParticipants.Select(x => x.User).ToList();
                    for (int i = 0; i < users.Count; i++)
                    {
                        User user = users[i];
                        DTHelper.SendSequenceAssignmentMail(user, savedSessionTasks);
                    }
                }
                return Json(new { Result = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = false, Message=ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        private static SessionTask SaveAndSendSessionTask(SessionTask sessionTask)
        {
            using (Repository<SessionTask> Repository = new Repository<SessionTask>())
            {
                sessionTask = Repository.Inserted(sessionTask);
            }
            List<User> participants = new List<User>();
            using (Ctx ctx = new Ctx())
            {
                if (sessionTask.assigned_table.Equals("Class"))
                    participants = ctx.Database.SqlQuery<User>(string.Format(LOCAL.Objects.DBScripts.GetUsersByClassId, sessionTask.class_id)).ToList();
                else if (sessionTask.assigned_table.Equals(nameof(Cluster)))
                    participants = ctx.Database.SqlQuery<User>(string.Format(LOCAL.Objects.DBScripts.GetUsersByClusterId, sessionTask.cluster_id)).ToList();
                else if (sessionTask.assigned_table.Equals(nameof(Room)))
                    participants = ctx.Database.SqlQuery<User>(string.Format(LOCAL.Objects.DBScripts.GetUsersByRoomId, sessionTask.room_id)).ToList();
            }
            foreach (User user in participants)
            {
                SessionTaskParticipant sessionTaskParticipant = new SessionTaskParticipant();
                using (Repository<SessionTaskParticipant> Repository = new Repository<SessionTaskParticipant>())
                {
                    sessionTaskParticipant.session_task_id = sessionTask.id;
                    sessionTaskParticipant.participant_id = user.id;
                    sessionTaskParticipant = Repository.Inserted(sessionTaskParticipant);
                }
                sessionTaskParticipant.User = user;
                sessionTaskParticipant.SessionTask = sessionTask;
                sessionTask.SessionTaskParticipants.Add(sessionTaskParticipant);
            }

            DTHelper.SetTaskUsage(sessionTask.task_id);
            return sessionTask;
        }
        public ActionResult AssignTaskToClass(int id)
        {
            _Class _class = null;
            using (Repository<_Class> Repository = new Repository<_Class>())
            {
                Repository.Include(nameof(_Class.ClassStudents), nameof(_Class.Clusters));
                _class = Repository.SingleSelectByQuery(x => x.id == id && x.is_active);
            }
            Cacher.Save("Class", _class);
            Cacher.Save("SelectedTasks", new List<SequenceTask>());
            Request.Cookies.Remove("TaskAddStatus");
            return View("ClassTaskAssignment");
        }
        public ActionResult AssignSequenceToClass(int id)
        {
            _Class _class = null;
            using (Repository<_Class> Repository = new Repository<_Class>())
            {
                Repository.Include(nameof(_Class.ClassStudents), nameof(_Class.Clusters));
                _class = Repository.SingleSelectByQuery(x => x.id == id && x.is_active);
            }
            Cacher.Save("Class", _class);
            Cacher.Save("SelectedTasks", new List<SequenceTask>());
            Request.Cookies.Remove("TaskAddStatus");
            return View("ClassTaskAssignment");
        }
        public ActionResult GetTasks(string type = "My")
        {
            ViewData["Type"] = type;
            try
            {
                List<_Task> tasks = new List<_Task>();
                using (Repository<_Task> Repository = new Repository<_Task>())
                {
                    var ts = Repository.MultiSelectByQuery(t => t.is_active && t.status.Equals(nameof(LOCAL.Enums.StatusTypes.Published)));
                    tasks = type == "My" ?
                        ts.Where(t => t.created_by == CurrentUser.id).ToList() :
                        ts.ToList();
                }

                return PartialView("_AssignableTasksListPartial", tasks);
            }
            catch (Exception ex)
            {
                return Json(new { Result = false }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult GetSelectedTasksButton()
        {
            try
            {
                List<SequenceTask> tasks = Cacher.Get<List<SequenceTask>>("SelectedTasks");
                ViewBag.SelectedTasksCount = tasks.Count;
                return PartialView("_SelectedTasksButtonPartial");
            }
            catch (Exception ex)
            {
                return Json(new { Result = false }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetSelectedTasks()
        {
            try
            {
                List<SequenceTask> tasks = Cacher.Get<List<SequenceTask>>("SelectedTasks");
                ViewBag.SelectedTasksCount = tasks.Count;
                return PartialView("_SelectedTasksPartialModal", tasks);
            }
            catch (Exception ex)
            {
                return Json(new { Result = false }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult AddRemoveTask(int id)
        {
            try
            {
                List<SequenceTask> tasks = Cacher.Get<List<SequenceTask>>("SelectedTasks");
                SequenceTask sequenceTask = tasks.FirstOrDefault(t => t.task_id == id);
                if (sequenceTask != null)
                {
                    tasks.Remove(sequenceTask);
                    Response.Cookies.Add(new HttpCookie("TaskAddStatus", "false"));
                }
                else
                {
                    _Task task = null;
                    using (Repository<_Task> Repository = new Repository<_Task>())
                    {
                        Repository.Include(nameof(_Task.TargetLanguages));
                        task = Repository.SingleSelectByQuery(t => t.id == id && t.is_active);
                    }
                    if (task != null)
                    {
                        sequenceTask = new SequenceTask();
                        sequenceTask.task_id = id;
                        sequenceTask.Task = task;
                        Response.Cookies.Add(new HttpCookie("TaskAddStatus", "true"));
                        tasks.Add(sequenceTask);
                    }
                }
                Cacher.Save("SelectedTasks", tasks);
                ViewBag.SelectedTasksCount = tasks.Count;
                return PartialView("_SelectedTasksButtonPartial");
            }
            catch (Exception ex)
            {
                return Json(new { Result = false });
            }
        }
    }
}
