﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DT.DAL;
using DT.DTO;

namespace DT.WEB.Controllers
{
    public class StudentManagementController : BaseController
    {
        // GET: StudentManagement
        public ActionResult Index()
        {
            ViewBag.Title = LOCAL.Objects.Tables.ClassStudents;
            List<ClassStudent> students = new List<ClassStudent>();
            if (CurrentUser.role_id == 1 || CurrentUser.role_id == 2)
            {
                ViewBag.SubDirectory = LOCAL.Components.WebMenuItems.Student_Management;
                using (Repository<ClassStudent> Repository = new Repository<ClassStudent>())
                {
                    Repository.Include(nameof(ClassStudent.User), nameof(ClassStudent.Class), "User.ProficiencyLanguages");
                    students = Repository.MultiSelectByQuery(x => x.is_active).ToList();
                }
                return View(students);
            }
            else if (CurrentUser.role_id == 3)
            {
                ViewBag.SubDirectory = LOCAL.Components.WebMenuItems.My_Students;
                using (Repository<ClassStudent> Repository = new Repository<ClassStudent>())
                {
                    Repository.Include(nameof(ClassStudent.User), nameof(ClassStudent.Class), "User.ProficiencyLanguages");
                    students = Repository.MultiSelectByQuery(t =>
                    t.is_active && t.created_by == CurrentUser.id).ToList();
                }
                return View(students);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult GetStudents(string search = "")
        {
            string s = "";
            if (!string.IsNullOrEmpty(search))
                s = search.ToLower().Trim();
            var list = from u in StaticDB.Users
                       where u.role_id == 4
                       where u.name.ToLower().Contains(s)  ||
                        u.lastname.ToLower().Contains(s) ||
                        u.mail.ToLower().Contains(s)
                       select new
                       {
                           id = u.id,
                           text = $"{u.name} {u.lastname} ({u.mail})"
                       };
            return Json(new { results = list, pagination = new { more = false } }, JsonRequestBehavior.AllowGet);
        }
    }
}