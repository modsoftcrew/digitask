﻿using MS.BLL;
using DT.DAL;
using DT.DTO;
using System;
using System.Linq;
using System.Web.Mvc;


namespace DT.WEB.Controllers
{
    public class AccountController : Controller
    {
        string folderPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Logs/");
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
        }
        public ActionResult Start()
        {
            return View();
        }
        public ActionResult Login(string msg = null)
        {
            User user = WebUtils.Cookies.Get<User>(this, "User");
            if (user != null && user.remember_me)
            {
                Logger.Write($"User {user.SysStr()} has been loginned automatically.", folderPath);
                if (msg != null) 
                    ViewBag.Message = msg;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                if (msg != null)
                    ViewBag.Message = msg;
                return View(new User());
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(User model)
        {
            Logger.Write($"User has sent credentials : {model.ToJson()}", folderPath);
            User user = null;
            try
            {
                using (Ctx ctx = new Ctx(-1, false))
                {
                    user = ctx.Users.FirstOrDefault(u => model.mail.Equals(u.mail) && model.password.Equals(u.password) && u.is_active);
                    if (user != null)
                    {
                        if (!user.is_allowed)
                        {
                            Logger.Write($"User {user.SysStr()} is not allowed to login.", folderPath);
                            return View(model);
                        }
                        else if (!user.is_confirmed)
                        {
                            Logger.Write($"User {user.SysStr()} mail has not been confirmed.", folderPath);
                            Session["UserMail"] = model.mail;
                            return RedirectToAction("ResendConfirmation", "Account");
                        }
                        else
                        {
                            Session["User"] = user;
                            WebUtils.Cookies.Save(this, "User", user);
                            Language language = ctx.Languages.FirstOrDefault(x => x.iso_code == user.ui_language);
                            WebUtils.Cookies.Save(this, "Language", language);
                            Logger.Write($"User {user.SysStr()} has been loginned.", folderPath);
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    else
                    {
                        Logger.Write($"Wrong credentials for login.", folderPath);
                        model.id = -1;
                        return View(model);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write($"Login Exception : {ex.ToJson()}", folderPath);
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult LoginDynamic(string mail, string password)
        {
            User user = null;
            try
            {
                using (Ctx ctx = new Ctx(-1, false))
                {
                    user = ctx.Users.FirstOrDefault(u => mail.Equals(u.mail) && password.Equals(u.password) && u.is_active);
                    if (user != null)
                    {
                        if (!user.is_allowed)
                        {
                            Logger.Write($"User {user.SysStr()} is not allowed to login.", folderPath);
                            return Json(new { Result = false, Message = LOCAL.Messages.Warnings.YouAreNotAllowed });
                        }
                        else if (!user.is_confirmed)
                        {
                            Logger.Write($"User {user.SysStr()} mail has not been confirmed.", folderPath);
                            return Json(new { Result = false, Message = LOCAL.Messages.Warnings.YouHaveNotConfirmedMail });
                        }
                        else
                        {
                            Session["User"] = user;
                            WebUtils.Cookies.Save(this, "User", user);
                            Language language = ctx.Languages.FirstOrDefault(x => x.iso_code == user.ui_language);
                            WebUtils.Cookies.Save(this, "Language", language);
                            Logger.Write($"User {user.SysStr()} has been loginned.", folderPath);
                            return Json(new { Result = true });
                        }
                    }
                    else
                    {
                        Logger.Write($"Wrong credentials for login.", folderPath);
                        return Json(new { Result = false, Message = LOCAL.Messages.Warnings.InvalidPassword });
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write($"Login Exception : {ex.ToJson()}", folderPath);
            }
            return Json(new { Result = false, Message = LOCAL.Messages.Warnings.ServerError });
        }
        public ActionResult Register(string msg = null)
        {
            if (msg != null)
                ViewBag.Message = msg;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(User model)
        {
            Logger.Write($"User registers : {model.ToJson()}", folderPath);
            User user = null;
            if (model.role_id != 3 && model.role_id != 4)
                return View(model);
            try
            {
                User existing = null;
                using (Repository<User> Repository = new Repository<User>())
                {
                    existing = Repository.SingleSelectByQuery(x => x.mail.Trim().ToLower().Equals(model.mail.Trim().ToLower()));
                }
                if(existing != null)
                {
                    Logger.Write($"Existing user {user.SysStr()} tried to register.", folderPath);
                    ViewBag.Message = LOCAL.Messages.Warnings.UsernameExist;
                    return View(model);
                }
                // Role ID may not be 3 or 4 in DB. So we check the real ID's gets the actual Role ID to register
                Role role = new Role();
                if (model.role_id == 3)
                    role = StaticDB.Roles.FirstOrDefault(rr => rr.title == nameof(LOCAL.Enums.Roles.Teacher));
                else
                    role = StaticDB.Roles.FirstOrDefault(rr => rr.title == nameof(LOCAL.Enums.Roles.Student));

                using (Repository<User> Repository = new Repository<User>())
                {
                    user = Repository.SingleSelectByQuery(u => model.mail.Equals(u.mail) && u.is_active);
                    if (user == null)
                    {
                        model.role_id = role.id;
                        model.is_allowed = true;
                        model.is_active = true;
                        model.confirmation_expiration = Zaman.Simdi.AddDays(2);
                        model = Repository.Inserted(model);
                        if (DTHelper.SendConfirmationMail(model))
                            return RedirectToAction("Login");
                        else
                            return View(model);
                    }
                    else
                    {
                        if (!user.is_allowed)
                        {
                            Logger.Write($"Not allowed user {user.SysStr()} tried to register.", folderPath);
                            ViewBag.Message = LOCAL.Messages.Warnings.NotAllowedToRegister;
                            return View(model);
                        }
                        else if (!user.is_confirmed)
                        {
                            Logger.Write($"User {user.SysStr()} mail has not been confirmed.", folderPath);
                            Session["UserMail"] = user.mail;
                            return RedirectToAction("ResendConfirmation", "Account", new { msg = LOCAL.Messages.Infos.ResendConfirmationInfo });
                        }
                        else
                        {
                            Logger.Write($"Registered user {user.SysStr()} tried to register.", folderPath);
                            return RedirectToAction("ForgotPassword",new { msg = LOCAL.Messages.Warnings.AlreadyRegister});
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write($"Register Exception : {ex.ToJson()}", folderPath);
            }
            return View(model);
        }

        public ActionResult TermsAndConditions()
        {
            return PartialView("_TermsAndConditions");
        }

        public ActionResult ForgotPassword(string msg = null)
        {
            if (msg != null)
                ViewBag.Message = msg;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(User model)
        {
            Logger.Write($"User : {model.ToJson()} forgot mail", folderPath);
            User user = null;
            try
            {
                using (Repository<User> Repository = new Repository<User>())
                {
                    user = Repository.SingleSelectByQuery(u => model.mail.Equals(u.mail) && u.is_active);
                }

                if (user != null)
                {
                    if (!user.is_allowed)
                    {
                        Logger.Write($"User {user.SysStr()} is not allowed to login.", folderPath);
                        ViewBag.Message = LOCAL.Messages.Warnings.NotAllowedToLogin;
                        return View(model);
                    }
                    else if (!user.is_confirmed)
                    {
                        Logger.Write($"User {user.SysStr()} mail has not been confirmed.", folderPath);
                        ViewBag.Message = LOCAL.Messages.Warnings.NotConfirmedMail;
                        return View(model);
                    }
                    else
                    {
                        using (Repository<User> Repository = new Repository<User>())
                        {
                            user.is_active = true;
                            user.confirmation_expiration = Zaman.Simdi.AddDays(2);
                            user = Repository.Updated(user);
                        }
                        DTHelper.SendForgotPasswordMail(user);
                        return RedirectToAction("Login", new { msg = LOCAL.Messages.Infos.CrendentialsHaveBeenSent});
                    }
                }
                else
                {
                    Logger.Write($"Someone has tried to send mail to remind password.", folderPath);
                    ViewBag.Message = LOCAL.Messages.Warnings.NotExistedMail;
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                Logger.Write($"Forgot Password Exception : {ex.ToJson()}", folderPath);
            }
            return View(model);
        }
        public ActionResult CleanStatic()
        {
            try
            {
                WebUtils.FillStaticDB();
            }
            catch (Exception ex)
            {
                DT.DAL.Logger.Save("Önbellek boşaltılamadı.", ex);
            }
            return Redirect("/");
        }
        public ActionResult Logout()
        {
            Session.Clear();
            WebUtils.Cookies.Delete(this, "User");
            return RedirectToAction("Start");
        }

        [HttpPost]
        public ActionResult CheckMailIsAvailable(string mail)
        {
            try
            {
                if (string.IsNullOrEmpty(mail))
                    return Json(new
                    {
                        Result = false
                    });
                User user = StaticDB.Users.FirstOrDefault(x => x.mail.Trim().ToLower().Equals(mail.Trim().ToLower()));

                return Json(new
                {
                    Result = user == null
                }); ;
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Result = false
                });
            }
        }
        public ActionResult ResendConfirmation(string msg = null)
        {
            if (msg != null)
                ViewBag.Message = msg;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PostConfirmation(string mail)
        {
            User user = null;
            using (Repository<User> Repository = new Repository<User>())
            {
                user = Repository.SingleSelectByQuery(x => x.mail.Equals(mail.Trim()));
                if (user == null)
                    return RedirectToAction("Login", new {msg = LOCAL.Messages.Warnings.NotExistedMail });
                user.confirmation_expiration = Zaman.Simdi.AddDays(2);
                user = Repository.Updated(user);
            }
            DTHelper.SendConfirmationMail(user);
            return RedirectToAction("Login", new { msg = "Confirmation mail has been sent.Please check your inbox and also junk/spam folder in your mailbox." });
        }
        public ActionResult RawPageShower(int id)
        {
            RawPage entity = null;
            using (Repository<RawPage> Repository = new Repository<RawPage>())
            {
                entity = Repository.SingleSelectByQuery(x => x.id == id);
            }
            if (entity != null)
                entity.User = StaticDB.Users.GetById(entity.created_by);
            return PartialView("_RawPageViewer", entity);
        }
        public ActionResult VerifyMail(string mail)
        {
            try
            {
                string userMail = System.Web.HttpUtility.UrlDecode(mail.Decrypt());
                using (Repository<User> Repository = new Repository<User>())
                {
                    User user = Repository.SingleSelectByQuery(u => u.mail.Equals(userMail) && u.is_active);

                    // If confirmation time has been expired, soft delete user and redirect homepage
                    if (user != null)
                    {
                        if (user.confirmation_expiration < Zaman.Simdi)
                        {
                            Logger.Write($"User {user.SysStr()} CONFIRMATION EXPIRED.", folderPath);
                            Session["UserMail"] = mail;
                            return RedirectToAction("ResendConfirmation", new {msg= LOCAL.Messages.Infos.ResendConfirmationInfo });
                        }
                        if (!user.is_allowed)
                        {
                            Logger.Write($"User {user.SysStr()} is not allowed to login.", folderPath);
                            return View(user);
                        }
                        else
                        {
                            user.is_confirmed = true;
                            user.is_active = true;
                            user = Repository.Updated(user);
                            Logger.Write($"User {user.SysStr()} mail has been confirmed.", folderPath);
                            WebUtils.Cookies.Save("User", user);
                            Language language = StaticDB.Languages.FirstOrDefault(x => x.iso_code == user.ui_language);
                            WebUtils.Cookies.Save("Language", language);
                            Logger.Write($"User {user.SysStr()} has been loginned.", folderPath);
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    else
                    {
                        Logger.Write($"Someone has tried to confirm mail.", folderPath);
                        ViewBag.Message = LOCAL.Messages.Warnings.NotExistedMail;
                        return RedirectToAction("Start");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write($"VerifyMail Exception : {ex.ToJson()}", folderPath);
                return RedirectToAction("Start");
            }
        }
        public ActionResult ResetPassword(string mail)
        {
            try
            {
                string userMail = System.Web.HttpUtility.UrlDecode(mail.Decrypt());
                User user = null;
                using (Repository<User> Repository = new Repository<User>())
                {
                    user = Repository.SingleSelectByQuery(u => u.mail.Equals(userMail) && u.is_active);
                }
                if (user != null)
                {
                    if (user.confirmation_expiration < Zaman.Simdi)
                    {
                        return RedirectToAction("ForgotPassword", new {msg = LOCAL.Messages.Warnings.ResetPasswordExpiration });
                    }
                    if (!user.is_allowed)
                    {
                        Logger.Write($"User {user.SysStr()} is not allowed to login.", folderPath);
                        return View("Register", new {msg= LOCAL.Messages.Warnings.NotAllowedToLogin });
                    }
                    else
                    {
                        return View("ChangePassword", user);
                    }
                }
                else
                {
                    Logger.Write($"Someone has tried to confirm mail.", folderPath);
                    ViewBag.Message = LOCAL.Messages.Warnings.NotExistedMail;
                    return RedirectToAction("Start");
                }
            }
            catch (Exception ex)
            {
                Logger.Write($"VerifyMail Exception : {ex.ToJson()}", folderPath);
                return RedirectToAction("Start");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(User model)
        {
            using (Repository<User> Repository = new Repository<User>())
            {
                User user = Repository.SingleSelectByQuery(u => u.id == model.id);
                user.password = model.password;
                user.confirm_password = model.password;
                user.is_active = true;
                user = Repository.Updated(user);
            }
            return RedirectToAction("Login");
        }
        public ActionResult Subscription(string code)
        {
            string[] parameters = code.FromBase64().Split(';');
            string mail = parameters[0].Split('=')[1];
            bool status = parameters[1].Split('=')[1] == "true";
            try
            {
                using (Repository<User> Repository = new Repository<User>())
                {
                    User user = Repository.SingleSelectByQuery(u => u.mail.Equals(mail) && u.is_active);
                    if (user != null)
                    {
                        if (!user.is_allowed)
                        {
                            Logger.Write($"User {user.SysStr()} is not allowed to login.", folderPath);
                            ViewBag.Message = LOCAL.Messages.Warnings.NotAllowedToLogin;
                            return View(user);
                        }

                        user.is_subscribed = status;
                        user.is_active = true;
                        user = Repository.Updated(user);
                        Logger.Write($"User {user.SysStr()} subscription status has been changed.", folderPath);
                        return View(user);
                    }
                    else
                    {
                        Logger.Write($"Someone has tried to subscribed mail.", folderPath);
                        return RedirectToAction("Register");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write($"Subscription Exception : {ex.ToJson()}", folderPath);
                return RedirectToAction("Index", "Home");
            }
        }
    }
}