﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DT.DAL;
using DT.DTO;
using MS.BLL;

namespace DT.WEB.Controllers
{
    public class ClusterManagementController : BaseController
    {
        // GET: ClusterManagement
        public ActionResult Index()
        {
            ViewBag.Title = LOCAL.Objects.Tables.Clusters;
            ViewBag.SubDirectory = LOCAL.Components.WebMenuItems.My_Clusters;
            List<Cluster> clusters = new List<Cluster>();
            if (CurrentUser.role_id == 1 || CurrentUser.role_id == 2)
            {
                using (Repository<Cluster> Repository = new Repository<Cluster>())
                {
                    Repository.Include(nameof(Cluster.Class), nameof(Cluster.ClusterStudents));
                    clusters = Repository.MultiSelectByQuery(x => x.is_active).ToList();
                }
                return View(clusters);
            }
            else if (CurrentUser.role_id == 3)
            {
                using (Repository<Cluster> Repository = new Repository<Cluster>())
                {
                    Repository.Include(nameof(Cluster.Class), nameof(Cluster.ClusterStudents));
                    clusters = Repository.MultiSelectByQuery(t =>
                    t.is_active && t.created_by == CurrentUser.id).ToList();
                }
                return View(clusters);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult AddUpdate(int id = 0)
        {
            Cluster cluster = new Cluster();
            if (id != 0)
                using (Repository<Cluster> Repository = new Repository<Cluster>())
                {
                    cluster = Repository.SingleSelectByQuery(x => x.id == id && x.is_active);
                }
            return PartialView("_AddUpdatePartialModal", cluster);
        }

        [HttpPost]
        public ActionResult AddUpdate(DT.DTO.Cluster cluster)
        {
            if (ModelState.IsValid)
            {

                using (Repository<Cluster> Repository = new Repository<Cluster>())
                {
                    if (cluster.id == 0)
                        cluster= Repository.Inserted(cluster);
                    else
                        cluster = Repository.Updated(cluster);
                }
            }
            return Json(new { Result = true, Cluster = cluster });
            //return RedirectToAction("Index");
        }
        public ActionResult GetRandomColor()
        {
            System.Drawing.Color color = Utilities.getRandomColor();
            string strColor = "#" + color.R.ToString("X2") + color.G.ToString("X2") + color.B.ToString("X2");
            return Json(new { Color = strColor }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id)
        {

            using (Repository<Cluster> Repository = new Repository<Cluster>())
            {
                Cluster cluster = Repository.SingleSelectByQuery(x => x.id == id && x.is_active);
                Repository.SoftDeleting(cluster);
            }
            return RedirectToAction("Index");
        }
    }
}