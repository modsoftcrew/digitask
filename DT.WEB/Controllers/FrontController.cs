﻿using MS.BLL;
using DT.DAL;
using DT.DTO;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;

namespace DT.WEB.Controllers
{
    public class FrontController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
        }
        public ActionResult TaskPreview(string smartcode)
        {
            ViewBag.Title = "Task Preview";
            try
            {
                DT.DTO._Task task = new DT.DTO._Task();
                using (Ctx ctx = new Ctx())
                {
                    task = ctx.Tasks.FirstOrDefault(x => x.smartcode.Equals(smartcode));
                    task.TaskMaterials = ctx.TaskMaterials.Where(x => x.task_id == task.id).ToList();
                    ((List<TaskMaterial>)task.TaskMaterials).ForEach(x => x.Material = ctx.Materials.Find(x.material_id));
                }
                  ((List<TaskMaterial>)task.TaskMaterials).ForEach(x => x.title = x.Material.title);
                ((List<TaskMaterial>)task.TaskMaterials).ForEach(x => x.type = x.Material.type);
                if (task != null)
                {
                    ViewBag.Title = task.title + " [PREVIEW]";
                    return View(task);
                }
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("Start","Account");
        }
    }
}