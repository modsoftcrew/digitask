﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DT.DAL;
using DT.DTO;
using DT.WEB.Models;

namespace DT.WEB.Controllers
{
    public class LanguageManagementController : BaseController
    {
        // GET: LanguageManagement
        public ActionResult Index()
        {
            ViewBag.Title = LOCAL.Objects.Tables.Languages;
            ViewBag.SubDirectory = LOCAL.Objects.Tables.Languages;
            List<Language> entities = new List<Language>();
            if (CurrentUser.role_id == 1 || CurrentUser.role_id == 2)
            {
                using (Repository<Language> Repository = new Repository<Language>())
                {
                    entities = Repository.SelectByAll().ToList();
                }
                return View(entities);
            }
            else 
                return RedirectToAction("Index", "Home");
        }
        public ActionResult AddUpdate(int id = 0)
        {
            Language entity = new Language();
            if (id != 0)
                using (Repository<Language> Repository = new Repository<Language>())
                {
                    entity = Repository.SingleSelectByQuery(x => x.id == id);
                }
            if(entity == null)
                entity = new Language();
            return PartialView("_AddUpdatePartial",entity);
        }

        [HttpPost]
        public ActionResult AddUpdate(Language entity)
        {
            using (Repository<Language> Repository = new Repository<Language>())
            {

                if (entity.id == 0)
                    entity = Repository.Inserted(entity);
                else
                    entity = Repository.Updated(entity);

            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            Language language = StaticDB.Languages.GetById(id);
            if(language == null ||
                StaticDB.TargetLanguages.Count(t=>t.language_id == id) > 0 ||
                StaticDB.ProficiencyLanguages.Count(t=>t.language_id == id) > 0 ||
                StaticDB.Users.Count(x=>x.ui_language.Equals(language.iso_code)) > 0)
                return RedirectToAction("Index");
            using (Repository<Language> Repository = new Repository<Language>())
            {
                Language entity = Repository.SingleSelectByQuery(x => x.id == id);
                Repository.Deleting(entity);
            }
            return RedirectToAction("Index");
        }
    }
}