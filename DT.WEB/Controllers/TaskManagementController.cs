﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Web;
using System.Web.Mvc;
using DT.DAL;
using DT.DTO;
using DT.WEB.Models;
using MS.BLL;
using Newtonsoft.Json;

namespace DT.WEB.Controllers
{
    public class TaskManagementController : BaseController
    {
        public ActionResult Index(string type = "all")
        {
            ViewBag.Title = LOCAL.Objects.Tables.Tasks;
            ViewBag.Type = type;
            if (type.Equals("all"))
            {
                ViewBag.SubDirectory = LOCAL.Components.WebMenuItems.All_Tasks;
                ViewBag.CanEdit = CurrentUser.role_id == 1 || CurrentUser.role_id == 2;
                return View();
            }
            else if (type.Equals("bookmarked"))
            {
                ViewBag.SubDirectory = LOCAL.Components.WebMenuItems.BookmarkedTasks;
                ViewBag.CanEdit = false;
                return View();
            }
            else if (type.Equals("my"))
            {
                ViewBag.SubDirectory = LOCAL.Components.WebMenuItems.My_Tasks;
                ViewBag.CanEdit = true;
                return View();
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult DeletedIndex()
        {
            ViewBag.Title = LOCAL.Objects.Tables.Tasks + " " + LOCAL.Components.WebMenuItems.DeletedTasksList;
            ViewBag.SubDirectory = LOCAL.Components.WebMenuItems.DeletedTasksList;
            if (ViewBag.Admin = (CurrentUser.role_id == 1 || CurrentUser.role_id == 2))
            {
                return View(StaticDB.DeletedTasks);
            }
            else
            {
                return View(StaticDB.DeletedTasks.Where(t => t.created_by == CurrentUser.id).ToList());
            }
        }
        public ActionResult Selector(int range = 0)
        {
            ViewBag.Range = range;
            return PartialView("_TaskSelector");
        }
        [HttpPost]
        public ActionResult GetList(string type = "all", int range = 0)
        {
            int draw = Convert.ToInt32(Request.Form["draw"]);// etkin sayfa numarası
            int start = Convert.ToInt32(Request["start"]);//listenen ilk kayıtın  index numarası
            int length = Convert.ToInt32(Request["length"]);//sayfadaki toplam listelenecek kayit sayısı
            string search = Request["search[value]"];//arama
            string formList = "";
            foreach (var item in Request.Form)
            {
                formList += item.ToStr() + " : " + Request.Form[item.ToStr()].ToStr() + Environment.NewLine;
            }
            string sortColumnName = Request["columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]"];//Sıralama yapılacak column adı
            string sortDirection = Request["order[0][dir]"];//sıralama türü
            sortColumnName = string.IsNullOrEmpty(sortColumnName) ? "created_date" : sortColumnName;


            List<_Task> tasks = new List<_Task>();
            List<int> bookmarkedTaskIds = new List<int>();
            using (Ctx ctx = new Ctx())
            {
                tasks = ctx.Tasks.Where(t => t.is_active && (t.created_by != CurrentUser.id ? !t.status.Equals("Draft") : true) && (range != 0 ? t.group_range == range : true)).ToList();
                bookmarkedTaskIds = ctx.BookmarkedTasks.Where(b => b.user_id == CurrentUser.id).Select(b => b.task_id).ToList();
            }
            foreach (_Task task in tasks)
            {

                task.IsBookmarked = bookmarkedTaskIds.Contains(task.id);
                task.TargetSkill = (task.target_skill == null ? "-" : new ResourceManager(typeof(LOCAL.Enums.TargetSkills)).GetString(task.target_skill));
                task.UserCanEdit = (task.created_by == CurrentUser.id && !task.being_used) || CurrentUser.role_id == 1 || CurrentUser.role_id == 2;
                task.Creator = task.created_by != task.author_id ? StaticDB.Users.GetById(task.created_by).ToStr() : "";
                task.Author = StaticDB.Users.GetById(task.author_id).ToStr();
                task.target_languages_str = string.Join(",", StaticDB.TargetLanguages.Where(x => x.task_id == task.id).Select(x => x.Language));
                task.CoDesignerUsers = new List<User>();
                foreach (int co_user_id in task.CoDesigners)
                {
                    User coDesigner = StaticDB.Users.GetById(co_user_id);
                    if (coDesigner != null)
                        task.CoDesignerUsers.Add(coDesigner);
                }
            }
            if (type == "bookmarked")
                tasks = tasks.Where(b => b.IsBookmarked).ToList();
            else if (type == "my")
                tasks = tasks.Where(x => x.created_by == CurrentUser.id).ToList();

            List<_Task> list = new List<_Task>();
            if (sortColumnName.Equals(nameof(_Task.CreatedDate)))
            {
                if (sortDirection != "asc")
                    list = tasks.OrderByDescending(m => m.created_date).ToList();
                else
                    list = tasks.OrderBy(m => m.created_date).ToList();
            }
            else if (sortColumnName.Equals(nameof(_Task.PublishDate)))
            {
                if (sortDirection != "asc")
                    list = tasks.OrderByDescending(m => m.publish_date).ToList();
                else
                    list = tasks.OrderBy(m => m.publish_date).ToList();
            }
            else
            {
                list = tasks.AsQueryable().OrderByDynamic(sortColumnName, sortDirection != "asc").ToList();
            }
            if (search == null)
                search = "";

            if (!string.IsNullOrEmpty(search))
            {
                List<_Task> newList = new List<_Task>();
                foreach (var item in list)
                {
                    foreach (PropertyInfo property in typeof(_Task).GetProperties())
                    {
                        var value = property.GetValue(item);
                        if (value != null && value.ToStr().ToLower().Contains(search.ToLower()))
                        {
                            newList.Add(item);
                        }
                    }
                }
                list = newList;
            }
            for (int i = 1; i < 12; i++)
            {
                string prop = Request.Form[$"columns[{i}][data]"];
                if (prop.Equals("function"))
                    prop = Request.Form[$"columns[{i}][name]"];
                string value = Request.Form[$"columns[{i}][search][value]"];
                if (string.IsNullOrEmpty(prop) || string.IsNullOrEmpty(value))
                    continue;
                list = list.Filter(prop, value);
            }

            var filteredRows = list.Skip(start).Take(length).ToList();
            JsonData jsonData = new JsonData();
            jsonData.draw = draw;
            jsonData.recordsTotal = filteredRows.Count;
            jsonData.recordsFiltered = list.Count;
            jsonData.data = filteredRows;
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSelectList(string q = "", string type = "all")
        {
            List<_Task> tasks = StaticDB.Tasks;
            if (type == "bookmarked")
            {
                List<BookmarkedTask> bookmarkedTasks = new List<BookmarkedTask>();
                using (Repository<BookmarkedTask> Repository = new Repository<BookmarkedTask>())
                {
                    Repository.Include(nameof(BookmarkedTask.Task));
                    bookmarkedTasks = Repository.MultiSelectByQuery(x => x.is_active && x.user_id == CurrentUser.id).ToList();
                }
                bookmarkedTasks.ForEach(x => x.Task.IsBookmarked = true);
                tasks = bookmarkedTasks.Select(x => x.Task).ToList();
            }
            else if (type == "my")
            {
                tasks = tasks.Where(t => t.created_by == CurrentUser.id).ToList();
            }
            List<Select2Model> list = (from x in tasks
                                       where
                                       x.title != null
                                       && x.is_active
                                       && string.IsNullOrEmpty(q)
                                       && string.IsNullOrWhiteSpace(q)
                                       && x.title.ToLower().StartsWith(q.ToLower())
                                       select new Select2Model
                                       {
                                           id = x.id,
                                           text = x.title
                                       }).ToList();

            return Json(new { items = list }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetTasks(string proficiency_level, int age)
        {
            List<_Task> tasks = new List<_Task>();
            using (Repository<_Task> Repository = new Repository<_Task>())
            {
                tasks = Repository.MultiSelectByQuery(s =>
                s.is_active &&
                s.status.Equals(nameof(LOCAL.Enums.StatusTypes.Published)) &&
                age >= s.target_min_age &&
                age <= s.target_max_age &&
                s.target_proficiency_level.Equals(proficiency_level)).ToList();
            }
            return Json(new { results = tasks.Select(t => new { id = t.id, text = t.Readable }) }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Create(string title)
        {
            ViewBag.Title = LOCAL.Objects.Tables.Tasks;
            ViewBag.SubDirectory = LOCAL.Components.Buttons.Create;
            DT.DTO._Task task = new DT.DTO._Task();
            task.title = title;
            task.smartcode = Guid.NewGuid().ToString();
            task.author_id = CurrentUser.id;
            task.created_by = CurrentUser.id;
            task.modified_by = CurrentUser.id;
            task.status = nameof(LOCAL.Enums.StatusTypes.Draft);

            using (Repository<DT.DTO._Task> Repository = new Repository<DT.DTO._Task>())
            {
                task = Repository.Inserted(task);
            }
            return RedirectToAction("AddUpdate", new { id = task.id });
        }
        public ActionResult AddUpdate(int id)
        {
            ViewBag.Title = LOCAL.Objects.Tables.Tasks;
            ViewBag.SubDirectory = id == 0 ? LOCAL.Components.Buttons.Create : LOCAL.Components.Buttons.Edit;
            DT.DTO._Task task = new DT.DTO._Task();

            using (Ctx ctx = new Ctx())
            {
                task = ctx.Tasks.FirstOrDefault(x => x.id == id);
                task.TaskMaterials = ctx.TaskMaterials.Where(x => x.task_id == id).ToList();
                task.TargetLanguages = ctx.TargetLanguages.Where(x => x.task_id == id).ToList();
                task.TaskTags = ctx.TaskTags.Where(x => x.task_id == id).ToList();
                ((List<TaskTag>)task.TaskTags).ForEach(x => x.Tag = ctx.Tags.Find(x.tag_id));
                ((List<TaskMaterial>)task.TaskMaterials).ForEach(x => x.Material = ctx.Materials.Find(x.material_id));
            }

            task.CoDesignerUsers = new List<User>();
            foreach (int designer_id in task.CoDesigners)
            {
                task.CoDesignerUsers.Add(StaticDB.Users.GetById(designer_id));
            }
            ((List<TaskMaterial>)task.TaskMaterials).ForEach(x => x.title = x.Material.title);
            ((List<TaskMaterial>)task.TaskMaterials).ForEach(x => x.type = x.Material.type);
            var tags = from t in task.TaskTags
                       where t.Tag != null && string.IsNullOrEmpty(t.Tag.phrase)
                       select new
                       {
                           value = t.Tag.phrase
                       };
            task.tags = JsonConvert.SerializeObject(tags);
            task.target_languages = task.TargetLanguages.Where(x => x.task_id == id).Select(x => x.language_id).ToArray();
            return View(task);
        }
        public ActionResult Clone(int id)
        {
            DT.DTO._Task task = new DT.DTO._Task();
            using (Repository<DT.DTO._Task> Repository = new Repository<DT.DTO._Task>())
            {
                task = Repository.SingleSelectByQuery(x => x.id == id);
                task.original_id = id;
                task.id = 0;
                task.being_used = false;
                task.status = nameof(LOCAL.Enums.StatusTypes.Draft);
                task.smartcode = Guid.NewGuid().ToString();
                task = Repository.Inserted(task);
            }
            _Task original = StaticDB.Tasks.GetById(id);
            if (original != null && original.created_by != CurrentUser.id)
                DTHelper.SetTaskUsage(id);
            using (Ctx ctx = new Ctx())
            {
                ctx.Database.ExecuteSqlCommand(string.Format(LOCAL.Objects.DBScripts.CloneTask, id, task.id, CurrentUser.id));
            }
            WebUtils.FillStaticDB();
            return RedirectToAction("AddUpdate", new { id = task.id });
        }


        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddUpdate(DT.DTO._Task task)
        {
            try
            {
                var materials = task.TaskMaterials;
                var target_langs = task.target_languages;
                task.co_designers = task._CoDesigners == null || task._CoDesigners.Count == 0 ? "[]" : task._CoDesigners.ToJson();
                task.TaskMaterials = new HashSet<TaskMaterial>();
                task.institution = CurrentUser.institution.ToStr();
                if (!string.IsNullOrEmpty(task.status) && task.status.Equals("Published"))
                    task.publish_date = Zaman.Simdi;
                else
                    task.publish_date = new DateTime(2099, 1, 1);
                using (Repository<DT.DTO._Task> Repository = new Repository<DT.DTO._Task>())
                {
                    if (task.id == 0)
                        task = Repository.Inserted(task);
                    else
                        task = Repository.Updated(task);
                }
                using (Ctx ctx = new Ctx())
                {
                    ctx.Database.ExecuteSqlCommand($"DELETE FROM TargetLanguages WHERE task_id={task.id}; " +
                                            string.Format(LOCAL.Objects.DBScripts.ResetTaskMaterialsUsage, task.id) +
                                            $" DELETE FROM TaskMaterials WHERE task_id={task.id}; " +
                                            $"DELETE FROM TaskTags WHERE task_id={task.id}; ");
                }
                foreach (int language_id in target_langs)
                {
                    using (Repository<TargetLanguage> Repository = new Repository<TargetLanguage>())
                    {
                        TargetLanguage targetLanguage = new TargetLanguage(task.id, language_id, task.target_proficiency_level);
                        Repository.Inserting(targetLanguage);
                    }
                }

                foreach (TaskMaterial taskMaterial in materials)
                {
                    using (Repository<TaskMaterial> Repository = new Repository<TaskMaterial>())
                    {
                        taskMaterial.task_id = task.id;
                        Repository.Inserting(taskMaterial);
                    }
                    DTHelper.SetMaterialUsage(taskMaterial.material_id);
                }
                StaticDB.SetProperty(nameof(Material));
                try
                {
                    TagifyValueModel[] tags = JsonConvert.DeserializeObject<TagifyValueModel[]>(task.tags);
                    foreach (TagifyValueModel tt in tags.Where(t => !string.IsNullOrEmpty(t.value) && !string.IsNullOrWhiteSpace(t.value)))
                    {
                        string strT = tt.value.Trim().ToLower();
                        Tag tag = StaticDB.Tags.FirstOrDefault(t => t.phrase.TrimEnd().ToLower().Equals(strT));
                        if (tag == null)
                            using (Repository<Tag> Repository = new Repository<Tag>())
                            {
                                tag = new Tag();
                                tag.phrase = strT;
                                tag = Repository.Inserted(tag);
                            }
                        using (Repository<TaskTag> Repository = new Repository<TaskTag>())
                        {
                            TaskTag taskTag = new TaskTag();
                            taskTag.tag_id = tag.id;
                            taskTag.task_id = task.id;
                            Repository.Inserting(taskTag);
                        }
                    }
                }
                catch (Exception tex)
                {
                    Logger.Save("Task Tags Error", tex);
                }
                return Json(new { Result = true });
            }
            catch (Exception ex)
            {
                Logger.Save("Task Save Error", ex);
                return Json(new { Result = false, Message = ex });
            }
        }


        [HttpPost]
        public ActionResult Bookmark(int id)
        {
            try
            {
                _Task task = null;
                using (Repository<_Task> Repository = new Repository<_Task>())
                {
                    task = Repository.SingleSelectByQuery(m => m.id == id);
                }
                if (task == null)
                    return Json(new { Result = false, Message = LOCAL.Messages.Warnings.RecordNotFound });

                BookmarkedTask bookmarkedTask = null;
                bool status = false;
                using (Repository<BookmarkedTask> Repository = new Repository<BookmarkedTask>())
                {
                    bookmarkedTask = Repository.SingleSelectByQuery(m => m.task_id == id && m.user_id == CurrentUser.id && m.is_active);
                    if (bookmarkedTask == null)
                    {
                        bookmarkedTask = new BookmarkedTask();
                        bookmarkedTask.task_id = id;
                        bookmarkedTask.user_id = CurrentUser.id;
                        bookmarkedTask = Repository.Inserted(bookmarkedTask);
                        status = true;
                    }
                    else
                    {
                        Repository.Deleting(bookmarkedTask);
                        status = false;
                    }
                }
                return Json(new { Result = true, Bookmarked = status });
            }
            catch (Exception ex)
            {
                return Json(new { Result = false, Message = ex });
            }
        }

        public ActionResult Details(int id)
        {
            _Task task = new _Task();
            using (Ctx ctx = new Ctx())
            {
                task = ctx.Tasks.FirstOrDefault(x => x.id == id && x.is_active);
                if (task != null)
                {
                    task.TaskMaterials = ctx.TaskMaterials.Where(t => t.is_active && t.task_id == id).ToList();
                    ((List<TaskMaterial>)task.TaskMaterials).ForEach(x => x.Material = ctx.Materials.Find(x.material_id));
                }
            }
            if (task == null)
                return PartialView("~/Views/Shared/NullModal.cshtml");
            return PartialView("~/Views/Front/_TaskPreviewModal.cshtml", task);
        }
        public ActionResult TaskDetails(int id)
        {
            _Task task = new _Task();
            using (Ctx ctx = new Ctx())
            {
                task = ctx.Tasks.FirstOrDefault(x => x.id == id && x.is_active);
                if (task != null)
                {
                    task.TaskMaterials = ctx.TaskMaterials.Where(t => t.is_active && t.task_id == id).ToList();
                    ((List<TaskMaterial>)task.TaskMaterials).ForEach(x => x.Material = ctx.Materials.Find(x.material_id));
                }
            }
            if (task == null)
                return PartialView("~/Views/Shared/NullModal.cshtml");
            return PartialView("~/Views/Front/_TaskPreviewBody.cshtml", task);
        }
        public ActionResult Menu(int id)
        {
            _Task task = new _Task();
            using (Ctx ctx = new Ctx())
            {
                task = ctx.Tasks.FirstOrDefault(x => x.id == id);
            }
            task.UserCanEdit = (task.created_by == CurrentUser.id && !task.being_used) || CurrentUser.role_id == 1 || CurrentUser.role_id == 2;
            if (task == null)
                return PartialView("~/Views/Shared/NullModal.cshtml");
            return PartialView("_MenuPartial", task);
        }
        public ActionResult Delete(int id)
        {

            using (Repository<_Task> Repository = new Repository<_Task>())
            {
                _Task task = Repository.SingleSelectByQuery(x => x.id == id && x.is_active);
                Repository.SoftDeleting(task);
            }
            return RedirectToAction("Index");
        }
        public ActionResult PreviewModal(int id)
        {
            List<TaskMaterial> tms = new List<TaskMaterial>();
            using (Ctx ctx = new Ctx())
            {
                tms = ctx.TaskMaterials.Where(x => x.is_active && x.task_id == id).ToList();
                tms.ForEach(x => x.Material = ctx.Materials.FirstOrDefault(a => x.material_id == a.id));
            }
            return PartialView("_PreviewModal", tms);
        }
        public ActionResult GenerateShareButtons(int id)
        {
            _Task task = StaticDB.Tasks.GetById(id);
            return PartialView("_ShareButtons", task);
        }
        public ActionResult Preview(string smartcode)
        {
            try
            {
                ViewBag.Title = LOCAL.Objects.Tables.Tasks;
                ViewBag.SubDirectory = LOCAL.Components.Buttons.Preview;
                DT.DTO._Task task = new DT.DTO._Task();
                using (Ctx ctx = new Ctx())
                {
                    task = ctx.Tasks.FirstOrDefault(x => x.smartcode.Equals(smartcode));
                    task.TaskMaterials = ctx.TaskMaterials.Where(x => x.task_id == task.id).ToList();
                    ((List<TaskMaterial>)task.TaskMaterials).ForEach(x => x.Material = ctx.Materials.Find(x.material_id));
                }
                ((List<TaskMaterial>)task.TaskMaterials).ForEach(x => x.title = x.Material.title);
                ((List<TaskMaterial>)task.TaskMaterials).ForEach(x => x.type = x.Material.type);
                if (task != null)
                    return View(task);
            }
            catch (Exception ex)
            {

            }
            return Redirect(Request.UrlReferrer.ToStr());
        }
        [HttpPost]
        public ActionResult InstantPreview(List<TaskMaterial> tms)
        {
            if (tms == null)
                tms = new List<TaskMaterial>();
            using (Ctx ctx = new Ctx())
            {
                tms.ForEach(x => x.Material = ctx.Materials.FirstOrDefault(a => x.material_id == a.id));
            }
            return PartialView("_PreviewBodyPartial", tms);
        }

        public ActionResult GetRelatedInfo(int id)
        {
            _Task task = StaticDB.AllTasks.GetById(id);
            task.BookmarkedTasks = StaticDB.BookmarkedTasks.Where(x => x.task_id == id).ToList();
            task.BookmarkedTasks.ToList().ForEach(x => x.User = StaticDB.Users.GetById(x.user_id));
            task.SequenceTasks = StaticDB.SequenceTasks.Where(x => x.task_id == id).ToList();
            task.SequenceTasks.ToList().ForEach(x => x.Sequence = StaticDB.Sequences.GetById(x.sequence_id));
            task.SessionTasks = StaticDB.SessionTasks.Where(x => x.task_id == id).ToList();
            task.TaskActions = StaticDB.TaskActions.Where(x => x.task_id == id).ToList();
            task.TaskEditors = StaticDB.TaskEditors.Where(x => x.task_id == id).ToList();
            task.TaskEditors.ToList().ForEach(x => x.User = StaticDB.Users.GetById(x.editor_id));
            task.TaskMaterials = StaticDB.TaskMaterials.Where(x => x.task_id == id).ToList();
            task.TaskTags = StaticDB.TaskTags.Where(x => x.task_id == id).ToList();
            return PartialView("_RelatedPartial", task);
        }

        public ActionResult HardDelete(int id)
        {
            using (Ctx ctx = new Ctx())
            {
                string deleteQuery = string.Format(LOCAL.Objects.DBScripts.HardDeleteTask, id);
                ctx.Database.ExecuteSqlCommand(deleteQuery);
            }
            WebUtils.FillStaticDB();
            return Redirect(Request.UrlReferrer.ToStr());
        }

        public ActionResult RecoverTask(int id)
        {
            using (Repository<_Task> Repository = new Repository<_Task>())
            {
                _Task task = Repository.SingleSelectByQuery(x => x.id == id);
                task.is_active = true;
                Repository.Updated(task);
            }
            return RedirectToAction("Index");
        }
    }
}
