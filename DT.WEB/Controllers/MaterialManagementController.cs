﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using DT.DAL;
using DT.DTO;
using DT.WEB.Models;
using MS.BLL;
using Newtonsoft.Json;

namespace DT.WEB.Controllers
{
    public class MaterialManagementController : BaseController
    {
        public ActionResult Index(string type = "all")
        {
            ViewBag.Title = LOCAL.Objects.Tables.Materials;
            List<Material> materials = new List<Material>();
            List<int> bookmarkedMaterialIds = new List<int>();
            ViewBag.Type = type;
            if (type.Equals("all"))
            {
                using (Ctx ctx = new Ctx())
                {
                    materials = ctx.Materials.Where(x => x.is_active).ToList();
                    bookmarkedMaterialIds = ctx.BookmarkedMaterials.Where(b => b.user_id == CurrentUser.id).Select(b => b.material_id).ToList();
                }
                materials.ForEach(m => m.IsBookmarked = bookmarkedMaterialIds.Contains(m.id));
                ViewBag.CanEdit = false;
                ViewBag.SubDirectory = LOCAL.Components.WebMenuItems.All_Materials;
                return View(materials);
            }
            else if (type.Equals("bookmarked"))
            {
                List<BookmarkedMaterial> bookmarkedMaterials = new List<BookmarkedMaterial>();
                using (Repository<BookmarkedMaterial> Repository = new Repository<BookmarkedMaterial>())
                {
                    Repository.Include(nameof(BookmarkedMaterial.Material));
                    bookmarkedMaterials = Repository.MultiSelectByQuery(x => x.is_active && x.user_id == CurrentUser.id).ToList();
                }
                bookmarkedMaterials.ForEach(x => x.Material.IsBookmarked = true);
                ViewBag.CanEdit = false;
                ViewBag.SubDirectory = LOCAL.Components.WebMenuItems.Bookmarked_Materials;
                return View(bookmarkedMaterials.Select(x => x.Material).ToList());
            }
            else
            {
                using (Ctx ctx = new Ctx())
                {
                    materials = ctx.Materials.Where(t => t.is_active &&
                    t.created_by == CurrentUser.id).ToList();
                    bookmarkedMaterialIds = ctx.BookmarkedMaterials.Where(b => b.user_id == CurrentUser.id).Select(b => b.material_id).ToList();
                }
                materials.ForEach(m => m.IsBookmarked = bookmarkedMaterialIds.Contains(m.id));
                ViewBag.CanEdit = true;
                ViewBag.SubDirectory = LOCAL.Components.WebMenuItems.My_Materials;
                return View(materials);
            }
        }
        [HttpPost]
        public ActionResult GetList(string type = "all")
        {
            int draw = Convert.ToInt32(Request.Form["draw"]);// etkin sayfa numarası
            int start = Convert.ToInt32(Request["start"]);//listenen ilk kayıtın  index numarası
            int length = Convert.ToInt32(Request["length"]);//sayfadaki toplam listelenecek kayit sayısı
            string search = Request["search[value]"];//arama
            string sortColumnName = Request["columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]"];//Sıralama yapılacak column adı
            string sortDirection = Request["order[0][dir]"];//sıralama türü
            sortColumnName = string.IsNullOrEmpty(sortColumnName) ? "created_date" : sortColumnName;


            List<Material> materials = new List<Material>();
            List<int> bookmarkedMaterialIds = new List<int>();
            using (Ctx ctx = new Ctx())
            {
                materials = ctx.Materials.Where(x => x.is_active).ToList();
                bookmarkedMaterialIds = ctx.BookmarkedMaterials.Where(b => b.user_id == CurrentUser.id).Select(b => b.material_id).ToList();
            }
            materials.ForEach(m => m.IsBookmarked = bookmarkedMaterialIds.Contains(m.id));
            materials.ForEach(m => m.Creator = StaticDB.Users.GetById(m.created_by).ToStr());
            materials.ForEach(m => m.details = m.created_date.ToShortDateString());
            if (type == "bookmarked")
                materials = materials.Where(b => b.IsBookmarked).ToList();
            else if (type == "my")
                materials = materials.Where(x => x.created_by == CurrentUser.id).ToList();

            List<Material> list = new List<Material>();
            if (sortColumnName.Equals("details"))
            {
                if (sortDirection != "asc")
                    list = materials.OrderByDescending(m => m.created_date).ToList();
                else
                    list = materials.OrderBy(m => m.created_date).ToList();
            }
            else
            {
                list = materials.AsQueryable().OrderByDynamic(sortColumnName, sortDirection != "asc").ToList();
            }
            if (search == null)
                search = "";

            if (!string.IsNullOrEmpty(search))
            {
                List<Material> newList = new List<Material>();
                foreach (var item in list)
                {
                    foreach (PropertyInfo property in typeof(Material).GetProperties())
                    {
                        var value = property.GetValue(item);
                        if (value != null && value.ToStr().ToLower().Contains(search.ToLower()))
                        {
                            newList.Add(item);
                        }
                    }
                }
                list = newList;
            }
            for (int i = 1; i < 5; i++)
            {
                string prop = Request.Form[$"columns[{i}][data]"];
                if (prop.Equals("function"))
                    prop = Request.Form[$"columns[{i}][name]"];
                string value = Request.Form[$"columns[{i}][search][value]"];
                if (string.IsNullOrEmpty(prop) || string.IsNullOrEmpty(value))
                    continue;
                list = list.Filter(prop, value);
            }


            var filteredRows = list.Skip(start).Take(length).ToList();
            JsonData jsonData = new JsonData();
            jsonData.draw = draw;
            jsonData.recordsTotal = filteredRows.Count;
            jsonData.recordsFiltered = list.Count;
            jsonData.data = filteredRows;
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetMaterial(int id)
        {
            Material material = StaticDB.Materials.GetById(id);
            return Json(new { Result = material != null, Material = material }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeletedIndex()
        {
            ViewBag.Title = LOCAL.Objects.Tables.Tasks + " " + LOCAL.Components.WebMenuItems.DeletedMaterialsList;
            ViewBag.SubDirectory = LOCAL.Components.WebMenuItems.DeletedMaterialsList;
            if (ViewBag.Admin = (CurrentUser.role_id == 1 || CurrentUser.role_id == 2))
            {
                return View(StaticDB.DeletedMaterials);
            }
            else
            {
                return View(StaticDB.DeletedMaterials.Where(t => t.created_by == CurrentUser.id).ToList());
            }
        }
        public ActionResult Selector()
        {
            return PartialView("_MaterialSelector");
        }
        public ActionResult GetMaterialBody(int material_id)
        {
            Material material = StaticDB.Materials.GetById(material_id);
            return PartialView("_MaterialBody", material);
        }
        public ActionResult AddUpdate(int id = 0, bool clone = false)
        {
            if (id == 0 && !clone)
                return PartialView("_CreatePartialModal", new Material());
            else if (id != 0 && clone)
            {
                Material cloned = new Material();
                using (Repository<Material> Repository = new Repository<Material>())
                {
                    cloned = Repository.SingleSelectByQuery(m => m.id == id);
                    cloned.id = 0;
                    cloned.created_by = CurrentUser.id;
                    cloned.modified_by = CurrentUser.id;
                    cloned.title += " (COPIED)";
                    cloned = Repository.Inserted(cloned);
                }
                return PartialView("_CreatePartialModal", cloned);
            }
            else
            {
                Material material = StaticDB.Materials.GetById(id);
                if (material.created_by == CurrentUser.id || CurrentUser.role_id == 1 || CurrentUser.role_id == 2)
                    return PartialView("_CreatePartialModal", StaticDB.Materials.GetById(id));
                else
                    return PartialView("_CreatePartialModal", new Material());
            }
        }
        [HttpPost]
        public ActionResult AddUpdate(Material material)
        {
            if (CurrentUser.role_id > 3 && material.id != 0 && material.created_by != CurrentUser.id)
                return Json(new { Result = false, Message = LOCAL.Messages.Warnings.NotAuthorized });
            string result = null;// CheckMaterial(material.body);
            if (result == null)
            {
                using (Repository<Material> Repository = new Repository<Material>())
                {
                    if (material.id == 0)
                        material = Repository.Inserted(material);
                    else
                        material = Repository.Updated(material);
                }
            }
            return Json(new { Result = result == null, Message = result.ToStr() });
        }

        private string CheckMaterial(string body)
        {
            List<string> YIds = GetYoutubeIds(body);
            int notCCCount = 0;
            foreach (string id in YIds)
            {
                if (!IsYoutubeCC(id))
                    ++notCCCount;
            }
            return notCCCount == 0 ? null : $"According to YouTube records, your material's content has {notCCCount} not creative common licenced video(s). You can use only creative common videos.";
        }

        private List<string> GetYoutubeIds(string body)
        {
            var rx = new Regex(@"^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$", RegexOptions.Compiled);
            MatchCollection matches = rx.Matches(body);
            List<string> strList = new List<string>();
            if (matches != null && matches.Count > 0)
            {
                foreach (var item in matches)
                    strList.Add(item.ToStr());
            }
            return strList;
        }

        public ActionResult UpdateOrder(int id, bool is_order = true)
        {
            ViewBag.IndexOrder = is_order;
            TaskMaterial material = null;
            using (Repository<TaskMaterial> Repository = new Repository<TaskMaterial>())
            {
                material = Repository.SingleSelectByQuery(m => m.id == id);
            }
            if (material == null)
            {
                var materials = Cacher.Get<List<TaskMaterial>>("TaskMaterials");
                material = materials.GetById(id);
            }
            return PartialView("_UpdateOrderPartial", material);
        }
        [HttpPost]
        public ActionResult UpdateOrder(int material_id, int value, bool is_order = true)
        {
            try
            {
                TaskMaterial material = null;
                using (Repository<TaskMaterial> Repository = new Repository<TaskMaterial>())
                {
                    material = Repository.SingleSelectByQuery(m => m.id == material_id);
                    if (material != null)
                    {
                        if (is_order)
                            material.index_order = value;
                        else
                            material.activation_time = value;
                        material = Repository.Updated(material);
                    }
                }
                if (material == null)
                {
                    var materials = Cacher.Get<List<TaskMaterial>>("TaskMaterials");
                    if (is_order)
                        materials.GetById(material_id).index_order = value;
                    else
                        materials.GetById(material_id).activation_time = value;
                    Cacher.Save("TaskMaterials", materials);
                }
                return Json(new { Result = true });
            }
            catch (Exception ex)
            {
                return Json(new { Result = false, Message = ex });
            }
        }
        public ActionResult Details(int id, string function = "Details")
        {
            Material material = new Material();
            List<int> bookmarkedMaterialIds = new List<int>();
            using (Ctx ctx = new Ctx())
            {
                material = ctx.Materials.Find(id);
                material.IsBookmarked = ctx.BookmarkedMaterials.FirstOrDefault(b => b.user_id == CurrentUser.id && b.material_id == id) != null;
            }

            if (function.Equals("Details"))
                return PartialView("_DetailPartialModal", material);
            else
                return PartialView("_TaskDetailPartialModal", material);
        }
        public ActionResult DetailsData(int id)
        {
            Material material = new Material();
            List<int> bookmarkedMaterialIds = new List<int>();
            using (Ctx ctx = new Ctx())
            {
                material = ctx.Materials.Find(id);
                material.IsBookmarked = ctx.BookmarkedMaterials.FirstOrDefault(b => b.user_id == CurrentUser.id && b.material_id == id) != null;
            }
            return PartialView("_DetailPartial", material);
        }
        [HttpPost]
        public ActionResult Bookmark(int id)
        {
            try
            {
                Material material = null;
                using (Repository<Material> Repository = new Repository<Material>())
                {
                    material = Repository.SingleSelectByQuery(m => m.id == id);
                }
                if (material == null)
                    return Json(new { Result = false, Message = LOCAL.Messages.Warnings.RecordNotFound });

                BookmarkedMaterial bookmarkedMaterial = null;
                bool status = false;
                using (Repository<BookmarkedMaterial> Repository = new Repository<BookmarkedMaterial>())
                {
                    bookmarkedMaterial = Repository.SingleSelectByQuery(m => m.material_id == id && m.user_id == CurrentUser.id && m.is_active);
                    if (bookmarkedMaterial == null)
                    {
                        bookmarkedMaterial = new BookmarkedMaterial();
                        bookmarkedMaterial.material_id = id;
                        bookmarkedMaterial.user_id = CurrentUser.id;
                        bookmarkedMaterial = Repository.Inserted(bookmarkedMaterial);
                        status = true;
                    }
                    else
                    {
                        Repository.Deleting(bookmarkedMaterial);
                    }
                }

                DTHelper.SetMaterialUsage(material.id);
                StaticDB.SetProperty(nameof(Material));
                return Json(new { Result = true, Bookmarked = status });
            }
            catch (Exception ex)
            {
                return Json(new { Result = false, Message = ex });
            }
        }

        public ActionResult Delete(int id, string type)
        {

            using (Repository<Material> Repository = new Repository<Material>())
            {
                Material material = Repository.SingleSelectByQuery(x => x.id == id && x.is_active);
                Repository.SoftDeleting(material);
            }

            return RedirectToAction("Index", new { type });
        }
        [HttpPost]
        public ActionResult CheckYoutubeLicence(string video_id)
        {
            try
            {
                return Json(new { Result = IsYoutubeCC(video_id), Message = LOCAL.Messages.Warnings.YoutubeNotCC }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = false, Message = LOCAL.Messages.Warnings.YoutubeApiError }, JsonRequestBehavior.AllowGet);
            }
        }
        public bool IsYoutubeCC(string video_id)
        {
            using (WebClient client = new WebClient())
            {
                string url = $"https://www.googleapis.com/youtube/v3/videos?id={video_id}&key={LOCAL.ApplicationInfo.YoutubeAPIKey}&part=status";
                string strResponse = client.DownloadString(url);
                if (!string.IsNullOrEmpty(strResponse))
                {
                    YoutubeResponseModel response = JsonConvert.DeserializeObject<YoutubeResponseModel>(strResponse);
                    if (response != null && response.items.Count > 0)
                    {
                        string license = response.items[0].status.license;
                        return license.Equals("creativeCommon");
                    }
                }
            }
            return false;
        }
        public ActionResult GetRelatedInfo(int id)
        {
            Material task = StaticDB.AllMaterials.GetById(id);
            task.BookmarkedMaterials = StaticDB.BookmarkedMaterials.Where(x => x.material_id == id).ToList();
            task.BookmarkedMaterials.ToList().ForEach(x => x.User = StaticDB.Users.GetById(x.user_id));
            task.TaskMaterials = StaticDB.TaskMaterials.Where(x => x.material_id == id).ToList();
            task.TaskMaterials.ToList().ForEach(x => x.Task = StaticDB.AllTasks.GetById(x.task_id));
            return PartialView("_RelatedPartial", task);
        }

        public ActionResult HardDelete(int id)
        {
            using (Ctx ctx = new Ctx())
            {
                string deleteQuery = string.Format(LOCAL.Objects.DBScripts.HardDeleteMaterial, id);
                ctx.Database.ExecuteSqlCommand(deleteQuery);
            }
            WebUtils.FillStaticDB();
            return Redirect(Request.UrlReferrer.ToStr());
        }
        public ActionResult RecoverMaterial(int id)
        {
            using (Repository<Material> Repository = new Repository<Material>())
            {
                Material material = Repository.SingleSelectByQuery(x => x.id == id);
                material.is_active = true;
                Repository.Updated(material);
            }
            return RedirectToAction("Index");
        }
    }
}