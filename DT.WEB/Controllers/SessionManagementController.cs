﻿using DT.DAL;
using DT.DTO;
using MS.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DT.WEB.Controllers
{
    public class SessionManagementController : BaseController
    {
        public ActionResult Start(string code)
        {
            ViewBag.Title = "Task Screen";
            string[] decodedCode = code.FromBase64().Split(';');
            string struser_id = decodedCode[0].Split('=')[1];
            string strsession_id = decodedCode[1].Split('=')[1];
            int user_id = struser_id.ToInteger();
            int session_id = strsession_id.ToInteger();
            if (user_id != CurrentUser.id || session_id == 0)
            {
                Logger.Save("USER does not match code", new UnauthorizedAccessException($"USER ID:{user_id}, CURRENT_USER : {CurrentUser.ToJson()}"));
                return RedirectToAction("Index", "Home");
            }
            SessionTask sessionTask = GetSessionTask(session_id);

            return View("Start",sessionTask);
        }
        [HttpPost]
        public ActionResult TaskMaterialSource(int task_id, int index_order)
        {
            List<TaskMaterial> taskMaterials = new List<TaskMaterial>();
            using (Ctx ctx = new Ctx())
            {
                taskMaterials = ctx.TaskMaterials.Where(x => x.task_id == task_id && x.index_order == index_order && x.is_active).ToList();
                taskMaterials.ForEach(x => x.Material = ctx.Materials.FirstOrDefault(a => a.id == x.material_id));
            }
            return PartialView("_TaskMaterialsBodyPartial", taskMaterials);
        }
        private static SessionTask GetSessionTask(int session_id)
        {
            SessionTask sessionTask = new SessionTask();
            using (Repository<SessionTask> Repository = new Repository<SessionTask>())
            {
                Repository.Include(nameof(SessionTask.Task));
                Repository.Include(nameof(SessionTask.Class));
                Repository.Include(nameof(SessionTask.Cluster));
                Repository.Include(nameof(SessionTask.Room));
                sessionTask = Repository.SingleSelectByQuery(s => s.id == session_id && s.is_active);
                if (sessionTask.planned_start_time < Zaman.Simdi && sessionTask.start_time == null)
                    sessionTask.start_time = Zaman.Simdi;

                sessionTask = Repository.Updated(sessionTask);
            }
            sessionTask.Task = StaticDB.Tasks.GetById(sessionTask.task_id);
            sessionTask.Task.TaskMaterials = StaticDB.TaskMaterials.Where(x => x.task_id == sessionTask.task_id).ToList();
            sessionTask.Task.TaskMaterials.ToList().ForEach(x => x.Material = StaticDB.Materials.GetById(x.material_id));
            using (Repository<SessionTaskParticipant> Repository = new Repository<SessionTaskParticipant>())
            {
                sessionTask.SessionTaskParticipants = Repository.MultiSelectByQuery(x => x.session_task_id == sessionTask.id && x.is_active).ToList();

            }
            using (Repository<ParticipantLog> Repository = new Repository<ParticipantLog>())
            {
                sessionTask.SessionTaskParticipants.ToList().ForEach(s => s.ParticipantLogs = Repository.MultiSelectByQuery(a => a.session_task_participant_id == s.participant_id).ToList());

            }
            sessionTask.SessionTaskParticipants.ToList()
                .ForEach(s => s.User = StaticDB.Users.GetById(s.participant_id));

            return sessionTask;
        }

        public ActionResult StartWithMessage(int user_id, int session_id)
        {
            ViewBag.Title = "Task Screen";
            if (user_id != CurrentUser.id)
            {
                Logger.Save("USER does not match code", new UnauthorizedAccessException($"USER ID:{user_id}, CURRENT_USER : {CurrentUser.ToJson()}"));
                return Redirect(Request.UrlReferrer.ToString());
            }
            SessionTask sessionTask = GetSessionTask(session_id);
            return View("Start", sessionTask);
        }
        public ActionResult StartTask(int user_id, int session_task_id)
        {
            SessionTask sessionTask = null;
            using (Repository<SessionTask> Repository =new Repository<SessionTask>())
            {
                sessionTask = Repository.SingleSelectByQuery(s => s.id == session_task_id && s.is_active);
            }
            if (sessionTask == null)
                return Redirect(Request.UrlReferrer.ToString());
            return RedirectToAction(nameof(StartWithMessage),new {user_id = user_id, session_id = sessionTask.id });
        }
        public ActionResult StartSingleTask(string smartcode)
        {
            return View();
        }

        public ActionResult Delete(int id)
        {

            using (Repository<SessionTask> Repository = new Repository<SessionTask>())
            {
                SessionTask sessionTask = Repository.SingleSelectByQuery(x => x.id == id && x.is_active);
                Repository.SoftDeleting(sessionTask);
            }

            return RedirectToAction("AssignmentIndex", "TaskResultManagement");
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Review(Rating rating)
        {
            try
            {
                using (Repository<Rating> Repository = new Repository<Rating>())
                {
                    rating = Repository.Inserted(rating);
                }
            }
            catch (Exception ex)
            {
                Logger.Save("Rating Error", ex);
            }
            return RedirectToAction("Index", "Student");
        }
    }
}