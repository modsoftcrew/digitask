﻿using DT.DAL;
using DT.DTO;
using MS.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DT.WEB.Controllers
{
    public class ClassManagementController : BaseController
    {
        // GET: ClassManagement
        public ActionResult Index()
        {
            ViewBag.Title = LOCAL.Objects.Tables.Classes;
            ViewBag.SubDirectory = LOCAL.Components.WebMenuItems.My_Classes;
            List<_Class> classes = new List<_Class>();
            if (CurrentUser.role_id == 1 || CurrentUser.role_id == 2)
            {
                using (Repository<DT.DTO._Class> Repository = new Repository<DT.DTO._Class>())
                {
                    Repository.Include(nameof(_Class.ClassStudents), nameof(_Class.Clusters));
                    classes = Repository.MultiSelectByQuery(x => x.is_active).ToList();
                }
                using (Ctx ctx = new Ctx())
                {
                    classes.ForEach(_class => _class.ClassStudents.ToList().ForEach(x => x.User = ctx.Users.Find(x.student_id)));
                    classes.ForEach(_class => _class.Clusters.ToList().ForEach(x => x.ClusterStudents = ctx.ClusterStudents.Where(s => s.cluster_id == x.id).ToList()));
                }
                return View(classes);
            }
            else if (CurrentUser.role_id == 3)
            {
                using (Repository<DT.DTO._Class> Repository = new Repository<DT.DTO._Class>())
                {
                    Repository.Include(nameof(_Class.ClassStudents), nameof(_Class.Clusters));
                    classes = Repository.MultiSelectByQuery(t =>
                    t.is_active && t.created_by == CurrentUser.id).ToList();
                }
                using (Ctx ctx = new Ctx())
                {
                    classes.ForEach(_class => _class.ClassStudents.ToList().ForEach(x => x.User = ctx.Users.Find(x.student_id)));
                    classes.ForEach(_class => _class.Clusters.ToList().ForEach(x => x.ClusterStudents = ctx.ClusterStudents.Where(s => s.cluster_id == x.id).ToList()));
                }
                return View(classes);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult AddUpdate(int id = 0, bool just_student = false)
        {
            ViewBag.Title = (id==0?"Add ":"Update ") + LOCAL.Objects.Entity._Class;
            DT.DTO._Class _class = new DT.DTO._Class();
            _class.teacher_id = CurrentUser.id;
            ViewBag.JustStudent = just_student;
            if (id != 0)
            {
                using (Repository<DT.DTO._Class> Repository = new Repository<DT.DTO._Class>())
                {
                    Repository.Include(nameof(_Class.ClassStudents));
                    _class = Repository.SingleSelectByQuery(x => x.id == id);
                }
                _class.ClassStudents.ToList().ForEach(x => x.User = StaticDB.Users.GetById(x.student_id));
            }
            _class.students = _class.ClassStudents.Select(c => c.student_id).ToArray();
            return PartialView("_AddUpdatePartialModal", _class);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddUpdate(DT.DTO._Class _class)
        {
            ViewBag.Title = (_class.id == 0 ? "Add " : "Update ") + LOCAL.Objects.Entity._Class;
            try
            {
                _class.max_student_limit = _class.students.Length;
                using (Repository<DT.DTO._Class> Repository = new Repository<DT.DTO._Class>())
                {
                    if (_class.id == 0)
                        _class = Repository.Inserted(_class);
                    else
                    {
                        _class = Repository.Updated(_class);
                    }

                }
                using (Repository<DT.DTO._Class> Repository = new Repository<DT.DTO._Class>())
                {
                    Repository.ExecuteSQL($"DELETE FROM ClassStudents WHERE class_id={_class.id}");

                }
                foreach (int student_id in _class.students)
                {
                    using (Repository<ClassStudent> Repository = new Repository<ClassStudent>())
                    {
                        ClassStudent entity = new ClassStudent();
                        entity.class_id = _class.id;
                        entity.student_id = student_id;
                        Repository.Inserting(entity);
                    }
                }
                return Json(new { Result = true });
            }
            catch (Exception ex)
            {
                return Json(new { Result = false, Message = ex });
            }
        }

        public ActionResult Clustering(int id)
        {
            _Class @class = new _Class();
            using (Repository<_Class> Repository = new Repository<_Class>())
            {
                Repository.Include(nameof(_Class.Clusters));
                StaticDB.Classes = Repository.MultiSelectByQuery(c => c.is_active).ToList();
                @class = StaticDB.Classes.GetById(id);
            }
            return PartialView("_AddUpdateClusterPartialModal", @class);
        }
        [HttpPost]
        public ActionResult ClusterSelectedStudents(int id, ClassStudent[] students)
        {
            Cluster cluster = new Cluster();
            using (Repository<Cluster> Repository = new Repository<Cluster>())
            {
                cluster.class_id = id;
                cluster.title = $"{LOCAL.Objects.Entity.Cluster}-{StaticDB.Clusters.Count(c=>c.class_id == id) + 1}";
                System.Drawing.Color color = Utilities.getRandomColor();
                cluster.color_code = "#" + color.R.ToString("X2") + color.G.ToString("X2") + color.B.ToString("X2");
                cluster = Repository.Inserted(cluster);
            }
            foreach (ClassStudent student in students)
            {
                using (Repository<ClusterStudent> Repository = new Repository<ClusterStudent>())
                {
                    ClusterStudent clusterStudent = new ClusterStudent();
                    clusterStudent.cluster_id = cluster.id;
                    clusterStudent.class_student_id = student.student_id;
                    Repository.Inserting(clusterStudent);
                }
            }
            _Class @class = new _Class();
            using (Repository<_Class> Repository = new Repository<_Class>())
            {
                Repository.Include(nameof(_Class.Clusters));
                Repository.Include(nameof(_Class.ClassStudents));
                @class = Repository.SingleSelectByQuery(c => c.id == id);
            }
            @class.ClassStudents.ToList().ForEach(c => c.User = StaticDB.Users.GetById(c.student_id));
            @class.Clusters = StaticDB.Clusters.Where(c => c.class_id == id).ToList();
            @class.Clusters.ToList().ForEach(x => x.ClusterStudents = StaticDB.ClusterStudents.Where(s => s.cluster_id == x.id).ToList());
            return PartialView("_ClassBodyPartial", @class);
        }
        [HttpPost]
        public ActionResult DeleteSelectedStudents(int id, ClassStudent[] students)
        {
            string clusterStudentDeleteQuery = "", classStudentDeleteQuery = "";
            foreach (ClassStudent classStudent in students)
            {
                clusterStudentDeleteQuery += $"DELETE CS FROM ClusterStudents CS " +
                    $"LEFT JOIN Clusters C on C.id = CS.cluster_id " +
                    $"WHERE CS.class_student_id = {classStudent.student_id} AND C.class_id = {id};{Environment.NewLine}";
                classStudentDeleteQuery += $"DELETE FROM ClassStudents WHERE id={classStudent.student_id};{Environment.NewLine}";
            }
            using (Repository<Cluster> Repository = new Repository<Cluster>())
            {
                Repository.ExecuteSQL(clusterStudentDeleteQuery);
                Repository.ExecuteSQL(classStudentDeleteQuery);
            }
            StaticDB.SetProperty(nameof(Cluster));
            StaticDB.SetProperty(nameof(ClusterStudent));
            StaticDB.SetProperty(nameof(ClassStudent));
            _Class @class = new _Class();
            using (Repository<_Class> Repository = new Repository<_Class>())
            {
                Repository.Include(nameof(_Class.Clusters));
                Repository.Include(nameof(_Class.ClassStudents));
                @class = Repository.SingleSelectByQuery(c => c.id == id);
            }
            @class.ClassStudents.ToList().ForEach(c => c.User = StaticDB.Users.GetById(c.student_id));
            @class.Clusters = StaticDB.Clusters.Where(c => c.class_id == id).ToList();
            @class.Clusters.ToList().ForEach(x => x.ClusterStudents = StaticDB.ClusterStudents.Where(s => s.cluster_id == x.id).ToList());
            return PartialView("_ClassBodyPartial", @class);
        }
        [HttpPost]
        public ActionResult GetClusterStudents(int id)
        {
            List<ClusterStudent> students = new List<ClusterStudent>();
            using (Repository<ClusterStudent> Repository = new Repository<ClusterStudent>())
            {
                Repository.Include(nameof(ClusterStudent.ClassStudent));
                students = Repository.MultiSelectByQuery(c => c.cluster_id == id).ToList();
            }
            students.ForEach(s => s.ClassStudent.User = StaticDB.Users.GetById(s.ClassStudent.student_id));
            return PartialView("_ClusterStudentsPartial", students);
        }
        [HttpPost]
        public ActionResult Clustering(int id, string type, int value)
        {
            _Class currentClass = StaticDB.Classes.GetById(id);
            currentClass.ClassStudents = StaticDB.ClassStudents.Where(c => c.class_id == id).ToList();
            decimal d = Convert.ToDecimal(currentClass.ClassStudents.Count) / Convert.ToDecimal(value);
            int size = (int)Math.Floor(d);
            List<Cluster> clusterList = new List<Cluster>();
            using (Repository<Cluster> Repository = new Repository<Cluster>())
            {
                Repository.ExecuteSQL($"DELETE CS FROM ClusterStudents CS " +
                    $"LEFT JOIN Clusters C on C.id = CS.cluster_id " +
                    $"WHERE C.class_id = {id};");
                Repository.ExecuteSQL($"DELETE FROM Clusters WHERE class_id={id};");
            }
            for (int i = 0; i < (type.Equals("byCount")?value:size); i++)
            {
                using (Repository<Cluster> Repository = new Repository<Cluster>())
                {
                    Cluster cluster = new Cluster();
                    cluster.class_id = id;
                    cluster.title = $"{LOCAL.Objects.Entity.Cluster}-{i + 1}";
                    System.Drawing.Color color = Utilities.getRandomColor();
                    cluster.color_code = "#" + color.R.ToString("X2") + color.G.ToString("X2") + color.B.ToString("X2");
                    cluster = Repository.Inserted(cluster);
                }
            }
            StaticDB.SetProperty(nameof(Cluster));
            using (Repository<Cluster> Repository = new Repository<Cluster>())
            {
                clusterList = Repository.MultiSelectByQuery(c => c.class_id == id).ToList();
            }
            currentClass.ClassStudents.ToList().Shuffle();
            var clusters = currentClass.ClassStudents.ToList()
                .SplitList(type.Equals("byCount") ? size : value)
                .ToList();
            int index = 0;
            foreach (var item in clusters)
            {
                if (clusterList.Count < index + 1)
                    break;
                Cluster cluster = clusterList[index];
                foreach (ClassStudent classStudent in item)
                {
                    using (Repository<ClusterStudent> Repository = new Repository<ClusterStudent>())
                    {
                        ClusterStudent clusterStudent = new ClusterStudent();
                        clusterStudent.class_student_id = classStudent.id;
                        clusterStudent.cluster_id = cluster.id;
                        Repository.Inserting(clusterStudent);
                    }
                }
                ++index;
            }
            StaticDB.SetProperty(nameof(ClusterStudent));
            _Class @class = new _Class();
            using (Repository<_Class> Repository = new Repository<_Class>())
            {
                Repository.Include(nameof(_Class.Clusters));
                Repository.Include(nameof(_Class.ClassStudents));
                StaticDB.Classes = Repository.MultiSelectByQuery(c => c.is_active).ToList();
                @class = StaticDB.Classes.GetById(id);
            }
            @class.ClassStudents.ToList().ForEach(c => c.User = StaticDB.Users.GetById(c.student_id));
            @class.Clusters = StaticDB.Clusters.Where(c => c.class_id == id).ToList();
            @class.Clusters.ToList().ForEach(x => x.ClusterStudents = StaticDB.ClusterStudents.Where(s => s.cluster_id == x.id).ToList());
            return PartialView("_ClassBodyPartial", @class);
        }

        private static Cluster CreateClusters(int id, int value, Repository<Cluster> Repository)
        {
            Cluster cluster = new Cluster();
            for (int i = 0; i < value; i++)
            {
                cluster.class_id = id;
                cluster.title = $"{LOCAL.Objects.Entity.Cluster}-{i + 1}";
                System.Drawing.Color color = Utilities.getRandomColor();
                cluster.color_code = "#" + color.R.ToString("X2") + color.G.ToString("X2") + color.B.ToString("X2");
                cluster = Repository.Inserted(cluster);
            }
            return cluster;
        }

        [HttpPost]
        public ActionResult RemoveClustering(int id)
        {
            using (Repository<Cluster> Repository = new Repository<Cluster>())
            {
                Repository.ExecuteSQL($"DELETE CS FROM ClusterStudents CS LEFT JOIN Clusters C on C.id = CS.cluster_id WHERE C.class_id = {id};");
                Repository.ExecuteSQL($"DELETE FROM Clusters WHERE class_id={id};");
            }
            StaticDB.SetProperty(nameof(Cluster));
            StaticDB.SetProperty(nameof(ClusterStudent));
            _Class @class = new _Class();
            using (Repository<_Class> Repository = new Repository<_Class>())
            {
                Repository.Include(nameof(_Class.Clusters));
                Repository.Include(nameof(_Class.ClassStudents));
                StaticDB.Classes = Repository.MultiSelectByQuery(c => c.is_active).ToList();
                @class = StaticDB.Classes.GetById(id);
            }
            @class.ClassStudents.ToList().ForEach(c => c.User = StaticDB.Users.GetById(c.student_id));
            @class.Clusters.ToList().ForEach(x => x.ClusterStudents = StaticDB.ClusterStudents.Where(s => s.cluster_id == x.id).ToList());
            return PartialView("_ClassBodyPartial", @class);
        }
        public ActionResult Details(int id)
        {
            _Class _class = new _Class();
            using (Repository<_Class> Repository = new Repository<_Class>())
            {
                _class = Repository.SingleSelectByQuery(x => x.id == id && x.is_active);
            }
            if (_class == null)
                return RedirectToAction("Index");
            ViewBag.SubDirectory = id == 0 ? LOCAL.Components.Buttons.Create : LOCAL.Components.Buttons.Details;
            return PartialView("_DetailPartialModal", _class);
        }
        [HttpPost]
        public ActionResult DeleteCluster(int id, int cluster_id)
        {

            using (Repository<Cluster> Repository = new Repository<Cluster>())
            {
                Repository.ExecuteSQL($"DELETE CS FROM ClusterStudents CS WHERE CS.cluster_id = {cluster_id};");
                Repository.ExecuteSQL($"DELETE FROM SessionTasks WHERE cluster_id={cluster_id};");
                Repository.ExecuteSQL($"DELETE FROM Clusters WHERE id={cluster_id};");
            }
            StaticDB.SetProperty(nameof(Cluster));
            StaticDB.SetProperty(nameof(ClusterStudent));
            _Class @class = new _Class();
            using (Repository<_Class> Repository = new Repository<_Class>())
            {
                Repository.Include(nameof(_Class.Clusters));
                Repository.Include(nameof(_Class.ClassStudents));
                StaticDB.Classes = Repository.MultiSelectByQuery(c => c.is_active).ToList();
                @class = StaticDB.Classes.GetById(id);
            }
            @class.ClassStudents.ToList().ForEach(c => c.User = StaticDB.Users.GetById(c.student_id));
            @class.Clusters.ToList().ForEach(x => x.ClusterStudents = StaticDB.ClusterStudents.Where(s => s.cluster_id == x.id).ToList());
            return PartialView("_ClassBodyPartial", @class);
        }
        public ActionResult Delete(int id)
        {
            using (Ctx ctx = new Ctx())
            {
                string q = $"UPDATE Classes SET is_active=0 WHERE id={id};" +
                           $"UPDATE Clusters SET is_active=0 WHERE class_id={id};" +
                           $"UPDATE ClassStudents SET is_active=0 WHERE class_id={id};";
                ctx.Database.ExecuteSqlCommand(q);
            }
            StaticDB.SetProperty(nameof(_Class));
            StaticDB.SetProperty(nameof(Cluster));
            StaticDB.SetProperty(nameof(ClassStudent));
            return RedirectToAction("Index");
        }
    }
}