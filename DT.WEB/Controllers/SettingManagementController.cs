﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DT.DAL;
using DT.DTO;
using DT.WEB.Models;

namespace DT.WEB.Controllers
{
    public class SettingManagementController : BaseController
    {
        // GET: SettingManagement
        public ActionResult Index()
        {
            ViewBag.Title = LOCAL.Objects.Tables.Settings;
            ViewBag.SubDirectory = LOCAL.Objects.Tables.Settings;
            List<Setting> entities = new List<Setting>();
            if (CurrentUser.role_id == 1)
            {
                using (Repository<Setting> Repository = new Repository<Setting>())
                {
                    entities = Repository.SelectByAll().ToList();
                }
                return View(entities);
            }
            else 
                return RedirectToAction("Index", "Home");
        }
        public ActionResult AddUpdate(int id = 0)
        {
            if (CurrentUser.role_id != 1)
                return RedirectToAction("Index", "Home");
            Setting entity = new Setting();
            if (id != 0)
                using (Repository<Setting> Repository = new Repository<Setting>())
                {
                    entity = Repository.SingleSelectByQuery(x => x.id == id);
                }
            if(entity == null)
                entity = new Setting();
            return PartialView("_AddUpdatePartial",entity);
        }

        [HttpPost]
        public ActionResult AddUpdate(Setting entity)
        {
            if (CurrentUser.role_id != 1)
                return RedirectToAction("Index", "Home");
            using (Repository<Setting> Repository = new Repository<Setting>())
            {
                if (entity.id == 0)
                    entity = Repository.Inserted(entity);
                else
                    entity = Repository.Updated(entity);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            if (CurrentUser.role_id != 1)
                return RedirectToAction("Index", "Home");
            using (Repository<Setting> Repository = new Repository<Setting>())
            {
                Setting entity = Repository.SingleSelectByQuery(x => x.id == id);
                Repository.Deleting(entity);
            }
            return RedirectToAction("Index");
        }
    }
}