﻿using DT.DAL;
using DT.DTO;
using MS.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DT.WEB.Controllers
{
    public class ClassStudentManagementController : BaseController
    {
        public ActionResult Index(int class_id)
        {
            ViewBag.ClassId = class_id;
            List<ClassStudent> classStudents = new List<ClassStudent>();
            if (class_id != 0)
                using (Repository<ClassStudent> Repository = new Repository<ClassStudent>())
                {
                    Repository.Include(nameof(ClassStudent.User));
                    classStudents = Repository.MultiSelectByQuery(x => x.class_id == class_id && x.is_active).ToList();
                }
            return PartialView("_AddUpdatePartialModal", classStudents);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                List<ClassStudent> classStudents = Cacher.Get<List<ClassStudent>>("ClassStudents");
                List<ClassStudent> newList = new List<ClassStudent>();
                int index = 1;
                foreach (ClassStudent cs in classStudents)
                {
                    if (cs.id != id)
                    {
                        newList.Add(cs);
                        ++index;
                    }
                }
                Cacher.Save("ClassStudents", newList);
                return Json(new { Result = true });
            }
            catch (Exception ex)
            {
                return Json(new { Result = false, Message = ex.Message });
            }
        }

    }
}