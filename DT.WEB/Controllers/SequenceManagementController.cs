﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using DT.DAL;
using DT.DTO;
using MS.BLL;

namespace DT.WEB.Controllers
{
    public class SequenceManagementController : BaseController
    {
        // GET: SequenceManagement
        public ActionResult Index(string type = "all")
        {
            ViewBag.Title = LOCAL.Objects.Tables.Sequences;
            ViewBag.SubDirectory = LOCAL.Objects.Tables.Sequences;
            ViewBag.Type = type;
            List<Sequence> sequences = new List<Sequence>();
            List<int> bookmarkedIds = new List<int>();
            using (Ctx ctx = new Ctx())
            {
                sequences = ctx.Sequences.Where(x => x.is_active).ToList();
                bookmarkedIds = ctx.BookmarkedSequences.Where(b => b.user_id == CurrentUser.id).Select(b => b.sequence_id).ToList();
                sequences.ForEach(m => m.IsBookmarked = bookmarkedIds.Contains(m.id));
            }
            if (type == "all")
            {
                ViewBag.CanEdit = false;
                return View(sequences);
            }
            else if (type.Equals("bookmarked"))
            {
                ViewBag.SubDirectory = LOCAL.Components.WebMenuItems.BookmarkedSequences;
                sequences = sequences.Where(x => x.is_active && x.IsBookmarked).ToList();
                ViewBag.CanEdit = false;
                return View(sequences);
            }
            else if (type == "my")
            {
                sequences = sequences.Where(t =>
                t.is_active && t.created_by == CurrentUser.id).ToList();
                return View(sequences);
            }
            else
                return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        public ActionResult GetList(string type = "all", int range = 0)
        {
            int draw = Convert.ToInt32(Request.Form["draw"]);// etkin sayfa numarası
            int start = Convert.ToInt32(Request["start"]);//listenen ilk kayıtın  index numarası
            int length = Convert.ToInt32(Request["length"]);//sayfadaki toplam listelenecek kayit sayısı
            string search = Request["search[value]"];//arama
            string sortColumnName = Request["columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]"];//Sıralama yapılacak column adı
            string sortDirection = Request["order[0][dir]"];//sıralama türü
            sortColumnName = string.IsNullOrEmpty(sortColumnName) ? "created_date" : sortColumnName;


            List<Sequence> sequences = new List<Sequence>();
            List<int> bookmarkedSequenceIds = new List<int>();
            using (Ctx ctx = new Ctx())
            {
                string que = LOCAL.Objects.DBScripts.GetSequences;
                if (range != 0)
                    que += $" AND T.group_range = {range};";
                sequences = ctx.Database.SqlQuery<Sequence>(que).ToList();
                sequences.ForEach(s => s.SequenceTasks = s.SequenceTasks.Where(x => x.is_active && x.sequence_id == s.id).ToList());
                bookmarkedSequenceIds = ctx.BookmarkedSequences.Where(b => b.user_id == CurrentUser.id).Select(b => b.sequence_id).ToList();
            }
            sequences.ForEach(m => m.IsBookmarked = bookmarkedSequenceIds.Contains(m.id));
            sequences.ForEach(m => m.Creator = StaticDB.Users.GetById(m.created_by).ToStr());
            sequences.ForEach(m => m.tasks_count = StaticDB.SequenceTasks.Count(x=>x.is_active && x.sequence_id == m.id));
            sequences.ForEach(s => s.CanEdit = s.created_by == CurrentUser.id || CurrentUser.role_id == 1 || CurrentUser.role_id == 2);
            if (type == "bookmarked")
                sequences = sequences.Where(b => b.IsBookmarked).ToList();
            else if (type == "my")
                sequences = sequences.Where(x => x.created_by == CurrentUser.id).ToList();

            List<Sequence> list = new List<Sequence>();
            if (sortColumnName.Equals(nameof(Sequence.created_date)))
            {
                if (sortDirection != "asc")
                    list = sequences.OrderByDescending(m => m.created_date).ToList();
                else
                    list = sequences.OrderBy(m => m.created_date).ToList();
            }
            else
            {
                list = sequences.AsQueryable().OrderByDynamic(sortColumnName, sortDirection != "asc").ToList();
            }
            if (search == null)
                search = "";

            if (!string.IsNullOrEmpty(search))
            {
                List<Sequence> newList = new List<Sequence>();
                foreach (var item in list)
                {
                    foreach (PropertyInfo property in typeof(Sequence).GetProperties())
                    {
                        var value = property.GetValue(item);
                        if (value != null && value.ToStr().ToLower().Contains(search.ToLower()))
                        {
                            newList.Add(item);
                        }
                    }
                }
                list = newList;
            }
            for (int i = 1; i < 6; i++)
            {
                string prop = Request.Form[$"columns[{i}][data]"];
                if (prop.Equals("function"))
                    prop = Request.Form[$"columns[{i}][name]"];
                string value = Request.Form[$"columns[{i}][search][value]"];
                if (string.IsNullOrEmpty(prop) || string.IsNullOrEmpty(value))
                    continue;
                list = list.Filter(prop, value);
            }
            var filteredRows = list.Skip(start).Take(length).ToList();
            JsonData jsonData = new JsonData();
            jsonData.draw = draw;
            jsonData.recordsTotal = filteredRows.Count;
            jsonData.recordsFiltered = list.Count;
            jsonData.data = filteredRows;
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddUpdate(int id = 0)
        {
            ViewBag.Title = LOCAL.Objects.Tables.Sequences;
            ViewBag.SubDirectory = id == 0 ? LOCAL.Components.Buttons.Create : LOCAL.Components.Buttons.Edit;
            DT.DTO.Sequence sequence = new DT.DTO.Sequence();
            string generatedSequenceId = Guid.NewGuid().ToString();
            Cacher.Delete("SequenceId");
            Cacher.Delete("SequenceTasks");
            Cacher.SetStr("SequenceId", generatedSequenceId);
            List<SequenceTask> sequenceTasks = new List<SequenceTask>();
            StaticDB.Tasks.ForEach(t => t.TaskMaterials = StaticDB.TaskMaterials.Where(x => x.task_id == t.id && x.is_active).ToList());
            if (id != 0)
            {
                using (Ctx ctx = new Ctx())
                {
                    sequence = ctx.Sequences.FirstOrDefault(x => x.id == id);
                    sequenceTasks = ctx.SequenceTasks.Where(s => s.sequence_id == id && s.is_active).ToList();
                }
                sequenceTasks.ForEach(s => s.Task = StaticDB.Tasks.FirstOrDefault(x => x.id == s.task_id));
            }
            else
            {
                sequence.created_by = CurrentUser.id;
            }
            sequenceTasks.ForEach(s => s.SequenceId = generatedSequenceId);
            Cacher.Save("SequenceTasks", sequenceTasks);
            return View(sequence);
        }

        public ActionResult Selector(int range = 0)
        {
            ViewBag.Range = range;
            return PartialView("_SequenceSelector");
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddUpdate(DT.DTO.Sequence sequence)
        {
            try
            {
                using (Repository<DT.DTO.Sequence> Repository = new Repository<DT.DTO.Sequence>())
                {
                    if (sequence.id == 0)
                        sequence = Repository.Inserted(sequence);
                    else
                        sequence = Repository.Updated(sequence);



                }

                using (Ctx ctx = new Ctx())
                {
                    ctx.Database.ExecuteSqlCommand($"DELETE FROM SequenceTasks WHERE sequence_id={sequence.id}");
                }
                foreach (SequenceTask sequenceTask in Cacher.Get<List<SequenceTask>>("SequenceTasks"))
                {
                    using (Repository<SequenceTask> Repository = new Repository<SequenceTask>())
                    {
                        SequenceTask entity = new SequenceTask();
                        entity.sequence_id = sequence.id;
                        entity.task_id = sequenceTask.task_id;
                        entity.index_order = sequenceTask.index_order;
                        Repository.Inserting(entity);
                    }

                    DTHelper.SetTaskUsage(sequenceTask.task_id);
                }
                return Json(new { Result = true, Sequence = sequence });
            }
            catch (Exception ex)
            {
                return Json(new { Result = false, Message = ex });
            }
        }

        [HttpPost]
        public ActionResult Bookmark(int id)
        {
            try
            {
                Sequence sequence = null;
                using (Repository<Sequence> Repository = new Repository<Sequence>())
                {
                    sequence = Repository.SingleSelectByQuery(m => m.id == id);
                }
                if (sequence == null)
                    return Json(new { Result = false, Message = LOCAL.Messages.Warnings.RecordNotFound });

                BookmarkedSequence bookmarkedSequence = null;
                using (Repository<BookmarkedSequence> Repository = new Repository<BookmarkedSequence>())
                {
                    bookmarkedSequence = Repository.SingleSelectByQuery(m => m.sequence_id == id && m.user_id == CurrentUser.id && m.is_active);
                    if (bookmarkedSequence == null)
                    {
                        bookmarkedSequence = new BookmarkedSequence();
                        bookmarkedSequence.sequence_id = id;
                        bookmarkedSequence.user_id = CurrentUser.id;
                        bookmarkedSequence = Repository.Inserted(bookmarkedSequence);
                        return Json(new { Result = true, Bookmarked = true });
                    }
                    else
                    {
                        Repository.Deleting(bookmarkedSequence);
                        return Json(new { Result = true, Bookmarked = false });
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = false, Message = ex });
            }
        }
        public ActionResult Details(int id)
        {
            Sequence sequence = new Sequence();
            using (Repository<Sequence> Repository = new Repository<Sequence>())
            {
                Repository.Include(nameof(Sequence.SequenceTasks));
                sequence = Repository.SingleSelectByQuery(x => x.id == id && x.is_active);
            }
            if (sequence == null)
                return RedirectToAction("Index");
            ViewBag.SubDirectory = id == 0 ? LOCAL.Components.Buttons.Create : LOCAL.Components.Buttons.Details;
            sequence.SequenceTasks.ToList().ForEach(item => item.Task = StaticDB.Tasks.GetById(item.task_id));
            sequence.SequenceTasks.ToList().ForEach(item => item.Task.User = StaticDB.Users.GetById(item.Task.author_id));
            return PartialView("_DetailPartialModal", sequence);
        }
        public ActionResult Delete(int id)
        {

            using (Repository<Sequence> Repository = new Repository<Sequence>())
            {
                Sequence sequence = Repository.SingleSelectByQuery(x => x.id == id && x.is_active);
                Repository.SoftDeleting(sequence);
            }
            return RedirectToAction("Index");
        }
    }
}