﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DT.WEB.Controllers
{
    public class TaskEngagementHistoryController : Controller
    {
        // GET: TaskEngagementHistory
        public ActionResult Index()
        {
            ViewBag.Title = "Task Engagement History";
            return View();
        }
    }
}