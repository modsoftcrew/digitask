﻿using DT.DAL;
using DT.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DT.WEB.Controllers
{
    public class StudentController : BaseController
    {
        // GET: Student
        public ActionResult Index()
        {
            ViewBag.Title = "Dashboard";
            StudentDashboardViewModel model = new StudentDashboardViewModel();
            using (Ctx ctx = new Ctx())
            {
                model.Tasks = ctx.Database.SqlQuery<StudentTaskModel>(string.Format(LOCAL.Objects.DBScripts.GetStudentAssignedTasks, CurrentUser.id)).ToList();
            }
            using (Repository<RawPage> Repository = new Repository<RawPage>())
            {
                model.Feeds = Repository.MultiSelectByQuery(r => r.name.Equals("FEED") && r.is_active && r.role_ids.Contains(CurrentUser.role_id.ToString())).ToList();
            }
            return View(model);
        }
        public JsonResult GetEvents()
        {
            var events = new List<EventModel>();
            using (Ctx ctx = new Ctx())
            {
                string q = string.Format(LOCAL.Objects.DBScripts.GetStudentEvents, CurrentUser.id);
                events = ctx.Database.SqlQuery<EventModel>(q).ToList();
            }
            return Json(events.ToArray(), JsonRequestBehavior.AllowGet);
        }
    }
}