﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DT.DAL;
using DT.DTO;
using DT.WEB.Models;

namespace DT.WEB.Controllers
{
    public class RoleManagementController : BaseController
    {
        // GET: RoleManagement
        public ActionResult Index()
        {
            ViewBag.Title = LOCAL.Objects.Tables.Roles;
            ViewBag.SubDirectory = LOCAL.Objects.Tables.Roles;
            List<Role> entities = new List<Role>();
            if (CurrentUser.role_id == 1)
            {
                using (Repository<Role> Repository = new Repository<Role>())
                {
                    entities = Repository.SelectByAll().ToList();
                }
                return View(entities);
            }
            else 
                return RedirectToAction("Index", "Home");
        }
        public ActionResult AddUpdate(int id = 0)
        {
            if (CurrentUser.role_id != 1 || id < 5)
                return RedirectToAction("Index", "Home");
            Role entity = new Role();
            if (id != 0)
                using (Repository<Role> Repository = new Repository<Role>())
                {
                    entity = Repository.SingleSelectByQuery(x => x.id == id);
                }
            if(entity == null)
                entity = new Role();
            return PartialView("_AddUpdatePartial",entity);
        }

        [HttpPost]
        public ActionResult AddUpdate(Role entity)
        {
            if (CurrentUser.role_id != 1 || entity.id < 5)
                return RedirectToAction("Index", "Home");
            using (Repository<Role> Repository = new Repository<Role>())
            {
                if (entity.id == 0)
                    entity = Repository.Inserted(entity);
                else
                    entity = Repository.Updated(entity);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            if (CurrentUser.role_id != 1 || id < 5)
                return RedirectToAction("Index", "Home");
            using (Repository<Role> Repository = new Repository<Role>())
            {
                Role entity = Repository.SingleSelectByQuery(x => x.id == id);
                Repository.Deleting(entity);
            }
            return RedirectToAction("Index");
        }
    }
}