﻿using MS.BLL;
using DT.DAL;
using DT.DTO;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Resources;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DT.WEB.Controllers
{
    [NoCache]
    public class BaseController:Controller
    {
        public User CurrentUser { get; set; }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            try
            {
                Session.Timeout = 300;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                    | SecurityProtocolType.Tls11
                                                    | SecurityProtocolType.Tls12
                                                    | SecurityProtocolType.Ssl3;

                CurrentUser = WebUtils.Cookies.Get<User>(filterContext.HttpContext, "User");

                string serverUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                string apiServerUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")) + "api/";

                WebUtils.Cookies.Save(filterContext.HttpContext, "ServerUrl", serverUrl);
                WebUtils.Cookies.Save(filterContext.HttpContext, "ApiServerUrl", apiServerUrl);
                if (CurrentUser == null)
                {
                    Session.Clear();
                    filterContext.Result = Giris();
                    return;
                }
                else
                {
                    Language language = StaticDB.Languages.FirstOrDefault(x => x.iso_code.Equals(CurrentUser.ui_language));

                    Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(language != null ? language.iso_code : "en");
                    Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(language != null ? language.iso_code : "en");
                }
                base.OnActionExecuting(filterContext);
            }
            catch (Exception ex)
            {
                DT.DAL.Logger.Save("Action Executing Error", ex);
            }
        }
        protected override void OnException(ExceptionContext filterContext)
        {
            DT.DAL.Logger.Save("OnException", filterContext.Exception);
            Session["PageEx"] = filterContext.Exception;
            filterContext.HttpContext.ClearError();
            filterContext.HttpContext.Response.RedirectToRoute($"error-page");
        }
        public RedirectToRouteResult Giris()
        {
            return RedirectToAction("Start", "Account");
        }
    }
}