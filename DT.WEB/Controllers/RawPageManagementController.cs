﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DT.DAL;
using DT.DTO;
using DT.WEB.Models;
using MS.BLL;
using Newtonsoft.Json;

namespace DT.WEB.Controllers
{
    public class RawPageManagementController : BaseController
    {
        // GET: RawPageManagement
        public ActionResult Index()
        {
            ViewBag.Title = LOCAL.Objects.Tables.RawPages;
            ViewBag.SubDirectory = LOCAL.Objects.Tables.RawPages;
            List<RawPage> entities = new List<RawPage>();
            if (CurrentUser.role_id == 1)
            {
                using (Repository<RawPage> Repository = new Repository<RawPage>())
                {
                    entities = Repository.SelectByAll().ToList();
                }
                return View(entities);
            }
            else
                return RedirectToAction("Index", "Home");
        }
        public ActionResult AddUpdate(int id = 0)
        {
            if (CurrentUser.role_id != 1)
                return RedirectToAction("Index", "Home");
            RawPage entity = new RawPage();
            if (id != 0)
                using (Repository<RawPage> Repository = new Repository<RawPage>())
                {
                    entity = Repository.SingleSelectByQuery(x => x.id == id);
                }
            if (entity == null)
                entity = new RawPage();
            if (entity.role_ids != null && entity.role_ids.Trim().StartsWith("["))
            {
                entity.roles = JsonConvert.DeserializeObject<string[]>(entity.role_ids).Select(s => s.ToInteger()).ToList();
            }
            return View(entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult AddUpdate(RawPage entity)
        {
            if (CurrentUser.role_id != 1)
                return RedirectToAction("Index", "Home");
            entity.role_ids = "[]";
            if (entity.roles.Count > 0)
                entity.role_ids = entity.roles.ToJson();
            using (Repository<RawPage> Repository = new Repository<RawPage>())
            {
                if (entity.id == 0)
                    entity = Repository.Inserted(entity);
                else
                    entity = Repository.Updated(entity);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Details(int id)
        {
            return RedirectToAction("RawPageShower", "Account", new { id = id });
        }
        public ActionResult Delete(int id)
        {
            if (CurrentUser.role_id != 1)
                return RedirectToAction("Index", "Home");
            using (Repository<RawPage> Repository = new Repository<RawPage>())
            {
                RawPage entity = Repository.SingleSelectByQuery(x => x.id == id);
                Repository.SoftDeleting(entity);
            }
            return RedirectToAction("Index");
        }
    }
}