﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DT.DAL;
using DT.DTO;
using DT.WEB.Models;
using MS.BLL;

namespace DT.WEB.Controllers
{
    public class TaskResultManagementController : BaseController
    {
        // GET: TaskResult
        public ActionResult Index(int role_id = 0)
        {
            ViewBag.Title = LOCAL.Objects.Tables.TaskResults;
            ViewBag.SubDirectory = LOCAL.Components.Labels.List;
            return View();
        }

        [HttpPost]
        public ActionResult GetList()
        {
            int draw = Convert.ToInt32(Request.Form["draw"]);// etkin sayfa numarası
            int start = Convert.ToInt32(Request["start"]);//listenen ilk kayıtın  index numarası
            int length = Convert.ToInt32(Request["length"]);//sayfadaki toplam listelenecek kayit sayısı
            string search = Request["search[value]"];//arama
            string sortColumnName = Request["columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]"];//Sıralama yapılacak column adı
            string sortDirection = Request["order[0][dir]"];//sıralama türü
            sortColumnName = string.IsNullOrEmpty(sortColumnName) ? "created_date" : sortColumnName;


            List<TaskResult> taskResults = new List<TaskResult>();
            using (Ctx ctx = new Ctx())
            {
                if (CurrentUser.role_id == 1 || CurrentUser.role_id == 2)
                    taskResults = ctx.TaskResults.Where(x => x.is_active).ToList();
                else if (CurrentUser.role_id == 3)
                    taskResults = ctx.TaskResults.Where(x => x.is_active && x.teacher_id == CurrentUser.id).ToList();
            }
            taskResults.ForEach(m => m.StartDate = m.start_time.ToString("dd.MM.yyyy, HH:mm"));
            taskResults.ForEach(m => m.FinishDate = m.finish_time == null ? "-" : Convert.ToDateTime(m.finish_time).ToString("dd.MM.yyyy, HH:mm"));
            taskResults.ForEach(m => m.Teacher = StaticDB.Users.GetById(m.teacher_id).ToStr());
            taskResults.ForEach(m => m.Student = StaticDB.Users.GetById(m.student_id).ToStr());
            taskResults.ForEach(m => m.SessionTask = StaticDB.SessionTasks.GetById(m.session_task_id));
            taskResults.ForEach(m => m.Task = m.SessionTask != null ? StaticDB.Tasks.GetById(m.SessionTask.task_id) : null);

            List<TaskResult> list = new List<TaskResult>();
            if (sortColumnName.Equals(nameof(TaskResult.StartDate)))
            {
                if (sortDirection != "asc")
                    list = taskResults.OrderByDescending(m => m.start_time).ToList();
                else
                    list = taskResults.OrderBy(m => m.start_time).ToList();
            }
            else if (sortColumnName.Equals(nameof(TaskResult.FinishDate)))
            {
                if (sortDirection != "asc")
                    list = taskResults.OrderByDescending(m => m.finish_time).ToList();
                else
                    list = taskResults.OrderBy(m => m.finish_time).ToList();
            }
            else
            {
                list = taskResults.AsQueryable().OrderByDynamic(sortColumnName, sortDirection != "asc").ToList();
            }

            JsonData jsonData = new JsonData();
            jsonData.draw = draw;
            jsonData.recordsTotal = list.Count;
            jsonData.recordsFiltered = list.Count;
            jsonData.data = list;
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        // GET: TaskResult
        public ActionResult AssignmentIndex()
        {
            ViewBag.Title = LOCAL.Components.WebMenuItems.Assignments;
            ViewBag.SubDirectory = LOCAL.Components.Labels.List;
            List<SessionTask> SessionTasks = new List<SessionTask>();
            using (Ctx ctx = new Ctx())
            {
                if (CurrentUser.role_id == 3)
                {
                    SessionTasks = ctx.Database.SqlQuery<SessionTask>("EXEC GetTeacherSessionTasksByUserId " + CurrentUser.id + ";").ToList();
                }
                else if (CurrentUser.role_id == 1 || CurrentUser.role_id == 2)
                {
                    SessionTasks = ctx.SessionTasks.Where(s => s.is_active).ToList();
                }
            }
            return View(SessionTasks);
        }
        public ActionResult Details(int session_task_id)
        {
            List<TaskResult> taskResults = new List<TaskResult>();

            using (Repository<TaskResult> Repository = new Repository<TaskResult>())
            {
                taskResults = Repository.MultiSelectByQuery(x => x.session_task_id == session_task_id && x.is_active).ToList();
            }
            SessionTask sessionTask = StaticDB.SessionTasks.GetById(session_task_id);
            if (sessionTask != null)
            {
                ViewBag.SessionTask = sessionTask;
                ViewBag.Task = StaticDB.Tasks.GetById(sessionTask.task_id);
            }
            else
            {
                ViewBag.SessionTask = new SessionTask();
                ViewBag.Task = new _Task();
            }
            return PartialView("_DetailsPartial", taskResults);
        }
        public ActionResult SequenceDetails(int sequence_id)
        {
            List<TaskResult> taskResults = new List<TaskResult>();

            using (Ctx ctx = new Ctx())
            {
                taskResults = ctx.Database.SqlQuery<TaskResult>($"EXEC GetTaskResultsBySequenceId {sequence_id};").ToList();
            }
            return PartialView("_SequenceDetailsPartial", taskResults);
        }
        public ActionResult ShowAnswer(int id)
        {
            TaskResult taskResult = new TaskResult();

            using (Repository<TaskResult> Repository = new Repository<TaskResult>())
            {
                taskResult = Repository.SingleSelectByQuery(x => x.id == id);
            }
            return PartialView("_AnswerPartial", taskResult);
        }
    }
}