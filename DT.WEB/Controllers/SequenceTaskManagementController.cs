﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web;
using System.Web.Mvc;
using DT.DAL;
using DT.DTO;
using MS.BLL;

namespace DT.WEB.Controllers
{
    public class SequenceTaskManagementController : BaseController
    {

        [HttpPost]
        public ActionResult GetList()
        {
            int draw = Convert.ToInt32(Request.Form["draw"]);// etkin sayfa numarası
            int start = Convert.ToInt32(Request["start"]);//listenen ilk kayıtın  index numarası
            int length = Convert.ToInt32(Request["length"]);//sayfadaki toplam listelenecek kayit sayısı
            string search = Request["search[value]"];//arama
            string sortDirection = Request["order[0][dir]"];//sıralama türü
            List<SequenceTask> sequenceTasks = Cacher.Get<List<SequenceTask>>("SequenceTasks");
            sequenceTasks.ForEach(s => s.Task = StaticDB.Tasks.GetById(s.task_id));
            JsonData jsonData = new JsonData();
            jsonData.draw = draw;
            jsonData.recordsTotal = sequenceTasks.Count;
            jsonData.recordsFiltered = sequenceTasks.Count;
            jsonData.data = sequenceTasks.Where(t=>t.Task != null).Select(c=>
            new {
                id= c.id,
                index_order = c.index_order,
                task_id =c.task_id,
                type=c.Task.type,
                Creator = c.Task.created_by != c.Task.author_id ? StaticDB.Users.GetById(c.Task.created_by).ToStr() : "",
                Author = c.Task.Author = StaticDB.Users.GetById(c.Task.created_by).ToStr(),
                title = c.Task.title,
                target_subject = c.Task.target_subject,
                target_min_age = c.Task.target_min_age,
                target_max_age = c.Task.target_max_age,
                group_range = c.Task.group_range,
                target_languages_str = string.Join(",", StaticDB.TargetLanguages.Where(x => x.task_id == c.task_id).Select(x => x.Language)),
                information_gap = c.Task.information_gap
            }).Distinct();
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Reorder(int id, int[] ordered_ids)
        {
            try
            {
                List<SequenceTask> sequenceTasks = Cacher.Get<List<SequenceTask>>("SequenceTasks");
                List<SequenceTask> newList = new List<SequenceTask>();
                int index = 1;
                foreach (int ordered_id in ordered_ids.Distinct())
                {
                    SequenceTask sequenceTask = sequenceTasks.GetById(ordered_id);
                    if(sequenceTask != null)
                    {
                        sequenceTask.index_order = index;
                        newList.Add(sequenceTask);
                        ++index;
                    }
                }
                Cacher.Save("SequenceTasks", newList);
                return Json(new { Result = true });
            }
            catch (Exception ex)
            {
                return Json(new { Result = false, Message = ex.Message });
            }
        }
        [HttpPost]
        public ActionResult AddUpdate(DT.DTO.SequenceTask sequenceTask)
        {
            try
            {
                List<SequenceTask> sequenceTasks = Cacher.Get<List<SequenceTask>>("SequenceTasks");
                sequenceTask.index_order = 1;
                if (sequenceTasks.Count > 0)
                    sequenceTask.index_order = sequenceTasks.Max(s => s.index_order) + 1;
                sequenceTask.id = new Random(Zaman.Simdi.Millisecond).Next(1111111, 999999999);
                sequenceTask.SequenceId = Cacher.GetStr("SequenceId");
                sequenceTasks.Add(sequenceTask);
                _Task task = StaticDB.Tasks.GetById(sequenceTask.task_id);
                int group_range = 0;
                if (task != null)
                    group_range = task.group_range;
                Cacher.Save("SequenceTasks", sequenceTasks);
                return Json(new { Result = true, GroupRange = group_range });
            }
            catch (Exception ex)
            {
                return Json(new { Result = false, Message = ex.Message });
            }
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                List<SequenceTask> sequenceTasks = Cacher.Get<List<SequenceTask>>("SequenceTasks");
                List<SequenceTask> newList = new List<SequenceTask>();
                int index = 1;
                foreach (SequenceTask st in sequenceTasks)
                {
                    if (st.id != id)
                    {
                        st.index_order = index;
                        newList.Add(st);
                        ++index;
                    }
                }
                Cacher.Save("SequenceTasks", newList);
                return Json(new { Result = true });
            }
            catch (Exception ex)
            {
                return Json(new { Result = false, Message = ex.Message });
            }
        }
    }
}