﻿using DT.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DT.WEB.Controllers
{
    [BasicAuthentication]
    public class ApiBaseController : ApiController
    {
        public User CurrentUser { get; set; }
        public ApiBaseController()
        {  

        }
    }
}
