﻿using MS.BLL;
using MS.BLL.Helpers;
using DT.DTO;
using DT.WEB.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using DT.DAL;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json.Linq;

namespace DT.WEB.Controllers
{
    [NoCache]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Homepage";
            if (CurrentUser.role_id == 4)
            {
                return RedirectToAction("Index", "Student");
            }
            else
            {
                List<RawPage> feeds = new List<RawPage>();
                using (Repository<RawPage> Repository = new Repository<RawPage>())
                {
                    feeds = Repository.MultiSelectByQuery(r => r.name != null && r.name.Equals("FEED") && r.is_active && r.role_ids != null && r.role_ids.Contains(CurrentUser.role_id.ToString())).ToList();
                }
                return View(feeds);
            }

        }
        public ActionResult Search()
        {
            ViewBag.Title = "Seach Page";
            ViewBag.PageTitle = LOCAL.Components.Labels.EnhancedSearch;
            ViewBag.IsEnhanced = true;
            return View(new List<SearchResultModel>());
        }
        [HttpPost]
        public ActionResult Search(string search_phrase, bool is_enhanced = false, string typing = "all", string sorting = "asc", string ordering = "title")
        {
            ViewBag.IsEnhanced = is_enhanced;
            ViewBag.PageTitle = LOCAL.Components.Labels.SearchResults;
            List<SearchResultModel> results = new List<SearchResultModel>();
            foreach (_Task task in StaticDB.Tasks)
            {
                if (is_enhanced)
                {
                    var tasks = from t in StaticDB.Tasks
                                where t.title.ToLower().Contains(search_phrase.ToLower())
                                select new SearchResultModel
                                {
                                    title = t.title,
                                    type = nameof(LOCAL.Objects.Entity._Task),
                                    created_date = t.created_date
                                };
                    results.AddRange(tasks);
                }
                else
                {

                }
            }
            ViewBag.Title = $"\"{search_phrase}\" Search Results";
            return View();
        }
        public ActionResult ErrorPage()
        {
            Session["Action"] = "Error Page";
            ViewBag.Title = "Error Page";
            ViewBag.SubDirectory = "Error Page";
            return View();
        }
        public ActionResult InternalServerError()
        {
            Session["Action"] = "Internal Server Error";
            ViewBag.Title = "Internal Server Error";
            ViewBag.SubDirectory = "Internal Server Error";
            return View();
        }
        public ActionResult ChangeLanguage(int language_id)
        {
            try
            {
                Language language = StaticDB.Languages.GetById(language_id);
                CurrentUser.ui_language = language.iso_code;
                using (Repository<User> Repository = new Repository<User>())
                {
                    CurrentUser.is_active = true;
                    CurrentUser = Repository.Updated(CurrentUser);
                }

                Session["User"] = CurrentUser;
                Session["ErrorMessage"] = "";
                WebUtils.Cookies.Save("User", CurrentUser);
                WebUtils.Cookies.Save("Language", language);
            }
            catch (Exception ex)
            {
                DT.DAL.Logger.Save("Önbellek boşaltılamadı.", ex);
            }
            return Redirect(Request.UrlReferrer.ToString());
        }
        public ActionResult CleanStatic()
        {
            try
            {
                WebUtils.FillStaticDB();
            }
            catch (Exception ex)
            {
                DT.DAL.Logger.Save("Önbellek boşaltılamadı.", ex);
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        [HttpPost]
        public ActionResult GetAnnouncements()
        {
            try
            {
                List<RawPage> Announcements = new List<RawPage>();
                using (Repository<RawPage> Repository = new Repository<RawPage>())
                {
                    Announcements = Repository.MultiSelectByQuery(x => x.name.Equals("ANNOUNCEMENT") && x.is_active && x.role_ids.Contains(CurrentUser.role_id.ToString())).ToList();
                }
                Announcements.ForEach(a => a.User = StaticDB.Users.GetById(a.created_by));
                return Json(new { Result = true, list = Announcements },JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = false});
            }
        }
        [HttpPost]
        public ActionResult SendReport()
        {
            Exception exception;
            try
            {
                exception = Session["PageEx"] as Exception;
                string body = WebUtils.CreateJsonCode(exception) + $"<br/><strong>Reported By : {CurrentUser.SysStr()}</strong>";
                bool result = Mailer.Send("info@modsoft.com.tr", LOCAL.ApplicationInfo.Abbreviation + " Error Report", body, true);
                return Json(new { Result = result });
            }
            catch (Exception ex)
            {
                DT.DAL.Logger.Save("Hata Raporu Gönderilemedi", ex);
                return Json(new { Result = false, Message = ex.ToStr() });
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PostFeedback(string subject, string body)
        {
            try
            {
                string mailBody = body + $"<p><strong>{CurrentUser.SelectLabel}</strong></p>";
                Mailer.Send(LOCAL.ApplicationInfo.Mail, subject, mailBody,true);
            }
            catch (Exception ex)
            {
                DT.DAL.Logger.Save("Feedback Gönderilemedi", ex);
            }
            return Redirect(Request.UrlReferrer.ToStr());
        }
        public ActionResult GetReportForm()
        {
            return PartialView("_ReportFormModal");
        }
        public static bool ReCaptchaPassed(string gRecaptchaResponse)
        {
            HttpClient httpClient = new HttpClient();

            var res = httpClient.GetAsync($"https://www.google.com/recaptcha/api/siteverify?secret=6LeR5u0cAAAAAKyIRoZ3TmJGHYlP-A5aAst0lQo1&response={gRecaptchaResponse}").Result;

            if (res.StatusCode != HttpStatusCode.OK)
            {
                return false;
            }
            string JSONres = res.Content.ReadAsStringAsync().Result;
            dynamic JSONdata = JObject.Parse(JSONres);

            if (JSONdata.success != "true" || JSONdata.score <= 0.5m)
            {
                return false;
            }

            return true;
        }
    }
}