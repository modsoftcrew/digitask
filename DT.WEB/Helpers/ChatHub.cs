﻿using DT.DAL;
using DT.DTO;
using DT.WEB.Models;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using MoreLinq;
using MS.BLL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace DT.WEB
{
    [HubName("myChatHub")]
    public class ChatHub : Hub
    {
        public static List<SessionModel> sessionModels = new List<SessionModel>();
        public static List<SessionModel> SessionModels { get { return sessionModels; } set { sessionModels = value; } }
        public void send(int session_id, int user_id, string message)
        {
            if (message != "")
            {
                Messaging messaging = new Messaging();
                messaging.session_id = session_id;
                messaging.user_id = user_id;
                messaging.User = StaticDB.Users.GetById(user_id);
                messaging.body = message;
                messaging.datetime = Zaman.Simdi;

                SessionModel sessionModel = sessionModels.FirstOrDefault(x => x.SessionID == session_id);
                if (sessionModel != null)
                {
                    sessionModel.Messages.Add(messaging);
                }
                Clients.Group($"Session_{session_id}").addMessage(messaging);
            }
        }
        public void ready(int session_id, int user_id)
        {
            SessionModel sessionModel = sessionModels.FirstOrDefault(x => x.SessionID == session_id);
            if (sessionModel != null)
            {
                User user = sessionModel.Students.FirstOrDefault(x => x.id == user_id);
                if (user != null)
                {
                    sessionModel.Students.FirstOrDefault(x => x.id == user_id).IsReady = true;
                    user.IsReady = true;
                }
                else
                {
                    user = StaticDB.Users.GetById(user_id);
                    user.IsReady = true;
                    user.ConnectionId = Context.ConnectionId;
                    sessionModel.Students.Add(user);
                }
                Clients.Group($"Session_{session_id}").UserReadytatusChanged(user);
                if (sessionModel.Students.Count(x => x.IsReady) >= sessionModel.SessionTask.Task.group_range)
                {
                    sessionModel.SessionTask.start_time = Zaman.Simdi;
                    sessionModel.Students.Shuffle();
                    sessionModel.SessionTask.Task.TaskMaterials.ToList().ForEach(t => t.Material = StaticDB.Materials.GetById(t.material_id));
                    for (int i = 0; i < sessionModel.Students.Count; i++)
                    {
                        var taskMaterials = sessionModel.SessionTask.Task.TaskMaterials.Where(x => x.index_order == i + 1).GroupBy(t => t.activation_time);
                        sessionModel.Students[i].UserTaskMaterials.Clear();
                        foreach (var grpTm in taskMaterials)
                        {
                            sessionModel.Students[i].UserTaskMaterials.Add(new UserTaskMaterial(grpTm.Key, grpTm.ToList()));
                        }
                    }
                    Clients.Group($"Session_{session_id}").StartTask(sessionModel.Students, sessionModel.SessionTask.task_id);
                    Clients.Group($"Session_{session_id}").SessionStarted(Convert.ToDateTime(sessionModel.SessionTask.start_time));
                }
            }
        }



        public void sync(int session_id, int user_id, string text)
        {
            SessionModel sessionModel = sessionModels.FirstOrDefault(x => x.SessionID == session_id);
            if (sessionModel != null)
            {
                if (!sessionModel.SessionTask.Task.is_collabrative_writing)
                    return;
                sessionModel.Text = text;
                User user = sessionModel.Students.FirstOrDefault(x => x.id == user_id);
                string userFull = "";
                if (user != null)
                    userFull = user.FullName;
                Clients.Group($"Session_{session_id}", Context.ConnectionId).SetText(text, userFull);
            }
        }
        public void finishTask(int session_id)
        {
            SessionModel sessionModel = sessionModels.FirstOrDefault(x => x.SessionID == session_id);
            if (sessionModel != null)
            {
                SessionTask sessionTask = sessionModel.SessionTask;
                using (Repository<SessionTask> Repository = new Repository<SessionTask>())
                {
                    sessionTask = Repository.SingleSelectByQuery(s => s.id == sessionModel.SessionTask.id);
                    if (sessionTask == null)
                        sessionTask = sessionModel.SessionTask;
                    sessionTask.start_time = sessionModel.SessionTask.start_time;
                    sessionTask.end_time = Zaman.Simdi;
                    sessionTask.end_count = sessionModel.Students.Count;
                    sessionTask.conversations = sessionModel.Messages.ToJson();
                    sessionTask.status = nameof(LOCAL.Enums.SessionTaskStates.Completed);
                    sessionTask = Repository.Updated(sessionTask);
                }
                foreach (User user in sessionModel.Students)
                {
                    using (Repository<TaskResult> Repository = new Repository<TaskResult>())
                    {
                        TaskResult taskResult = Repository.SingleSelectByQuery(x =>
                        x.session_task_id == sessionModel.SessionTask.id &&
                        x.teacher_id == sessionModel.SessionTask.created_by &&
                        x.student_id == user.id);

                        if (taskResult == null)
                            taskResult = new TaskResult();

                        taskResult.session_task_id = sessionModel.SessionTask.id;
                        taskResult.teacher_id = sessionModel.SessionTask.created_by;
                        taskResult.student_id = user.id;
                        if (sessionModel.SessionTask.start_time != null)
                            taskResult.start_time = Convert.ToDateTime(sessionModel.SessionTask.start_time);
                        else
                            taskResult.start_time = Zaman.Simdi;
                        taskResult.finish_time = Zaman.Simdi;
                        string answer = "";
                        try
                        {
                            if (sessionModel.Answers == null)
                                answer = "NO ANSWER";
                            else if (sessionModel.Answers.ContainsKey(user.id))
                                answer = sessionModel.Answers[user.id];
                            else
                                answer = sessionModel.Answers.ToJson();

                        }
                        catch (Exception ex)
                        {

                        }
                        taskResult.completion_result = answer;

                        if (taskResult.id == 0)
                            taskResult = Repository.Inserted(taskResult);
                        else
                            taskResult = Repository.Updated(taskResult);
                    }
                }

                Clients.Group($"Session_{session_id}").SessionEnded(Convert.ToDateTime(sessionModel.SessionTask.end_time));
            }
        }
        public void approve(int session_id, int user_id)
        {
            SessionModel sessionModel = sessionModels.FirstOrDefault(x => x.SessionID == session_id);
            if (sessionModel != null)
                sessionModel.Students.FirstOrDefault(x => x.id == user_id).HasApprovedFinish = true;
            if (sessionModel.SessionTask.Task.is_collabrative_writing
                            && sessionModel.Students.Count == sessionModel.Students.Count(x => x.HasApprovedFinish))
            {
                finishTask(session_id);
            }
        }
        public void complete_anyway(int session_id, int user_id, string answer)
        {
            SessionModel sessionModel = sessionModels.FirstOrDefault(x => x.SessionID == session_id);
            if (sessionModel != null)
            {
                if (sessionModel.Answers.ContainsKey(user_id))
                    sessionModel.Answers[user_id] = answer;
                else
                    sessionModel.Answers.Add(user_id, answer);
                User user = sessionModel.Students.FirstOrDefault(x => x.id == user_id);
                if (user != null)
                {
                    Logger.Save($"{user.SysStr()} has pressed 'Complete Anyway' button at task id : {session_id}. Task is '{sessionModel.SessionTask.Task.title}'", new Exception());
                    using (Repository<TaskResult> Repository = new Repository<TaskResult>())
                    {
                        TaskResult taskResult = Repository.SingleSelectByQuery(x =>
                         x.session_task_id == sessionModel.SessionTask.id
                         && x.student_id == user_id);
                        if (taskResult != null)
                        {
                            taskResult.completion_result = answer;
                            taskResult.finish_time = Zaman.Simdi;
                            taskResult = Repository.Updated(taskResult);
                        }
                        else
                        {
                            taskResult = new TaskResult();
                            taskResult.session_task_id = sessionModel.SessionTask.id;
                            taskResult.teacher_id = sessionModel.SessionTask.created_by;
                            taskResult.student_id = user.id;

                            if (sessionModel.SessionTask.start_time != null)
                                taskResult.start_time = Convert.ToDateTime(sessionModel.SessionTask.start_time);
                            else
                                taskResult.start_time = Zaman.Simdi;
                            taskResult.completion_result = answer;
                            taskResult.finish_time = Zaman.Simdi;
                            taskResult = Repository.Inserted(taskResult);
                        }
                    }
                    if (sessionModel.SessionTask.Task.is_collabrative_writing)
                    {
                        Clients.Group($"Session_{session_id}").ApproveAnswer(user, answer);
                    }
                    else
                    {
                        Clients.Group($"Session_{session_id}").UserHasCompletedAnyway(user);
                    }
                }
            }
        }
        public void answer(int session_id, int user_id, string answer)
        {

            SessionModel sessionModel = sessionModels.FirstOrDefault(x => x.SessionID == session_id);
            if (sessionModel != null)
            {
                if (sessionModel.Answers.ContainsKey(user_id))
                    sessionModel.Answers[user_id] = answer;
                else
                    sessionModel.Answers.Add(user_id, answer);
                User user = sessionModel.Students.FirstOrDefault(x => x.id == user_id); if (user != null)
                {
                    bool result = false;
                    // TODO Change Answering
                    switch (sessionModel.SessionTask.Task.completion_type)
                    {
                        case "E":
                        case "ET":
                            List<TagifyValueModel> values = JsonConvert.DeserializeObject<List<TagifyValueModel>>(sessionModel.SessionTask.Task.exact_value.ToStr());
                            values.ForEach(x => x.value.Trim().ToLower());
                            string[] possibleAnswers = values.Select(x => x.value.ToLower()).ToArray();
                            result = possibleAnswers.Contains(answer.Trim().ToLower());
                            break;
                        case "W":
                        case "WT":
                            result = sessionModel.SessionTask.Task.maximum_word >= answer.Split(' ').Length && sessionModel.SessionTask.Task.minimum_word <= answer.Split(' ').Length;
                            break;
                        case "N":
                        case "T":
                            result = true;
                            break;
                        default:
                            break;
                    }
                    sessionModel.Students.FirstOrDefault(x => x.id == user_id).HasAnswered = result;
                    user.HasAnswered = result;
                    if (result)
                    {
                        sessionModel.Students.FirstOrDefault(x => x.id == user_id).HasApprovedFinish = result;
                        user.HasApprovedFinish = result;
                        using (Repository<TaskResult> Repository = new Repository<TaskResult>())
                        {
                            TaskResult taskResult = Repository.SingleSelectByQuery(x =>
                             x.session_task_id == sessionModel.SessionTask.id
                             && x.student_id == user_id);
                            if (taskResult != null)
                            {
                                taskResult.completion_result = answer;
                                taskResult.finish_time = Zaman.Simdi;
                                taskResult = Repository.Updated(taskResult);
                            }
                            else
                            {
                                taskResult = new TaskResult();
                                taskResult.session_task_id = sessionModel.SessionTask.id;
                                taskResult.teacher_id = sessionModel.SessionTask.created_by;
                                taskResult.student_id = user.id;

                                if (sessionModel.SessionTask.start_time != null)
                                    taskResult.start_time = Convert.ToDateTime(sessionModel.SessionTask.start_time);
                                else
                                    taskResult.start_time = Zaman.Simdi;
                                taskResult.completion_result = answer;
                                taskResult.finish_time = Zaman.Simdi;
                                taskResult = Repository.Inserted(taskResult);
                            }
                        }
                        if (sessionModel.SessionTask.Task.is_collabrative_writing
                            && sessionModel.Students.Count == sessionModel.Students.Count(x => x.HasApprovedFinish))
                        {

                            finishTask(session_id);
                        }
                        else if (sessionModel.SessionTask.Task.is_collabrative_writing)
                        {
                            Clients.Group($"Session_{session_id}").ApproveAnswer(user, answer);
                        }
                        else if (sessionModel.Students.Count == sessionModel.Students.Count(x => x.HasAnswered))
                        {
                            finishTask(session_id);
                        }
                        else
                        {
                            Clients.Group($"Session_{session_id}").UserAnswered(user, result);
                        }
                    }
                    else
                    {
                        Clients.Group($"Session_{session_id}").UserAnswered(user, result);
                    }
                }
            }
        }
        public void CreateSession(int session_id)
        {
            SessionModels.Add(new SessionModel(session_id));

        }
        public void DeleteSession(int session_id)
        {
            SessionModels.Remove(SessionModels.FirstOrDefault(x => x.SessionID == session_id));
        }
        public override Task OnConnected()
        {
            return base.OnConnected();
        }
        public override Task OnDisconnected(bool stopCalled)
        {
            if (!stopCalled)
                return base.OnDisconnected(stopCalled);
            SessionModel sessionModel = null;
            int session_id = 0;
            int user_id = 0;
            foreach (var item in SessionModels)
            {
                User user = item.Students.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
                if (user != null)
                {
                    user_id = user.id;
                    sessionModel = item;
                    item.Students.Remove(user);
                    if (item.Students.Count == 0)
                    {
                        Groups.Remove(Context.ConnectionId, $"Session_{item.SessionID}");
                        session_id = item.SessionID;
                    }
                    else
                    {
                        Clients.Group($"Session_{item.SessionID}").UserReadytatusChanged(user);
                        Clients.OthersInGroup($"Session_{item.SessionID}").UserStatusChanged(user, false);
                    }
                }
            }
            if (session_id != 0)
            {
                DeleteSession(session_id);
            }
            return base.OnDisconnected(stopCalled);
        }
        public override Task OnReconnected()
        {
            return base.OnReconnected();
        }
        public void new_user_connection(int session_id, int user_id)
        {
            SessionModel sessionModel = sessionModels.FirstOrDefault(s => s.SessionID == session_id);
            sessionModels = sessionModels.DistinctBy(x => x.SessionID).ToList();

            if (sessionModel == null)
            {
                sessionModel = new SessionModel(session_id);
                using (Repository<SessionTask> Repository = new Repository<SessionTask>())
                {
                    sessionModel.SessionTask = Repository.SingleSelectByQuery(s => s.id == session_id);
                }
                sessionModel.SessionTask.Task = StaticDB.Tasks.GetById(sessionModel.SessionTask.task_id);
                sessionModel.SessionTask.Task.TaskMaterials = StaticDB.TaskMaterials.Where(x => x.task_id == sessionModel.SessionTask.task_id).ToList();
                sessionModel.SessionTask.Task.TaskMaterials.ToList().ForEach(x => x.Material = StaticDB.Materials.GetById(x.material_id));
            }
            if (sessionModel.Messages.Count != 0)
            {
                foreach (Messaging messaging in sessionModel.Messages)
                {
                    Clients.Caller.addMessage(messaging);
                }
            }
            Groups.Add(Context.ConnectionId, $"Session_{session_id}");
            User user = sessionModel.Students.FirstOrDefault(x => x.id == user_id);
            sessionModel.Students = sessionModel.Students.DistinctBy(x => x.id).ToList();

            if (user == null)
            {
                user = StaticDB.Users.GetById(user_id);
                user.ConnectionId = Context.ConnectionId;
                user.IsReady = false;
                if (user.role_id == 4)
                    sessionModel.Students.Add(user);
                else
                    sessionModel.Supervisors.Add(user);
            }
            else
            {
                sessionModel.Students.FirstOrDefault(x => x.id == user_id).ConnectionId = Context.ConnectionId;
                sessionModel.Students.FirstOrDefault(x => x.id == user_id).IsReady = false;
            }

            sessionModels.Add(sessionModel);
            foreach (User us in sessionModel.Students)
            {
                Clients.Caller.UserStatusChanged(us, true);
            }
            Clients.Group($"Session_{session_id}", Context.ConnectionId).UserStatusChanged(user, true);
        }
    }

    public class SessionModel
    {
        private int sessionID;
        private string text;
        private List<Messaging> messages;
        private List<User> students;
        private List<User> supervisors;
        private Dictionary<int, string> answers;
        private SessionTask sessionTask;
        public SessionModel(int sessionID)
        {
            this.SessionID = sessionID;
            SessionTask = StaticDB.SessionTasks.GetById(SessionID);
            SessionTask.Task = StaticDB.Tasks.GetById(SessionTask.task_id);
            SessionTask.Task.TaskMaterials = StaticDB.TaskMaterials.Where(x => x.task_id == SessionTask.task_id).ToList();
            SessionTask.Task.TaskMaterials.ToList().ForEach(x => x.Material = StaticDB.Materials.GetById(x.material_id));
            Students = new List<User>();
            Messages = new List<Messaging>();
            Answers = new Dictionary<int, string>();
        }
        public int SessionID { get { return sessionID; } set { sessionID = value; } }
        public string Text { get { return text; } set { text = value; } }
        public List<Messaging> Messages { get { return messages; } set { messages = value; } }
        public List<User> Students { get { return students; } set { students = value; } }
        public List<User> Supervisors { get { return supervisors; } set { supervisors = value; } }
        public SessionTask SessionTask { get { return sessionTask; } set { sessionTask = value; } }
        public Dictionary<int, string> Answers { get { return answers; } set { answers = value; } }
    }
}