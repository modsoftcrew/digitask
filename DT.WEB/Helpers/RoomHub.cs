﻿using DT.DAL;
using DT.DTO;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using MS.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace DT.WEB
{
    [HubName("RoomHub")]
    public class RoomHub : Hub
    {
        public static RoomModel roomModel;
        public static RoomModel RoomModel { get { return roomModel; } set { roomModel = value; } }
        public void create_room(int room_id, int user_id)
        {
            Room room = StaticDB.Rooms.GetById(room_id);
            using (Repository<SessionTask> Repository = new Repository<SessionTask>())
            {
                room.SessionTasks = Repository.MultiSelectByQuery(s => s.is_active && s.room_id == room.id).ToList();
            }
            roomModel.Rooms.Add(room);
            Clients.All.addRoom(room);
            join_room(room_id, user_id);
        }
        public void join_room(int room_id, int user_id)
        {
            if (RoomModel.Rooms.GetById(room_id).RoomStudents.Count(x => x.student_id == user_id) > 0)
                return;
            User user = StaticDB.Users.GetById(user_id);
            Room room = RoomModel.Rooms.GetById(room_id);

            RoomStudent roomStudent = new RoomStudent
            {
                room_id = room_id,
                student_id = user_id,
                join_time = Zaman.Simdi,
                User = user            
            };

            //bool already_exist = false;
            //foreach (Room _room in RoomModel.Rooms)
            //{
            //    foreach(RoomStudent roomStu in _room.RoomStudents)
            //    {
            //        if (roomStu.student_id == roomStudent.student_id)
            //        {
            //            already_exist = true;
            //        }
            //    }
            //}

            //if (already_exist == false)
            //{
            //    if (room != null)
            //    {
            //        ++room.current_range;

            //        RoomModel.Rooms.GetById(room_id).RoomStudents.Add(roomStudent);
            //        Clients.All.addStudent(roomStudent);
            //    }
            //}

            if (room != null)
            {
                ++room.current_range;

                RoomModel.Rooms.GetById(room_id).RoomStudents.Add(roomStudent);
                Clients.All.addStudent(roomStudent, room);

                if(room.SessionTasks.Count != 0)
                {
                    if (room.group_range == room.current_range)
                    {
                        foreach (RoomStudent rs in room.RoomStudents)
                        {
                            Clients.Client(rs.User.ConnectionId).showReady(room.SessionTasks.ToList()[0].id);
                        }
                    }
                }

            }


        }
        public void leave_room(int room_id, int user_id)
        {
            if (RoomModel.Rooms.GetById(room_id).RoomStudents.Count(x => x.student_id == user_id) == 0)
                return;
            Room room = RoomModel.Rooms.GetById(room_id);
            if (room != null)
            {
                --room.current_range;
                RoomStudent roomStudent = room.RoomStudents.FirstOrDefault(x => x.student_id == user_id);
                RoomModel.Rooms.GetById(room_id).RoomStudents.Remove(roomStudent);
                Clients.All.removeStudent(roomStudent);
            }
        }
        public override Task OnConnected()
        {
            return base.OnConnected();
        }
        public override Task OnDisconnected(bool stopCalled)
        {
            User user = RoomModel.Users.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (user != null)
            {
                RoomModel.Users.Remove(user);
                Clients.Others.UserStatusChanged(user, false);
            }
            return base.OnDisconnected(stopCalled);
        }
        public override Task OnReconnected()
        {
            return base.OnReconnected();
        }
        public void new_user_connection(int user_id)
        {
            if (RoomModel == null)
            {
                RoomModel = new RoomModel()
                {
                    Rooms = StaticDB.Rooms
                };
            }
            if (RoomModel.Rooms.Count != 0)
            {
                foreach (Room room in RoomModel.Rooms)
                {
                    room.RoomStudents.ToList().ForEach(x => x.User = StaticDB.Users.GetById(x.student_id));
                    Clients.Caller.addRoom(room);
                }
            }
            User user = StaticDB.Users.GetById(user_id);
            user.ConnectionId = Context.ConnectionId;
            RoomModel.Users.Add(user);

            foreach (User us in RoomModel.Users)
            {
                Clients.Caller.UserStatusChanged(us, true);
            }
            Clients.Others.UserStatusChanged(user, true);
        }
    }

    public class RoomModel
    {
        private List<Room> rooms;
        private List<User> users;
        public RoomModel()
        {
            Users = new List<User>();
            Rooms = new List<Room>();
        }
        public List<Room> Rooms { get { return rooms; } set { rooms = value; } }
        public List<User> Users { get { return users; } set { users = value; } }
    }
}