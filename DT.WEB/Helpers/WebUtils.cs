﻿using MS.BLL;
using Newtonsoft.Json;
using DT.DAL;
using DT.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Mvc;
using System.Web.Helpers;

namespace DT.WEB
{
    public static class WebUtils
    {
        public static class Cookies
        {
            public static readonly string cookie_header = "DT_";
            public static void Save(Controller Controller, string key, object entity)
            {
                HttpCookie cookie = Controller.Request.Cookies[cookie_header + key];
                if (cookie != null)
                {
                    string json = entity.ToJson();
                    cookie.Value = HttpUtility.UrlEncode(json);
                }
                else
                {
                    string json = entity.ToJson();
                    cookie = new HttpCookie(cookie_header + key);
                    cookie.Value = HttpUtility.UrlEncode(json);
                }
                cookie.Expires = DateTime.Now.AddYears(1);
                Controller.Response.Cookies.Add(cookie);
            }
            public static void Save(string key, object entity)
            {
                HttpCookie cookie = HttpContext.Current.Request.Cookies[cookie_header + key];
                if (cookie != null)
                    cookie.Value = HttpUtility.UrlEncode(entity.ToJson());
                else
                {
                    cookie = new HttpCookie(cookie_header + key);
                    cookie.Value = HttpUtility.UrlEncode(entity.ToJson());
                }
                cookie.Expires = DateTime.Now.AddYears(1);
                HttpContext.Current.Request.Cookies.Add(cookie);
            }
            public static HttpResponseMessage Save(HttpResponseMessage response, string key, object entity)
            {
                CookieHeaderValue cookie = new CookieHeaderValue(cookie_header + key, entity.ToJson());
                cookie.Expires = DateTime.Now.AddYears(1);
                response.Headers.AddCookies(new CookieHeaderValue[] { cookie });
                return response;
            }
            public static void Save(Controller Controller, string key, string value)
            {
                HttpCookie cookie = Controller.Request.Cookies[cookie_header + key];
                if (cookie != null)
                    cookie.Value = HttpUtility.UrlEncode(value);
                else
                {
                    cookie = new HttpCookie(cookie_header + key);
                    cookie.Value = HttpUtility.UrlEncode(value);
                }
                cookie.Expires = DateTime.Now.AddYears(1);
                if (value == null)
                    Controller.Request.Cookies.Remove(cookie_header + key);
                else
                    Controller.Request.Cookies.Add(cookie);
            }
            public static void Save(HttpContextBase Context, string key, string value)
            {
                HttpCookie cookie = Context.Request.Cookies[cookie_header + key];
                if (cookie != null)
                    cookie.Value = HttpUtility.UrlEncode(value);
                else
                {
                    cookie = new HttpCookie(cookie_header + key);
                    cookie.Value = HttpUtility.UrlEncode(value);
                }
                cookie.Expires = DateTime.Now.AddYears(1);
                if (value == null)
                    Context.Request.Cookies.Remove(cookie_header + key);
                else
                    Context.Request.Cookies.Add(cookie);
            }
            public static string Get(string key)
            {
                HttpCookie cookie = HttpContext.Current.Request.Cookies[cookie_header + key];
                if (cookie != null)
                {
                    return cookie.Value;
                }
                else
                {
                    return "";
                }
            }
            public static T Get<T>(HttpContextBase Context, string key)
            {
                HttpCookie cookie = Context.Request.Cookies[cookie_header + key];
                if (cookie != null)
                    return JsonConvert.DeserializeObject<T>(HttpUtility.UrlDecode(cookie.Value));
                else
                {
                    return default(T);
                }
            }
            public static T Get<T>(Controller Controller, string key)
            {
                HttpCookie cookie = Controller.Request.Cookies[cookie_header + key];
                if (cookie != null)
                    return JsonConvert.DeserializeObject<T>(HttpUtility.UrlDecode(cookie.Value));
                else
                {
                    return default(T);
                }
            }
            public static T Get<T>(string key)
            {
                HttpCookie cookie = HttpContext.Current.Request.Cookies[cookie_header + key];
                if (cookie != null)
                {
                    if (typeof(T).Equals(typeof(User)))
                    {
                        User user = JsonConvert.DeserializeObject<User>(HttpUtility.UrlDecode(cookie.Value));
                        Language language = StaticDB.Languages.FirstOrDefault(x => x.iso_code.Equals(user.ui_language));
                        Save("Language", language.ToJson());
                    }
                    return JsonConvert.DeserializeObject<T>(HttpUtility.UrlDecode(cookie.Value));
                }
                else
                {
                    return default(T);
                }
            }
            public static T Get<T>(HttpActionContext Context, string key)
            {
                var cookieHeaderValue = Context.Request.Headers.GetCookies(cookie_header + key).FirstOrDefault();
                if (cookieHeaderValue == null)
                    return default(T);

                var cookie = cookieHeaderValue.Cookies.Where(c => c.Name.Equals(cookie_header + key)).FirstOrDefault();
                if (cookie == null)
                    return default(T);

                return JsonConvert.DeserializeObject<T>(HttpUtility.UrlDecode(cookie.Value));
            }
            public static List<KeyValuePair<string, string>> GetAll(HttpRequestBase Request, string startsWith)
            {
                var keys = Request.Cookies.AllKeys.Where(k => k.StartsWith(startsWith));
                List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
                foreach (var key in keys)
                {
                    list.Add(new KeyValuePair<string, string>(key, Request.Cookies[key].Value));
                }
                return list;
            }
            public static List<KeyValuePair<string, string>> GetAll(HttpContextBase Context, string startsWith)
            {
                var keys = Context.Request.Cookies.AllKeys.Where(k => k.StartsWith(startsWith));
                List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
                foreach (var key in keys)
                {
                    list.Add(new KeyValuePair<string, string>(key, HttpUtility.UrlDecode(Context.Request.Cookies[key].Value)));
                }
                return list;
            }
            public static void Delete(Controller Controller, string key)
            {
                try
                {
                    HttpCookie cookie = Controller.Request.Cookies[cookie_header + key];
                    if (cookie != null)
                    {
                        Controller.Response.Cookies[cookie_header + key].Expires = DateTime.Now.AddDays(-1);
                    }
                }
                catch (Exception)
                {

                }
            }
            public static void Delete(HttpContextBase Context, string key)
            {
                try
                {
                    HttpCookie cookie = Context.Request.Cookies[cookie_header + key];
                    if (cookie != null)
                    {
                        Context.Response.Cookies[cookie_header + key].Expires = DateTime.Now.AddDays(-1);
                    }
                }
                catch (Exception)
                {

                }
            }
            public static void SetString(string key, string value)
            {
                HttpCookie cookie = HttpContext.Current.Request.Cookies[cookie_header + key];
                if (cookie != null)
                    cookie.Value = value;
                else
                {
                    cookie = new HttpCookie(cookie_header + key);
                    cookie.Value = HttpUtility.UrlEncode(value);
                }
                cookie.Expires = DateTime.Now.AddYears(1);
                HttpContext.Current.Request.Cookies.Add(cookie);
            }
            public static string GetString(string key)
            {
                HttpCookie cookie = HttpContext.Current.Request.Cookies[cookie_header + key];
                if (cookie != null)
                    return cookie.Value;
                else
                    return "";
            }
        }
        public static string GetStringByResourceType(Type type, string key)
        {
            User user = JsonConvert.DeserializeObject<User>(HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies[Cookies.cookie_header + "User"].Value));
            if (user.ui_language == null)
                user.ui_language = "en";
            return new ResourceManager(type)
                .GetResourceSet(new System.Globalization.CultureInfo(user.ui_language), true, true)
                .GetString(key);
        }
        public static System.Collections.DictionaryEntry[] GetResourcesByType(Type type, bool orderByKey = false)
        {
            User user = JsonConvert.DeserializeObject<User>(HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies[Cookies.cookie_header + "User"].Value));
            if (user.ui_language == null)
                user.ui_language = "en";
            var arr = new ResourceManager(type)
                .GetResourceSet(new System.Globalization.CultureInfo(user.ui_language), true, true)
                .OfType<System.Collections.DictionaryEntry>()
                .ToArray();
            if (orderByKey)
                return arr.OrderBy(x => x.Key).ToArray();
            else
                return arr.OrderBy(x => x.Value).ToArray();
        }
        public static User User(this HttpRequestBase Request)
        {
            User user = Cookies.Get<User>(Request.RequestContext.HttpContext, "User");
            if (user == null)
                return null;
            user.Role = StaticDB.Roles.GetById(user.role_id);
            return user;
        }
        public static Language Language(this HttpRequestBase Request)
        {
            Language language = Cookies.Get<Language>(Request.RequestContext.HttpContext, "Language");
            if (language == null)
            {
                language = StaticDB.Languages.FirstOrDefault(x => x.iso_code == Request.User().ui_language);
            }
            return language;
        }
        public static MvcHtmlString GetFlag(this HttpRequestBase Request, Language language = null, bool showTitle = true)
        {
            string title = "";
            if (language == null)
                language = Request.Language();
            else
                title = showTitle ? language.name : "";
            ResourceManager RM = new ResourceManager(typeof(LOCAL.Flags));
            byte[] flag = (byte[])RM.GetObject(language == null ? "tr" : language.iso_code);
            if(flag != null)
            {
                string svg = Encoding.UTF8.GetString(flag);
                return new MvcHtmlString($"<div class=\"flag-icon\">{svg} {title}</div>");
            }
            return new MvcHtmlString($"<div class=\"flag-icon\">{title}</div>");
        }
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }
        public static string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }
        public static class Settings
        {
            public static void Set(string key, object value)
            {
                try
                {
                    Setting setting = new Setting();
                    setting.key = key;
                    setting.value = value.ToJson();
                    using (Repository<Setting> Repository = new Repository<Setting>())
                    {
                        Setting s = Repository.SingleSelectByQuery(x => x.key == key);
                        if (s == null)
                            Repository.Inserting(setting);
                        else
                        {
                            s.key = key;
                            s.value = setting.value;
                            Repository.Updating(s);
                        }
                    }
                    StaticDB.SetProperty("Setting");
                }
                catch (Exception)
                {

                }
            }
            public static T Get<T>(string key)
            {
                try
                {
                    Setting setting = StaticDB.Settings.FirstOrDefault(s => s.key == key);
                    if (setting != null)
                        return JsonConvert.DeserializeObject<T>(HttpUtility.UrlDecode(setting.value));
                }
                catch (Exception)
                {

                }
                return default(T);
            }
            public static string GetValue(string key)
            {
                try
                {
                    Setting setting = StaticDB.Settings.FirstOrDefault(s => s.key == key);
                    if (setting != null)
                        return setting.value;
                }
                catch (Exception)
                {

                }
                return "";
            }
        }
        public static DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            DataTable dt = new DataTable();
            using (StreamReader sr = new StreamReader(strFilePath))
            {
                string[] headers = sr.ReadLine().Split(',');
                foreach (string header in headers)
                {
                    dt.Columns.Add(header);
                }

                while (!sr.EndOfStream)
                {
                    string[] rows = sr.ReadLine().Split(',');
                    if (rows.Length > 1)
                    {
                        DataRow dr = dt.NewRow();
                        for (int i = 0; i < headers.Length; i++)
                        {
                            dr[i] = rows[i].Trim();
                        }
                        dt.Rows.Add(dr);
                    }
                }

            }


            return dt;
        }
        public static string SaveFile(HttpPostedFileBase file)
        {
            try
            {
                if (string.IsNullOrEmpty(file.FileName))
                    return "";
                string file_name = Zaman.Simdi.Ticks + "_" + file.FileName.Review();
                string fname = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/Upload"), file_name);
                System.IO.File.WriteAllBytes(fname, file.ToByteArray());
                var request = HttpContext.Current.Request;
                string urlBase = string.Format("{0}://{1}{2}/", request.Url.Scheme, request.Url.Authority, (new System.Web.Mvc.UrlHelper(request.RequestContext)).Content("~"));
                return urlBase + "Upload/" + file_name;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public static string SaveFile(string base64)
        {
            try
            {
                if (string.IsNullOrEmpty(base64))
                    return "";
                string file_name = Zaman.Simdi.Ticks + "." + base64.Split(';')[0].Split('/')[1];
                string fname = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/Upload"), file_name);
                byte[] fileBytes = Convert.FromBase64String(base64.Split(';')[1].Split(',')[1]);
                System.IO.File.WriteAllBytes(fname, fileBytes);
                var request = HttpContext.Current.Request;
                string urlBase = string.Format("{0}://{1}{2}/", request.Url.Scheme, request.Url.Authority, (new System.Web.Mvc.UrlHelper(request.RequestContext)).Content("~"));
                return urlBase + "Upload/" + file_name;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public static string GetStatusColor(string status)
        {
            if (status.Equals("Started"))
                return "#FFEBCD";
            if (status.Equals("Continue"))
                return "#FAA460";
            if (status.Equals("Completed"))
                return "#98FB98";
            if (status.Equals("Reported"))
                return "#87CEEB";
            if (status.Equals("Archieved"))
                return "#D3D3D3";
            return "#fff";
        }
        public static DataTable ConvertXSLXtoDataTable(string strFilePath, string connString)
        {
            OleDbConnection oledbConn = new OleDbConnection(connString);
            DataTable dt = new DataTable();
            try
            {
                oledbConn.Open();

                DataTable dtSchema = oledbConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                string name = dtSchema.Rows[0].Field<string>("TABLE_NAME");
                using (OleDbCommand cmd = new OleDbCommand("SELECT * FROM [" + name + "]", oledbConn))
                {
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    oleda.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    oleda.Fill(ds);

                    dt = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {

                oledbConn.Close();
            }

            return dt;

        }
        public static IHtmlString ConvertToCode(this HtmlHelper helper, object obj)
        {
            if (obj == null)
                return helper.Raw("");
            string body = CreateJsonCode(obj);
            return helper.Raw(body);
        }

        public static string CreateJsonCode(object obj)
        {
            string result = "<pre><code>{0}</code></pre>";
            string code = obj.ToJson()
                .Replace("\\r", " ")
                .Replace("\\n", "<br/>")
                .Replace("\\r\\n", "<br/>")
                .Trim();
            string body = string.Format(result, code);
            return body;
        }

        public static string GetModelStateError(ModelStateDictionary modelState)
        {
            string errorMessage = "<ul>";
            foreach (var item in modelState.Values.Where(x => x.Errors.Count > 0))
            {
                foreach (var error in item.Errors)
                {
                    errorMessage += "<li style=\"text-align:left;\">" + error.ErrorMessage + "</li>";
                }
            }
            errorMessage += "</ul>";
            return errorMessage;
        }
        public static void FillStaticDB()
        {
            StaticDB.SetProperty(nameof(BookmarkedMaterial));
            StaticDB.SetProperty(nameof(BookmarkedTask));
            StaticDB.SetProperty(nameof(BookmarkedSequence));
            StaticDB.SetProperty(nameof(_Class));
            StaticDB.SetProperty(nameof(Cluster));
            StaticDB.SetProperty(nameof(ClusterStudent));
            StaticDB.SetProperty(nameof(ClassStudent));
            StaticDB.SetProperty(nameof(Language));
            StaticDB.SetProperty(nameof(Material));
            StaticDB.SetProperty(nameof(Permission));
            StaticDB.SetProperty(nameof(ProficiencyLanguage));
            StaticDB.SetProperty(nameof(Rating));
            StaticDB.SetProperty(nameof(Role));
            StaticDB.SetProperty(nameof(Room));
            StaticDB.SetProperty(nameof(RoomStudent));
            StaticDB.SetProperty(nameof(Sequence));
            StaticDB.SetProperty(nameof(SequenceTask));
            StaticDB.SetProperty(nameof(SessionTask));
            StaticDB.SetProperty(nameof(Setting));
            StaticDB.SetProperty(nameof(Tag));
            StaticDB.SetProperty(nameof(TargetLanguage));
            StaticDB.SetProperty(nameof(TaskAction));
            StaticDB.SetProperty(nameof(TaskEditor));
            StaticDB.SetProperty(nameof(TaskMaterial));
            StaticDB.SetProperty(nameof(TaskMetaHelpLink));
            StaticDB.SetProperty(nameof(_Task));
            StaticDB.SetProperty(nameof(TaskTag));
            StaticDB.SetProperty(nameof(User));
        }

        public static string GetAttributeDisplayName(this PropertyInfo property)
        {
            var atts = property.GetCustomAttributes(
                typeof(DisplayNameAttribute), true);
            if (atts.Length == 0)
                return property.Name;
            return (atts[0] as DisplayNameAttribute).DisplayName;
        }
        public static string GetMetaDisplayName(this PropertyInfo property)
        {
            var atts = property.DeclaringType.GetCustomAttributes(
                typeof(MetadataTypeAttribute), true);
            if (atts.Length == 0)
                return property.Name;

            var metaAttr = atts[0] as MetadataTypeAttribute;
            var metaProperty =
                metaAttr.MetadataClassType.GetProperty(property.Name);
            if (metaProperty == null)
                return property.Name;
            return GetAttributeDisplayName(metaProperty);
        }
        public static string Resize(this byte[] data)
        {
            using (var ms = new MemoryStream(data))
            {
                var image = Image.FromStream(ms);

                var ratioX = (double)600 / image.Width;
                var ratioY = (double)200 / image.Height;
                var ratio = Math.Min(ratioX, ratioY);

                var width = (int)(image.Width * ratio);
                var height = (int)(image.Height * ratio);

                var newImage = new Bitmap(width, height);
                Graphics.FromImage(newImage).DrawImage(image, 0, 0, width, height);
                Bitmap bmp = new Bitmap(newImage);

                ImageConverter converter = new ImageConverter();

                data = (byte[])converter.ConvertTo(bmp, typeof(byte[]));

                return "data:image/*;base64," + Convert.ToBase64String(data);
            }
        }
        public static string ToBase64(this Bitmap bmp)
        {
            ImageConverter converter = new ImageConverter();
            byte[] data = (byte[])converter.ConvertTo(bmp, typeof(byte[]));
            return "data:image/*;base64," + Convert.ToBase64String(data);
        }
        public static byte[] ToByteArray(this Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, imageIn.RawFormat);
                return ms.ToArray();
            }
        }
        public static string MakeActiveClass(this UrlHelper urlHelper, string controller, string action = null, object parameter = null)
        {
            string result = "";
            if (parameter == null)
                parameter = "-";
            string controllerName = urlHelper.RequestContext.RouteData.Values["controller"].ToString();
            string actionName = urlHelper.RequestContext.RouteData.Values["action"].ToString();
            string param = urlHelper.RequestContext.RouteData.Values["id"] != null ? urlHelper.RequestContext.RouteData.Values["id"].ToString() : "";

            if (action != null &&
                controllerName.Equals(controller, StringComparison.OrdinalIgnoreCase) &&
                actionName.Equals(action, StringComparison.OrdinalIgnoreCase) &&
                param.Equals(parameter.ToString(), StringComparison.OrdinalIgnoreCase))
                result = "active";
            return result;
        }
        public static byte[] ToByteArray(this HttpPostedFileBase file)
        {
            byte[] imgData;
            using (var reader = new BinaryReader(file.InputStream))
            {
                imgData = reader.ReadBytes(file.ContentLength);
            }
            return imgData;
        }
        public static string Theme(string defaultTheme = "MaterialCompact")
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["Theme"];
            if (cookie != null)
                return cookie.Value;
            else
            {
                return defaultTheme;
            }
        }

        public static string GetDName<T>()
        {
            var displayName = typeof(T).GetCustomAttributes(typeof(DisplayNameAttribute), true).SingleOrDefault() as DisplayNameAttribute;

            if (displayName != null)
                return displayName.DisplayName;

            return "";
        }
        public static string GetDName(this Type type)
        {
            var displayName = type.GetCustomAttributes(typeof(DisplayNameAttribute), true).SingleOrDefault() as DisplayNameAttribute;

            if (displayName != null)
                return displayName.DisplayName;

            return "";
        }
        public static string GetDName(this PropertyInfo property)
        {
            var displayName = property.GetCustomAttribute(typeof(DisplayNameAttribute)) as DisplayNameAttribute;

            if (displayName != null)
                return displayName.DisplayName;

            return property.Name;
        }
        public static bool IsReq(this PropertyInfo property)
        {
            object[] attrs = property.GetCustomAttributes(true);
            foreach (object attr in attrs)
            {
                RequiredAttribute reqAttr = attr as RequiredAttribute;
                if (reqAttr != null)
                    return true;
            }
            return false;
        }
        public static void Equalize(this object target, object source)
        {
            foreach (PropertyInfo prop in target.GetType().GetProperties().Where(p => !p.GetMethod.IsVirtual && p.Name != "created_date" && p.Name != "modified_date" && p.Name != "is_active" && p.Name != "details"))
            {
                var value = source.GetType().GetProperty(prop.Name).GetValue(source);
                target.GetType().GetProperty(prop.Name).SetValue(target, value);
            }
        }

        public static bool ValidateActionParameters(params int[] parameters)
        {
            foreach (int parameter in parameters)
            {
                if (parameter <= 0)
                    return false;
            }
            return true;
        }

        #region Network Tools
        public static string GetIp => HttpContext.Current.Request.UserHostName;
        public static string GetMAC
        {
            get
            {
                NetworkInterface[] networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
                string macAddress = string.Empty;
                foreach (NetworkInterface networkInterface in networkInterfaces)
                {
                    if (macAddress == string.Empty)
                    {
                        IPInterfaceProperties properties = networkInterface.GetIPProperties();
                        macAddress = networkInterface.GetPhysicalAddress().ToString();
                    }
                }
                return macAddress;
            }
        }
        public static string GetUserAgent => HttpContext.Current.Request.UserAgent;
        #endregion

        public static string ToEnglish(this string phrase)
        {
            phrase = phrase.Replace("ı", "i");
            phrase = phrase.Replace("ö", "o");
            phrase = phrase.Replace("ü", "u");
            phrase = phrase.Replace("ç", "c");
            phrase = phrase.Replace("ğ", "g");
            phrase = phrase.Replace("ş", "s");
            phrase = phrase.Replace("İ", "I");
            phrase = phrase.Replace("Ö", "O");
            phrase = phrase.Replace("Ü", "U");
            phrase = phrase.Replace("Ç", "C");
            phrase = phrase.Replace("Ğ", "G");
            phrase = phrase.Replace("Ş", "S");
            return phrase;
        }
        public static string FormatPhone(string phone)
        {
            if (phone == null)
                return "";
            string result = phone.Replace(" ", "").Trim();
            if (result.StartsWith("5"))
                result = "+90" + result;
            if (result.StartsWith("0"))
                result = "+9" + result;
            if (result.StartsWith("9"))
                result = "+" + result;
            return result;
        }
        public static string Review(this string str)
        {
            str = str.Trim();
            str = Regex.Replace(str.ToLower(), "/[^a-z0-9]/gi", "_");
            str = str.Replace(" ", "_");
            str = str.Replace("ı", "i");
            str = str.Replace("ö", "o");
            str = str.Replace("ü", "u");
            str = str.Replace("ç", "c");
            str = str.Replace("ğ", "g");
            str = str.Replace("ş", "s");
            return str;
        }
        public static bool ToBool(this bool? b)
        {
            if (b == null)
                return false;
            bool result = false;
            bool.TryParse(b.ToString(), out result);
            return result;
        }
        public static decimal Ondalik(string str)
        {
            try
            {
                return Convert.ToDecimal(str.Replace('.', ','));
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static decimal Ondalik(decimal? dec)
        {
            try
            {
                return Convert.ToDecimal(dec);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static double Ondalik(double? dob)
        {
            try
            {
                return Convert.ToDouble(dob);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static double ToDouble(this object val)
        {
            try
            {
                return Convert.ToDouble(val);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static int Tamsayi(object obj)
        {
            try
            {
                return Convert.ToInt32(obj.ToString());
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static long Long(string longStr)
        {
            try
            {
                return Convert.ToInt64(longStr);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static string Encrypt(this string str)
        {
            return Cipher.Encrypt(str).ToString();
        }
        public static string Decrypt(this string str)
        {
            return Cipher.Decrypt(str).ToString();
        }
    }
}