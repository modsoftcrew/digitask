﻿using DT.DAL;
using DT.DTO;
using MS.BLL;
using MS.BLL.Helpers;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Web;

namespace DT.WEB
{
    public static class DTHelper
    {
        #region Mailings
        public static bool SendConfirmationMail(User user)
        {
            string subscription_params = $"mail={user.mail};status=false".ToBase64();
            string mail = LOCAL.Mailings.Membership.MailConfirmation
                            .Replace("{{SITE_URL}}",LOCAL.ApplicationInfo.ApplicationWebsite)
                            .Replace("{{APP_NAME}}", LOCAL.ApplicationInfo.ApplicationName)
                            .Replace("{{USER_MAIL}}", user.mail)
                            .Replace("{{CONFIRMATION_LINK}}", $"{Credentials.SystemCredentials.RootUrl}verify-email?mail={HttpUtility.UrlEncode(user.mail.Encrypt())}")
                            .Replace("{{HELP_MAIL}}", SettingsManager.Get("HELP_MAIL"))
                            .Replace("{{FAQ_LINK}}", SettingsManager.Get("FAQ_LINK"))
                            .Replace("{{FB_LINK}}", SettingsManager.Get("FB_LINK"))
                            .Replace("{{TW_LINK}}", SettingsManager.Get("TW_LINK"))
                            .Replace("{{IN_LINK}}", SettingsManager.Get("IN_LINK"))
                            .Replace("{{YT_LINK}}", SettingsManager.Get("YT_LINK"))
                            .Replace("{{PRIVACY_LINK}}", SettingsManager.Get("PRIVACY_LINK"))
                            .Replace("{{UNSUBSCRIBE_LINK}}", $"{Credentials.SystemCredentials.RootUrl}subscription?code={subscription_params}");
            return Mailer.Send(user.mail,LOCAL.Mailings.Membership.MailConfirmationSubject, mail, true);
        }
        public static bool SendForgotPasswordMail(User user)
        {
            string subscription_params = $"mail={user.mail};status=false".ToBase64();
            string mail = LOCAL.Mailings.Membership.ForgotMail
                            .Replace("{{SITE_URL}}",LOCAL.ApplicationInfo.ApplicationWebsite)
                            .Replace("{{APP_NAME}}", LOCAL.ApplicationInfo.ApplicationName)
                            .Replace("{{USER_MAIL}}", user.mail)
                            .Replace("{{RESET_LINK}}", $"{Credentials.SystemCredentials.RootUrl}reset-link?mail={HttpUtility.UrlEncode(user.mail.Encrypt())}")
                            .Replace("{{HELP_MAIL}}", SettingsManager.Get("HELP_MAIL"))
                            .Replace("{{FAQ_LINK}}", SettingsManager.Get("FAQ_LINK"))
                            .Replace("{{FB_LINK}}", SettingsManager.Get("FB_LINK"))
                            .Replace("{{TW_LINK}}", SettingsManager.Get("TW_LINK"))
                            .Replace("{{IN_LINK}}", SettingsManager.Get("IN_LINK"))
                            .Replace("{{YT_LINK}}", SettingsManager.Get("YT_LINK"))
                            .Replace("{{PRIVACY_LINK}}", SettingsManager.Get("PRIVACY_LINK"))
                            .Replace("{{UNSUBSCRIBE_LINK}}", $"{Credentials.SystemCredentials.RootUrl}subscription?code={subscription_params}");
            return Mailer.Send(user.mail,LOCAL.Mailings.Membership.MailConfirmationSubject, mail, true);
        }


        public static List<T> Filter<T>(this List<T> list, string prop, string value)
        {
            PropertyInfo property = typeof(T).GetProperty(prop);
            if (property == null)
                return list;
            value = value.TrimStart('(').TrimEnd(')');
            return (from t in list
                    where
                    (property.GetValue(t).ToStr().ToLower().Contains(value))
                    select t).ToList();
        }
        public static bool SendTaskAssignmentMail(User user, SessionTask sessionTask)
        {
            string subject = LOCAL.Objects.Entity._Task;
            string code = $"user_id={user.id};session_id={sessionTask.id}";
            string url = $"{Credentials.SystemCredentials.RootUrl}session?code={code.ToBase64()}";

            var qrGenerator = new QRCodeGenerator();
            var qrCodeData = qrGenerator.CreateQrCode(url, QRCodeGenerator.ECCLevel.H);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);
            string qrCodeImageStr = qrCodeImage.ToBase64();
            string subscription_params = $"mail={user.mail};status=false".ToBase64();
            string mail = LOCAL.Mailings.TaskAssignment.AssignedTask
                            .Replace("{{SITE_URL}}", LOCAL.ApplicationInfo.ApplicationWebsite)
                            .Replace("{{APP_NAME}}", LOCAL.ApplicationInfo.ApplicationName)
                            .Replace("{{TASK}}", subject)
                            .Replace("{{START_DATE}}", sessionTask.planned_start_time.ToString("dd.MM.yyyy, HH:mmm"))
                            .Replace("{{LINK}}", url)
                            .Replace("{{QR_CODE}}", qrCodeImageStr)
                            .Replace("{{HELP_MAIL}}", SettingsManager.Get("HELP_MAIL"))
                            .Replace("{{FAQ_LINK}}", SettingsManager.Get("FAQ_LINK"))
                            .Replace("{{FB_LINK}}", SettingsManager.Get("FB_LINK"))
                            .Replace("{{TW_LINK}}", SettingsManager.Get("TW_LINK"))
                            .Replace("{{IN_LINK}}", SettingsManager.Get("IN_LINK"))
                            .Replace("{{YT_LINK}}", SettingsManager.Get("YT_LINK"))
                            .Replace("{{PRIVACY_LINK}}", SettingsManager.Get("PRIVACY_LINK"))
                            .Replace("{{UNSUBSCRIBE_LINK}}", $"{Credentials.SystemCredentials.RootUrl}subscription?code={subscription_params}");
            return Mailer.Send(user.mail, string.Format(LOCAL.Mailings.TaskAssignment.AssignmentSubject,subject), mail, true);
        }
        public static bool SendSequenceAssignmentMail(User user, List<SessionTask> sessionTasks)
        {
            string planning = "";
            foreach (SessionTask sessionTask in sessionTasks)
            {
                planning += string.Format(
                        "<tr style=\"height: 18px;\">" +
                            "<td style=\"width: 25%; height: 18px;\">{0}</td>" +
                            "<td style=\"width: 25%; height: 18px; text-align: center;\">{1}</td>" +
                            "<td style=\"width: 28.9939%; height: 18px; text-align: center;\">{2}</td>" +
                            "<td style=\"width: 21.0061%; height: 18px; text-align: center;\">{3}</td>" +
                        "</tr>{4}", sessionTask.Task.title, sessionTask.Task.information_gap, sessionTask.Task.target_subject, sessionTask.planned_start_time.ToString("dd.MM.yyyy, HH:mm"), Environment.NewLine);
            }
            string subscription_params = $"mail={user.mail};status=false".ToBase64();
            string mail = LOCAL.Mailings.TaskAssignment.AssignedSequence
                            .Replace("{{SITE_URL}}", LOCAL.ApplicationInfo.ApplicationWebsite)
                            .Replace("{{APP_NAME}}", LOCAL.ApplicationInfo.ApplicationName)
                            .Replace("{{TASK}}", LOCAL.Objects.Entity.Sequence)
                            .Replace("{{PLANNING}}", planning)
                            .Replace("{{HELP_MAIL}}", SettingsManager.Get("HELP_MAIL"))
                            .Replace("{{FAQ_LINK}}", SettingsManager.Get("FAQ_LINK"))
                            .Replace("{{FB_LINK}}", SettingsManager.Get("FB_LINK"))
                            .Replace("{{TW_LINK}}", SettingsManager.Get("TW_LINK"))
                            .Replace("{{IN_LINK}}", SettingsManager.Get("IN_LINK"))
                            .Replace("{{YT_LINK}}", SettingsManager.Get("YT_LINK"))
                            .Replace("{{PRIVACY_LINK}}", SettingsManager.Get("PRIVACY_LINK"))
                            .Replace("{{UNSUBSCRIBE_LINK}}", $"{Credentials.SystemCredentials.RootUrl}subscription?code={subscription_params}");
            return Mailer.Send(user.mail, string.Format(LOCAL.Mailings.TaskAssignment.AssignmentSubject, LOCAL.Objects.Entity.Sequence), mail, true);
        }
        #endregion



        public static void SetTaskUsage(int id)
        {
            using (Ctx ctx = new Ctx())
            {
                ctx.Database.ExecuteSqlCommand(string.Format(LOCAL.Objects.DBScripts.SetTaskUsage, id));
            }
            StaticDB.SetProperty(nameof(_Task));
        }

        public static void SetMaterialUsage(int id)
        {
            using (Ctx ctx = new Ctx())
            {
                ctx.Database.ExecuteSqlCommand(string.Format(LOCAL.Objects.DBScripts.SetMaterialUsage, id));
            }
        }
    }
}