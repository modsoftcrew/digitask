﻿using DT.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DT.WEB
{
    public class Yetkilendir : AuthorizeAttribute
    {
        private readonly string[] allowedroles;
        private Role Role;
        private User CurrentUser;

        public Yetkilendir(params string[] roles)
        {
            this.allowedroles = roles;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            try
            {
                bool authorize = false;
                this.Role = httpContext.Session["Role"] as Role;
                this.CurrentUser = httpContext.Session["User"] as User;

                foreach (var role in allowedroles)
                {
                    if (authorize = (role == this.Role.title))
                        break;
                }
                return authorize;
            }
            catch (Exception)
            {
                return false;
            }
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            try
            {
                if (this.CurrentUser != null)
                {
                    filterContext.HttpContext.Session["ErrorMessage"] = "Yetkisiz Giriş";
                }
                else
                {
                    filterContext.HttpContext.Session["ErrorMessage"] = "Yetkisiz Giriş";
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Account", action = "Start" }));
                    return;               
                }
                filterContext.HttpContext.Session["PageEx"] = new Exception("Yetkisiz giriş tespit edildi. Durum sistem günlüğüne eklendi. İlgili sayfaya giriş izniniz olduğunu düşünüyorsanız lütfen sistem yöneticiniz ile irtibata geçiniz.", new Exception("\"" + filterContext.HttpContext.Request.Url.ToString() + "\" adresine " + CurrentUser.ToString() + " tarafından yetkisiz giriş isteği"));
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Home", action = "ErrorPage" }));
            }
            catch (Exception ex)
            {
                filterContext.HttpContext.Session["ErrorMessage"] = "Sistem İç Hatası";
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Home", action = "ErrorPage" }));
            }
        }
    }
    public static class Yetki
    {

        #region Roles
        public const string All = SystemAdmin + "," + Admin + "," + Worker + "," + Technician;
        public const string Admins = SystemAdmin + "," + Admin;
        public const string SystemAdmin = "Sistem Yöneticisi";
        public const string Admin = "Yönetici";
        public const string Worker = "Çalışan";
        public const string Technician = "Teknisyen";

        #endregion
    }
}