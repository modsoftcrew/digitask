﻿using DT.DAL;
using DT.DTO;
using DT.WEB.Models;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using MS.BLL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace DT.WEB
{
    [HubName("sessionHub")]
    public class SessionHub : Hub
    {
        public static List<User> users = new List<User>();
        public static List<User> Users { get { return users; } set { users = value; } }
        public static List<Messaging> messages = new List<Messaging>();
        public static List<Messaging> Messages { get { return messages; } set { messages = value; } }
        public void session_started(int user_id)
        {
            if (Users == null)
                Users = new List<User>();
            User user = Users.GetById(user_id);
            if (user == null)
            {
                user = StaticDB.Users.GetById(user_id);
                user.ConnectionId = Context.ConnectionId;
                Users.Add(user);
                Clients.Others.userConnected(user);
            }
            if(user.role_id == 1)
            {
                Clients.Caller.receiveOnlines(Users);
            }
        }
        public override Task OnConnected()
        {
            return base.OnConnected();
        }
        public override Task OnDisconnected(bool stopCalled)
        {
            User user = Users.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (user != null)
            {
                Users.Remove(user);
                Clients.Others.userDisconnected(user);
            }
            return base.OnDisconnected(stopCalled);
        }
        public override Task OnReconnected()
        {
            return base.OnReconnected();
        }

        public void sendMessage(int user_id, string message)
        {
            if (message != "")
            {
                Messaging messaging = new Messaging();
                messaging.user_id = user_id;
                messaging.User = StaticDB.Users.GetById(user_id);
                messaging.body = message;
                messaging.datetime = Zaman.Simdi;
                Messages.Add(messaging);

                Clients.Others.messageReceived(messaging);
            }
        }
    }
}