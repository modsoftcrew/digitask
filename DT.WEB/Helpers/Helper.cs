﻿using Newtonsoft.Json;
using DT.DTO;
using DT.WEB.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using MS.BLL;

namespace DT.WEB
{
    public static class Helper
    {
        public static MvcHtmlString GetBoolString(bool value)
        {
            if (value)
                return new MvcHtmlString("<i class=\"fas fa-check text-success\"></i>");
            return new MvcHtmlString("<i class=\"fas fa-times text-danger\"></i>");
        }
        public static string Theme(string defaultTheme = "MaterialCompact")
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["Theme"];
            if (cookie != null)
                return cookie.Value;
            else
            {
                return defaultTheme;
            }
        }

        public static string GetDisplayName<T>()
        {
            var displayName = typeof(T).GetCustomAttributes(typeof(DisplayNameAttribute), true).SingleOrDefault() as DisplayNameAttribute;

            if (displayName != null)
                return displayName.DisplayName;

            return "";
        }
        public static string GetDisplayName(this Type type)
        {
            var displayName = type.GetCustomAttributes(typeof(DisplayNameAttribute), true).SingleOrDefault() as DisplayNameAttribute;

            if (displayName != null)
                return displayName.DisplayName;

            return "";
        }
        public static string GetDisplayName(this PropertyInfo property)
        {
            var displayName = property.GetCustomAttribute(typeof(DisplayNameAttribute)) as DisplayNameAttribute;

            if (displayName != null)
                return displayName.DisplayName;

            return property.Name;
        }
        public static bool IsRequired(this PropertyInfo property)
        {
            object[] attrs = property.GetCustomAttributes(true);
            foreach (object attr in attrs)
            {
                RequiredAttribute reqAttr = attr as RequiredAttribute;
                if (reqAttr != null)
                    return true;
            }
            return false;
        }
        public static void Equalize(this object target, object source)
        {
            foreach (PropertyInfo prop in target.GetType().GetProperties().Where(p => !p.GetMethod.IsVirtual && p.Name != "created_date" && p.Name != "modified_date" && p.Name != "is_active" && p.Name != "details"))
            {
                var value = source.GetType().GetProperty(prop.Name).GetValue(source);
                target.GetType().GetProperty(prop.Name).SetValue(target, value);
            }
        }

        public static bool ValidateActionParameters(params int[] parameters)
        {
            foreach (int parameter in parameters)
            {
                if (parameter <= 0)
                    return false;
            }
            return true;
        }
    
        
        public static string CreateVisitorLog(HttpRequestBase request)
        {
            VisitorLog visitorLog = new VisitorLog();
            visitorLog.http_user_agent = request.ServerVariables["HTTP_USER_AGENT"];
            visitorLog.remote_addr = request.ServerVariables["REMOTE_ADDR"];
            visitorLog.remote_host = request.ServerVariables["REMOTE_HOST"];
            visitorLog.request_method = request.ServerVariables["REQUEST_METHOD"];
            visitorLog.server_name = request.ServerVariables["SERVER_NAME"];
            visitorLog.server_port = request.ServerVariables["SERVER_PORT"];
            visitorLog.server_software = request.ServerVariables["SERVER_SOFTWARE"];
            visitorLog.page_url = request.ServerVariables["URL"];
            visitorLog.controller = request.RequestContext.RouteData.Values["controller"].ToString();
            visitorLog.action = request.RequestContext.RouteData.Values["action"].ToString();
            visitorLog.DateCreated = DateTime.Now;
            return visitorLog.ToJson();
        }
    }

}