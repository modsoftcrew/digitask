
$(function () {
    //centering function
    jQuery.fn.center = function () {
        this.css("position", "absolute");
        this.css("top", (($(window).height() - this.outerHeight() - 256) / 2) + "px");
        this.css("left", (($(window).width() - this.outerWidth() - 256) / 2) + "px");
        return this;
    }
    universalPreloader();
});
//function that handles the universal preloader positioning and alignment
function universalPreloader() {
    //center to the screen
    $(".preloader").center();
    //run each time user resizes window
    $(window).resize(function () {
        $(".preloader").center();
    });
}

//function that disposes the universal preloader when everything is loaded; called on window.load event
function RemovePreloader(){
		
	var parentD = $("#universal-preloader");
	var pre = $("#universal-preloader>div");
	
	//when the logo and ajax-loader fades out, fade out the curtain; when that completes, remove the universal preloader from the DOM
	pre.animate({opacity:'0'},{duration:300, complete:function(){
	
		parentD.animate({opacity:'0'},{duration:300, complete:function(){
		
		    parentD.css('z-index', '-999999');
		
		}});
																																		 
	}});
	
	
}
function ShowPreloader() {
    var parentD = $("#universal-preloader");
    var pre = $("#universal-preloader>div");
    parentD.css('z-index', '999999');
    pre.css('opacity', '1');
    parentD.css('opacity', '0.9');
}