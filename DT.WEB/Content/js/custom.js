﻿var Credentials = {
    Server: decodeURI($.cookie('DT_ServerUrl')),
    ApiServer: decodeURI($.cookie('DT_ApiServerUrl')),
    User: decodeURI(JSON.parse($.cookie('DT_User'))),
    Language: decodeURI(JSON.parse($.cookie('DT_Language')))
}
var ApiKey = "Basic " + btoa(unescape(encodeURIComponent(Credentials.User.mail)) + ":" + unescape(encodeURIComponent(Credentials.User.password)));
var PleaseChoose = {
    'tr': 'Lütfen Seçiniz',
    'en': 'Please Choose'
}
var RValues = new Object();

$(document).on('click', '[data-toggle="lightbox"]', function (event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});
function GetLocal(obj, key) {
    var v = obj[key];
    if (v)
        return v;
    else
        obj[0];
}
function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
jQuery(function () {
    console.log('Credentials : ' + JSON.stringify(Credentials));
    AwakeJSEvents();

    jQuery('.dismiss-modal').click(function () {
        jQuery('#info-modal').modal('toggle');
    });

    //FillBucket();    
});


function ShineElement(elm, msg) {
    var currentTitle = $(elm).attr('title');
    if (msg) {
        $(elm).attr('title', msg);
        $(elm).tooltip();
        $(elm).tooltip('enable');
        PlaySound('../sounds/bubble.mp3');
    }
    $(elm).css('transform', 'scale(5)');
    $(elm).addClass('animate__animated animate__tada');
    setTimeout(function () {
        $(elm).css('transform', 'scale(1)');
        $(elm).attr('title', currentTitle);
    }, 2000);
}
function PlaySound(sound) {
    try {
        var soundStatus = get('dt_notifications');
        if (soundStatus == 'true' && interacted) {
            try {
                var audio = new Audio(sound);
                audio.play();
            } catch (e) {

            }
        }
    } catch (e) {

    }
}
//function AddToBucket(id, title) {
//    var bucketStr = localStorage.getItem('bucket');
//    console.log(bucketStr);
//    var bucketArray = JSON.parse(bucketStr);
//    var task = {
//        id: id,
//        title: title
//    }
//    bucketArray.push(task);
//    console.log(JSON.stringify(task));
//    localStorage.setItem('bucket', JSON.stringify(bucketArray));
//    console.log(JSON.stringify(bucketArray));
//    FillBucket();
//}
//function FillBucket() {
//    if (localStorage.getItem("bucket") == '')
//        localStorage.setItem('bucket', '[]');
//    else {
//        $('#seq-tasks-list').html('');
//        var bucketArray = JSON.parse(localStorage.getItem('bucket'));
//        console.log(JSON.stringify(bucketArray));
//        if (bucketArray.length > 0)
//            $('#seq-bucket').removeClass('hidden');
//        else
//            $('#seq-bucket').addClass('hidden');
//        $('.seq-items-count').html(bucketArray.length);
//        $('#seq-tasks-list').html('');
//        $('#seq-tasks-list').append('<span class="dropdown-item dropdown-header">' + bucketArray.length + ' Tasks</span>');
//        for (var i = 0; i < bucketArray.length; i++) {
//            var task = bucketArray[i];
//            $('#seq-tasks-list').append(
//                '<div class="dropdown-divider"></div>' +
//                '<a href="#" class="dropdown-item text-truncate">' +
//                '<i class="fas fa-envelope mr-2"></i> ' + task.title +
//                '<span class="float-right text-muted text-sm" data-id="' + task.id + '" onclick="RemoveFromBucket(this);"><i class="fa fa-trash"></i></span>' +
//                '</a>');
//        }
//    }
//}
//function RemoveFromBucket(elm) {
//    var id = $(elm).data('id');
//    var newBucket = [];
//    var bucketArray = JSON.parse(localStorage.getItem('bucket'));
//    for (var i = 0; i < bucketArray.length; i++) {
//        var task = bucketArray[i];
//        if (task.id != id) {
//            newBucket.push(task);
//        }
//    }
//    localStorage.setItem('bucket', JSON.stringify(newBucket));
//    FillBucket();
//}
function log(name, evt) {
    if (!evt) {
        var args = "{}";
    } else {
        var args = JSON.stringify(evt.params, function (key, value) {
            if (value && value.nodeName) return "[DOM node]";
            if (value instanceof $.Event) return "[$.Event]";
            return value;
        });
    }
    var $e = $("<li>" + name + " -> " + args + "</li>");
    console.log($e);
    $e.animate({ opacity: 1 }, 10000, 'linear', function () {
        $e.animate({ opacity: 0 }, 2000, 'linear', function () {
            $e.remove();
        });
    });
}

String.prototype.toHHMMSS = function () {
    var sec_num = parseInt(this, 10); // don't forget the second param
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours < 10) { hours = "0" + hours; }
    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }
    return hours + ':' + minutes + ':' + seconds;
}
Number.prototype.toHHMMSS = function () {
    var sec_num = parseInt(this, 10); // don't forget the second param
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours < 10) { hours = "0" + hours; }
    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }
    return hours + ':' + minutes + ':' + seconds;
}

const toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
});
function AwakeJSEvents() {
    $('[data-type="date"]').datetimepicker({
        useCurrent: true,
        locale: Credentials.Language.iso_code,
        format: 'L'
    });
    $('[data-type="datetime"]').datetimepicker({
        icons: {
            time: "fas fa-clock",
            date: "fas fa-calendar-alt"
        },
        useCurrent: false
    });
    $('[data-type="time"]').datetimepicker({
        format: 'LT'
    });
    $('[data-toggle="tooltip"]').tooltip();
    $('.disabled *').attr('disabled', 'disabled');
    $('.disabled *').attr('title', 'İlişkisel Veri');
    $(".jconfirm .dropdown-submenu").hover(function () {
        var dropdownMenu = $(this).children(".dropdown-menu");
        if (dropdownMenu.css('display') == 'none') {
            dropdownMenu.css('display', 'block');
        }
        else {
            dropdownMenu.css('display', 'none');
        }
    });
    jQuery(".input.number-only").bind({
        keydown: function (e) {
            var commaCount = (jQuery(this).val().match(new RegExp("/^-?\d*\.?\d*$/", "g")) || []).length;
            if (commaCount == 1 && e.which == 188)
                return false;
            if (e.shiftKey === true) {
                if (e.which == 9) {
                    return true;
                }
                return false;
            }
            if ((e.which > 95 && e.which < 106) || e.which == 110) {
                return true;
            }
            if (e.which == 188) {
                return true;
            }
            if (e.which > 57) {
                return false;
            }
            if (e.which == 32) {
                return false;
            }
            return true;
        }
    });
    jQuery('.select2').select2({
        allowClear: true,
        placeholder: PleaseChoose[Credentials.Language.iso_code],
        language: Credentials.Language.iso_code
    });
    jQuery('.jconfirm .select2').select2({
        allowClear: true,
        placeholder: PleaseChoose[Credentials.Language.iso_code],
        language: Credentials.Language.iso_code,
        dropdownParent: $('.jconfirm')
    });

    $('.select2-container').css('width', '100%');
    //jQuery('input.completion-value-input').focus(function () {
    //    console.log('Changing Completion Type');
    //    var id = $(this).data('id');
    //    $('input[data-toggle="' + id + '"]').prop('checked', true);
    //    $('small.completion-value-input').removeClass('hidden');
    //    $('small.completion-value-input').not('[data-id="' + id + '"]').addClass('hidden');
    //    $('input.completion-value-input').not('[data-id="exact-value"]').val('');
    //});
    $('.material-body img').click(function () {
        window.open($(this).attr('src'), '', 'width=640,height=480');
    });
    $('input[data-toggle="tagify"]')
        .tagify()
        .on('add', function (e, tagData) {
            console.log('added', tagData);
        });
    $(document).keydown(function (e) {
        if (e.ctrlKey) {
            if (e.keyCode == 32) {
                console.log('CTRL + SPACE Pressed');
                var activeTitle = $('.active.fg-title');
                if (activeTitle) {
                    var index = parseInt($(activeTitle).data('index'));
                    var nextIndex = index + 1;
                    if (nextIndex > $('.fg-titles').children().length - 1)
                        nextIndex = 0;
                    console.log('Next Index : ' + nextIndex);
                    $('.fg-title[data-index="' + nextIndex + '"]').trigger('click');;
                }
            } else if (e.keyCode == 18) {
                console.log('CTRL + ALT Pressed');
                var activeTitle = $('.active.fg-title');
                if (activeTitle) {
                    var index = parseInt($(activeTitle).data('index'));
                    var nextIndex = index - 1;
                    if (nextIndex < 0)
                        nextIndex = $('.fg-titles').children().length - 1;
                    console.log('Next Index : ' + nextIndex);
                    $('.fg-title[data-index="' + nextIndex + '"]').trigger('click');
                }
            }
        } else {
            if (e.keyCode == 13) {
                $(".jconfirm-buttons button:first-child").trigger('click');
            }
        }

        var soundStatus = get('dt_notifications');
        if (soundStatus == null)
            store('dt_notifications', true);
    });
    $('[data-close="modal"]').click(function () {
        $(this).closest('.modal').modal('hide');
    });

    $('.text-truncate').attr('title', $('.text-truncate').text().trim());


}
function CloseModal(elm) {
    var target = $(elm).data('target');
    $(target).parent('.modal').modal('toggle');
}
function isDescendant(parent, child) {
    var node = child.parentNode;
    while (node != null) {
        if (node == parent) {
            return true;
        }
        node = node.parentNode;
    }
    return false;
}
function SelectById(list, id) {
    var entity = {};
    jQuery.each(list, function (i, item) {
        if (item.id == id) {
            entity = item;
        }
    });
    return entity;
}
function FormatDate(data) {
    var num = parseFloat(data.split('(')[1]);
    var date = new Date(num);
    return date.toLocaleDateString();
}
function FormatTime(data) {
    var num = parseFloat(data.split('(')[1]);
    var date = new Date(num);
    return addZeroBefore(date.getHours()) + ':' + addZeroBefore(date.getMinutes());
}
function addZeroBefore(n) {
    return (n < 10 ? '0' : '') + n;
}
function ShowInfo(str, header) {
    $.confirm({
        title: !header ? 'Info' : header,
        content: str,
        draggable: true,
        dragWindowBorder: false,
        buttons: {
            ok: {
                text: 'OK',
                keys: ['enter', 'space', 'esc'],
                action: function () {

                }
            }
        }
    });
}
function ConfirmGoLink(title, content, btnCancel, btnOk, url) {
    $.confirm({
        title: title,
        theme: 'supervan',
        content: content,
        draggable: true,
        dragWindowBorder: false,
        buttons: {

            cancel: {
                text: btnCancel,
                keys: ['esc'],
                action: function () {

                }
            },
            ok: {
                text: btnOk,
                keys: ['enter', 'space'],
                action: function () {
                    location.href = url;
                }
            }
        }
    });
}
function ShowAlert(title, content, btnOk) {
    $.alert({
        title: title,
        theme: 'supervan',
        content: content,
        draggable: true,
        dragWindowBorder: false,
        buttons: {
            ok: {
                text: btnOk,
                keys: ['enter', 'space']
            }
        }
    });
}

function ShowMessage(title, btnStart, url) {
    $.alert({
        title: title,
        theme: 'supervan',
        draggable: true,
        dragWindowBorder: false,
        buttons: {
            start: {
                text: btnStart,
                keys: ['enter', 'space'],
                action: function () {
                    location.href = url;
                }
            }
        }
    });
}
function IsConfirm(str) {
    jQuery('#confirm-modal-body').html(str);
    jQuery('#confirm-modal').modal();
}

function ShowPassword(elm) {
    var title = $(elm).data('title');
    var label = $(elm).data('label');
    var user_id = $(elm).data('user_id');
    $.confirm({
        title: title,
        theme: 'supervan',
        content: '<div class="form-group">' +
            '<label>' + label + '</label>' +
            '<input type="text" class="form-control" required autofocus />' +
            '</div>',
        buttons: {
            ok: {
                text: 'OK',
                btnClass: 'btn-blue',
                action: function () {
                    var password = this.$content.find('input').val();
                    if (!password) {
                        $.alert('Enter Password');
                        return false;
                    }
                    if (password != Credentials.User.password) {
                        $.alert('Wrong Password!');
                        return;
                    }
                    else {
                        $('#drop-modal-' + user_id).modal('hide');
                        PartialShow(elm);
                    }
                }
            },
            cancel: {
                text: 'CANCEL',
            },
        },
        onContentReady: function (data, status, xhr) {
            this.$content.find('input').attr('type', 'password');
        }
    });
}
function SetProp(elm) {
    var url = $(elm).data('url');
    var user_id = $(elm).data('id');
    var property = $(elm).data('property');
    var value = $(elm).data('value');

    $.ajax({
        url: url,
        type: 'POST',
        data: {
            user_id: user_id,
            property: property,
            value: value
        },
        beforeSend: function () {
            $(elm).html('<i class="fa fa-spinner fa-spin"></i>');
        },
        success: function (data) {
            if (!data.Result) {
                if ($(elm).attr('checked'))
                    $(elm).removeAttr('checked');
                else
                    $(elm).attr('checked');
            } else {
                $(elm).data('value', value != 'True' ? 'True' : 'False');
            }
        },
        complete: function () {
            $(elm).html(content);
        },
        error: function () {
            if ($(elm).attr('checked'))
                $(elm).removeAttr('checked');
            else
                $(elm).attr('checked');
        }
    });
}

/**
 * Get a prestored setting
 *
 * @param String name Name of of the setting
 * @returns String The value of the setting | null
 */
function get(name) {
    if (typeof (Storage) !== 'undefined') {
        return localStorage.getItem(name)
    } else {
        window.alert('Please use a modern browser to properly view this template!')
    }
}

/**
 * Store a new settings in the browser
 *
 * @param String name Name of the setting
 * @param String val Value of the setting
 * @returns void
 */
function store(name, val) {
    if (typeof (Storage) !== 'undefined') {
        localStorage.setItem(name, val)
    } else {
        window.alert('Please use a modern browser to properly view this template!')
    }
}

function RemoveItemOnce(arr, value) {
    var index = arr.indexOf(value);
    if (index > -1) {
        arr.splice(index, 1);
    }
    return arr;
}

/**
 * Retrieve default settings and apply them to the template
 *
 * @returns void
 */
function SetTheme() {
    if (get('is_collapsed') == 'true') {
        console.log('Setting Theme - Collapsing');
        document.body.classList.add('sidebar-collapse');
    }
    $('.toggle-button').on("click", function () {
        console.log('Setting Theme - Toggle Button');
        store('is_collapsed', get('is_collapsed') != 'true');
    });
    jQuery.each(jQuery('.thm-gen-set'), function (i, elm) {
        var targetElm = jQuery(elm).data('target-element');
        var cls = jQuery(elm).data('related-class');
        var thmGenSetVal = get(jQuery(elm).attr('id'));
        console.log('Setting Theme - Target Element : ' + targetElm + ', Class : ' + cls + ', SetValue : ' + thmGenSetVal);
        jQuery(elm).prop("checked", thmGenSetVal == 'true');
        if (thmGenSetVal != 'false') {
            jQuery(targetElm).addClass(cls);
        }
    });

    // Add the change skin listener 
    jQuery('.thm-gen-set').on('change', function (e) {
        var targetElm = jQuery(this).data('target-element');
        var cls = jQuery(this).data('related-class');
        if (jQuery(this).prop("checked")) {
            jQuery(targetElm).addClass(cls);
            store(jQuery(this).attr('id'), true);
        } else {
            jQuery(targetElm).removeClass(cls);
            store(jQuery(this).attr('id'), false);
        }
    })

    SetNavBarColor(get('nav_bar_color'));
    jQuery('.navbar-color-changer').click(function () {
        var cls = jQuery(this).data('related-class');
        store('nav_bar_color', cls);
        SetNavBarColor(cls);
    });

    ClassReplacer('body', 'accent', get('accent_color'));
    jQuery('.accent-color-changer').click(function () {
        var cls = jQuery(this).data('related-class');
        store('accent_color', cls);
        ClassReplacer('body', 'accent', cls);
    });

    ClassReplacer('.main-sidebar', 'sidebar', get('sidebar_color'));
    jQuery('.sidebar-color-changer').click(function () {
        var cls = jQuery(this).data('related-class');
        if (!cls)
            cls = "sidebar-light-navy";
        store('sidebar_color', cls);
        ClassReplacer('.main-sidebar', 'sidebar', cls);
    });

    ClassReplacer('.brand-link', 'navbar', get('brand_color'));
    jQuery('.brand-color-changer').click(function () {
        var cls = jQuery(this).data('related-class');
        store('brand_color', cls);
        ClassReplacer('.brand-link', 'navbar', cls);
    });
}
function SetNavBarColor(cls) {
    console.log('Navbar : ' + cls);
    jQuery('.main-header').attr('class', 'main-header navbar navbar-expand ' + (cls == null ? 'navbar-light' : cls));
}
function ClassReplacer(target, key, cls) {
    var classList = jQuery(target).attr('class').split(' ');
    for (var i = 0; i < classList.length; i++) {
        var accentCls = classList[i];
        if (accentCls.startsWith(key))
            jQuery(target).removeClass(accentCls);
    }
    jQuery(target).addClass(cls);

    if (target == '.main-sidebar' && !cls) {
        jQuery(target).addClass('sidebar-light-navy');
    }
}
function setInputFilter(textbox, inputFilter) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function (event) {
        textbox.bind(event, function () {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            }
        });
    });
}


// Install input filters.
setInputFilter(jQuery('.int'), function (value) {
    return /^-?\d*jQuery/.test(value);
});
setInputFilter(jQuery('.u-int'), function (value) {
    return /^\d*jQuery/.test(value);
});
setInputFilter(jQuery('.percent'), function (value) {
    return /^\d*jQuery/.test(value) && (value === "" || parseInt(value) <= 100);
});
setInputFilter(jQuery('.float'), function (value) {
    return /^-?\d*[,]?\d*jQuery/.test(value) && (value === "" || parseInt(value) <= 99999);
});
setInputFilter(jQuery('.money'), function (value) {
    return /^-?\d*[,]?\d{0,2}jQuery/.test(value) && (value === "" || parseFloat(value) <= 9999);
});


function ServerClock(container, cur_hour, cur_min, cur_sec) {
    this.container = container;
    this.hours = cur_hour;
    this.minutes = cur_min;
    this.seconds = cur_sec;
}
ServerClock.prototype.run = function () {
    setInterval(this.update.bind(this), 1000);
};
ServerClock.prototype.update = function () {
    this.updateTime(1);
    var strHour = this.hours < 10 ? '0' + this.hours : this.hours;
    var strMin = this.minutes < 10 ? '0' + this.minutes : this.minutes;
    var strSec = this.seconds < 10 ? '0' + this.seconds : this.seconds;
    var strClock = strHour + ':' + strMin + ':' + strSec;
    $(this.container).html(strClock);
};
ServerClock.prototype.updateTime = function (secs) {
    this.seconds += secs;
    if (this.seconds >= 60) {
        this.minutes++;
        this.seconds = 0;
    }
    if (this.minutes >= 60) {
        this.hours++;
        this.minutes = 0;
    }
    if (this.hours >= 24) {
        this.hours = 0;
    }
};

var isPartialContinue = false;
var PartialElement;
var PartialModal;
var WillReload = false;
function PartialShow(elm, warning) {
    var cont = true;
    if (warning)
        cont = confirm(warning);
    if (!cont)
        return;
    var url = $(elm).data('url');
    ShowPartial(url, elm);
}

function ShowPartial(url, elm, reload) {
    if (isPartialContinue) {
        alert("Previous request is being continued. Please wait...");
        return;
    }
    if (reload)
        WillReload = true;
    else
        WillReload = false;
    isPartialContinue = true;
    var content = '';
    if (elm) {
        PartialModal = $(elm).data('modal');
        if (!PartialModal)
            PartialModal = $('#partial-modal');
        content = $(elm).html();
    } else {
        PartialModal = $('#partial-modal');
    }
    $.ajax({
        url: url,
        success: function (data) {
            $(PartialModal).html(data).modal({ backdrop: 'static', keyboard: false });
        },
        beforeSend: function () {
            if (elm)
                $(elm).html('<i class="fa fa-spinner fa-spin"></i>');
        },
        complete: function () {
            if (elm)
                $(elm).html(content);
            isPartialContinue = false;
        },
    });
}

function AddInPartial(elm) {
    if (isPartialContinue) {
        alert("Previous request is being continued. Please wait...");
        return;
    }
    isPartialContinue = true;
    var url = $(elm).data('url');
    var container = $($(elm).data('container'));
    $.ajax({
        url: url,
        success: function (data) {
            $(container).html(data);
        },
        beforeSend: function () {
            $(container).html('<i class="fa fa-spinner fa-spin"></i>');
        },
        complete: function () {
            isPartialContinue = false;
        },
    });
}


function UpdateBookmark(elm, title, ok) {
    var id = $(elm).data('id');
    var url = $(elm).data('url');
    var content = $(elm).html();
    $.ajax({
        type: "POST",
        url: url + '/' + id,
        success: function (data) {
            if (data['Result']) {
                if (data['Bookmarked']) {
                    $('.btn-fav[data-id="' + id + '"').html('<i class="fas fa-star text-yellow animate__animated animate__bounceIn"></i>');
                } else {
                    $('.btn-fav[data-id="' + id + '"').html('<i class="far fa-star animate__animated animate__fadeIn"></i>');
                }
            } else {
                ShowAlert(title, data.Message, ok);
            }
        },
        beforeSend: function () {
            $(elm).html('<i class="fa fa-spinner fa-spin"></i>');
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            ShowAlert(title, errorThrown, ok);
            $(elm).html(content);
        }
    });
}


$('.copylink').click(function () {
    var link = $(this).data('url');
    navigator.clipboard.writeText(link); toastr.success(link, $(this).data('message'));
});
function ShowQR(elm) {
    var link = $(elm).data('url');
    var modal = $(elm).data('modal');
    var title = $(elm).data('title');
    ShowModal(modal, link, title);
}
function ShowModal(modal, link, title) {
    var modalContent = '<div class="modal-dialog" id="dynamic-modal"><div class="modal-content"><div class="modal-header"><h4 class="modal-title" id="info-modal-title">'
        + title
        + '</h4><button type="button" class="close" onclick="CloseModal(this)" data-target="#dynamic-modal"><i class="fas fa-times"></i></button></div><div class="modal-body">' +
        '<div id="qr-code-img"></div>' +
        '</div></div></div>';
    $(modal).html(modalContent).modal();
    $('#qr-code-img').qrcode({
        text: link,
        size: 466,
        quiet: 1
    });
}

function PrintTask(elm) {
    var smartlink = $(elm).data('smartlink');
    var a = window.open(smartlink, '', 'height=800, width=1000');
    a.print();
}