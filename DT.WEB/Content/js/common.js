﻿
function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
function SelectById(list, id) {
    var entity = {};
    jQuery.each(list, function (i, item) {
        if (item.id == id) {
            entity = item;
        }
    });
    return entity;
}
function FormatDate(data) {
    var num = parseFloat(data.split('(')[1]);
    var date = new Date(num);
    return date.toLocaleDateString();
}
function FormatTime(data) {
    var num = parseFloat(data.split('(')[1]);
    var date = new Date(num);
    return addZeroBefore(date.getHours()) + ':' + addZeroBefore(date.getMinutes());
}
function addZeroBefore(n) {
    return (n < 10 ? '0' : '') + n;
}
function ShowInfo(str, header) {
    $.confirm({
        title: !header?'BİLGİ':header,
        content: str,
        draggable: true,
        dragWindowBorder: false,
        buttons: {
            ok: {
                text: 'TAMAM',
                keys: ['enter', 'space', 'esc'],
                action: function () {

                }
            }
        } 
    });
}
function ConfirmGoLink(title, content, btnCancel, btnOk, url) {
    $.confirm({
        title: title,
        theme: 'supervan',
        content: content,
        draggable: true,
        dragWindowBorder: false,
        buttons: {

            cancel: {
                text: btnCancel,
                keys: ['esc'],
                action: function () {

                }
            },
            ok: {
                text: btnOk,
                keys: ['enter', 'space'],
                action: function () {
                    location.href = url;
                }
            }
        }
    });
}
function ShowAlert(title, content, btnOk) {
    $.alert({
        title: title,
        theme: 'supervan',
        content: content,
        draggable: true,
        dragWindowBorder: false,
        buttons: {
            ok: {
                text: btnOk,
                keys: ['enter', 'space']
            }
        }
    });
}

function ShowMessage(title, btnStart,url) {
    $.alert({
        title: title,
        theme: 'supervan',
        draggable: true,
        dragWindowBorder: false,
        buttons: {
            start: {
                text: btnStart,
                keys: ['enter', 'space'],
                action: function () {
                    location.href = url;
                }
            }
        }
    });
}
function IsConfirm(str) {
    jQuery('#confirm-modal-body').html(str);
    jQuery('#confirm-modal').modal();
}

function ShowPassword(elm) {
    var title = $(elm).data('title');
    var label = $(elm).data('label');
    var user_id = $(elm).data('user_id');
    $.confirm({
        title: title,
        theme: 'supervan',
        content: '<div class="form-group">' +
                '<label>' + label + '</label>' +
                '<input type="text" class="form-control" required autofocus />' +
            '</div>',
        buttons: {
            ok: {
                text: 'OK',
                btnClass: 'btn-blue',
                action: function () {
                    var password = this.$content.find('input').val();
                    if (!password) {
                        $.alert('Enter Password');
                        return false;
                    }
                    if (password != Credentials.User.password) {
                        $.alert('Wrong Password!');
                        return;
                    }
                    else {
                        $('#drop-modal-' + user_id).modal('hide');
                        PartialShow(elm);
                    }
                }
            },
            cancel: {
                text: 'CANCEL',
            },
        },
        onContentReady: function (data, status, xhr) {
            this.$content.find('input').attr('type', 'password');
        }
    });
}
/**
 * Get a prestored setting
 *
 * @param String name Name of of the setting
 * @returns String The value of the setting | null
 */
function get(name) {
    if (typeof (Storage) !== 'undefined') {
        return localStorage.getItem(name)
    } else {
        window.alert('Please use a modern browser to properly view this template!')
    }
}

/**
 * Store a new settings in the browser
 *
 * @param String name Name of the setting
 * @param String val Value of the setting
 * @returns void
 */
function store(name, val) {
    if (typeof (Storage) !== 'undefined') {
        localStorage.setItem(name, val)
    } else {
        window.alert('Please use a modern browser to properly view this template!')
    }
}
function RemoveItemOnce(arr, value) {
    var index = arr.indexOf(value);
    if (index > -1) {
        arr.splice(index, 1);
    }
    return arr;
}


var isPartialContinue = false;
var PartialElement;
var PartialModal;
var WillReload = false;
function PartialShow(elm, warning) {
    var cont = true;
    if (warning)
        cont = confirm(warning);
    if (!cont)
        return;
    var url = $(elm).data('url');
    ShowPartial(url, elm);
}

function ShowPartial(url, elm, reload) {
    if (isPartialContinue) {
        alert("Previous request is being continued. Please wait...");
        return;
    }
    if (reload)
        WillReload = true;
    else
        WillReload = false;
    isPartialContinue = true;
    PartialModal = $(elm).data('modal');
    if (!PartialModal)
        PartialModal = $('#partial-modal');
    var content = $(elm).html();
    $.ajax({
        url: url,
        success: function (data) {
            $(PartialModal).html(data).modal({ backdrop: 'static', keyboard: false });
        },
        beforeSend: function () {
            $(elm).html('<i class="fa fa-spinner fa-spin"></i>');
        },
        complete: function () {
            $(elm).html(content);
            isPartialContinue = false;
        },
    });
}

function CloseModal(elm) {
    var target = $(elm).data('target');
    $(target).parent('.modal').modal('toggle');
}