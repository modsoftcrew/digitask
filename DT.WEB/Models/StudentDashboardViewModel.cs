﻿using DT.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DT.WEB
{
    public class StudentDashboardViewModel
    {
        public StudentDashboardViewModel()
        {
            this.Events = new List<EventModel>();
            this.Tasks = new List<StudentTaskModel>();
            this.Feeds = new List<RawPage>();
        }
        public List<EventModel> Events { get; set; }
        public List<StudentTaskModel> Tasks { get; set; }
        public List<RawPage> Feeds { get; set; }
    }
    public class EventModel
    {
        public int id { get; set; }
        public int task_id { get; set; }
        public string target_proficiency_level { get; set; }
        public string information_gap { get; set; }
        public string group_range { get; set; }
        public string target_ages { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string background_color { get; set; }
        public DateTime planned_start_time { get; set; }
        public DateTime planned_end_time { get; set; }

    }
}