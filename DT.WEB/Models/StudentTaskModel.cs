﻿using DT.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DT.WEB
{
    public class StudentTaskModel:_Task
    {
        public int session_id { get; set; }
        public string assigned_table { get; set; }
        public int? class_id { get; set; }
        public int? cluster_id { get; set; }
        public int? room_id { get; set; }

        public DateTime planned_start_time { get; set; }
        public DateTime? start_time { get; set; }
        public DateTime? planned_end_time { get; set; }
        public DateTime? end_time { get; set; }
        public long assigned_by { get; set; }
    }
}