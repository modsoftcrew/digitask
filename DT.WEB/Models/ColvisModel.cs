﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DT.WEB.Models
{
    public class ColvisModel
    {
        public ColvisModel(string title)
        {
            text = title;
            show = new List<int>();
            hide = new List<int>();
        }
        public string extend { get { return "colvisGroup"; } }
        public string text { get; set; }
        public List<int> show { get; set; }
        public List<int> hide { get; set; }

    }
}