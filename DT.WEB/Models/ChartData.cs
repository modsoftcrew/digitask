﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DT.WEB.Models
{

    public class ChartData
    {
        public ChartData(List<string> labels, List<Dataset> datas)
        {
            this.labels = labels;
            this.datasets = datas;
        }
        public List<string> labels { get; set; }
        public List<Dataset> datasets { get; set; }
    }
    public class Dataset
    {
        public Dataset()
        {

        }
        public Dataset(string label, List<double> data)
        {
            this.label = label;
            this.data = data;
        }
        public string label { get; set; }
        public string fillColor { get; set; }
        public string strokeColor { get; set; }
        public string pointColor { get; set; }
        public string pointStrokeColor { get; set; }
        public string pointHighlightFill { get; set; }
        public string pointHighlightStroke { get; set; }
        public List<double> data { get; set; }
    }
}