﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DT.WEB
{
    public class RecordInfoModel
    {
        public Nullable<long> id { get; set; }
        public Nullable<DateTime> started_date { get; set; }
        public Nullable<DateTime> completed_date { get; set; }
        public Nullable<DateTime> reported_date { get; set; }
        public Nullable<DateTime> archieved_date { get; set; }
        public Nullable<DateTime> deleted_date { get; set; }
        public string status { get; set; }
        public Nullable<int> parameters_count { get; set; }
        public Nullable<int> required_parameters_count { get; set; }
        public Nullable<int> filled_parameters_count { get; set; }
        public Nullable<int> filled_required_parameters_count { get; set; }
        public Nullable<int> remain_parameters_count { get; set; }
        public Nullable<int> remain_required_parameters_count { get; set; }
    }
}