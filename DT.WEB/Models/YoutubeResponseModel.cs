﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DT.WEB.Models
{

    public class YoutubeResponseModel
    {
        public string kind { get; set; }
        public string etag { get; set; }
        public List<Item> items { get; set; }
        public PageInfo pageInfo { get; set; }
    }
    public class Status
    {
        public string uploadStatus { get; set; }
        public string privacyStatus { get; set; }
        public string license { get; set; }
        public bool embeddable { get; set; }
        public bool publicStatsViewable { get; set; }
        public bool madeForKids { get; set; }
    }

    public class Item
    {
        public string kind { get; set; }
        public string etag { get; set; }
        public string id { get; set; }
        public Status status { get; set; }
    }

    public class PageInfo
    {
        public int totalResults { get; set; }
        public int resultsPerPage { get; set; }
    }
}