﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DT.WEB.Models
{
    public class SearchResultModel
    {
        public string type { get; set; }
        public string title { get; set; }
        public string details { get; set; }
        public DateTime created_date { get; set; }
    }
}