﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DT.WEB.Models
{
    public class TimelineModel
    {
        public TimelineModel()
        {
        }
        public string Table { get; set; }
        public string Icon { get; set; }
        public string Color { get; set; }
        public object Object { get; set; }
        public string Details { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}