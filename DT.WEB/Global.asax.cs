using Microsoft.AspNet.SignalR;
using MS.BLL;
using Newtonsoft.Json;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace DT.WEB
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private Exception exception;
        protected void Application_Start()
        {
            WebUtils.FillStaticDB();
            GlobalConfiguration.Configuration.MessageHandlers.Add(new XHttpMethodDelegatingHandler());
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);


            var serializerSettings = new JsonSerializerSettings();
            serializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Serialize;
            serializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.Objects;

            var serializer = JsonSerializer.Create(serializerSettings);
            GlobalHost.DependencyResolver.Register(typeof(JsonSerializer), () => serializer);
        }
        protected void Application_Error(object sender, EventArgs e)
        {
            exception = Server.GetLastError();            
            var httpContext = ((HttpApplication)sender).Context;
            httpContext.Response.Clear();
            httpContext.ClearError();

            if (new HttpRequestWrapper(httpContext.Request).IsAjaxRequest())
            {
                DT.DAL.Logger.Save("AJAX Hatas�", exception);
                return;
            }
            else
            {
                DT.DAL.Logger.Save("Genel Hata", exception);
            }
        }
        public class XHttpMethodDelegatingHandler : DelegatingHandler
        {
            private static readonly string[] _allowedHttpMethods = { "PUT", "DELETE" };
            private static readonly string _httpMethodHeader = "X-HTTP-Method";

            protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {
                if (request.Method == HttpMethod.Post && request.Headers.Contains(_httpMethodHeader))
                {
                    string httpMethod = request.Headers.GetValues(_httpMethodHeader).FirstOrDefault();
                    if (_allowedHttpMethods.Contains(httpMethod, StringComparer.InvariantCultureIgnoreCase))
                        request.Method = new HttpMethod(httpMethod);
                }
                return base.SendAsync(request, cancellationToken);
            }
        }
    }
}
