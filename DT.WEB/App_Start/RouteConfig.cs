﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DT.WEB
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            #region FrontRouting
            routes.MapRoute("open-preview", "open-preview", new { controller = "Front", action = "TaskPreview" });
            #endregion        

            #region Account Manager  Routing
            routes.MapRoute("open", "open", new { controller = "Account", action = "Start" });
            routes.MapRoute("check-mail-availability", "check-mail-availability", new { controller = "Account", action = "CheckMailIsAvailable" });
            routes.MapRoute("sign-in", "sign-in", new { controller = "Account", action = "Login" });
            routes.MapRoute("register", "register", new { controller = "Account", action = "Register" });
            routes.MapRoute("forgot-password", "forgot-password", new { controller = "Account", action = "ForgotPassword" });
            routes.MapRoute("reset-link", "reset-link", new { controller = "Account", action = "ResetPassword" });
            routes.MapRoute("change-pass", "change-pass", new { controller = "ChangePassword", action = "ResetPassword" });
            routes.MapRoute("sign-out", "sign-out", new { controller = "Account", action = "Logout" });
            routes.MapRoute("clear-statics", "clear-statics", new { controller = "Account", action = "CleanStatic" });
            routes.MapRoute("verify-email", "verify-email", new { controller = "Account", action = "VerifyMail" });
            routes.MapRoute("subscription", "subscription", new { controller = "Account", action = "Subscription" });
            #endregion        

            #region Home Routing
            routes.MapRoute("homepage", "homepage", new { controller = "Home", action = "Index" });
            routes.MapRoute("error-page", "error-page", new { controller = "Home", action = "ErrorPage" });
            routes.MapRoute("server-error", "server-error", new { controller = "Home", action = "InternalServerError" });
            routes.MapRoute("change-lang", "change-lang", new { controller = "Home", action = "ChangeLanguage" });
            routes.MapRoute("clear-buffer", "clear-buffer", new { controller = "Home", action = "CleanStatic" });
            routes.MapRoute("send-error-report", "send-error-report", new { controller = "Home", action = "SendReport" });
            routes.MapRoute("get-help", "get-help", new { controller = "Home", action = "GetHelp" });
            #endregion

            #region Interactive
            routes.MapRoute("session", "session", new { controller = "SessionManagement", action = "Start" });
            #endregion

            //routes.MapRoute("DefaultLocalized",
            //    "{language}-{culture}/{controller}/{action}/{id}",
            //    new
            //    {
            //        controller = "Home",
            //        action = "Index",
            //        id = "",
            //        language = "en",
            //        culture = "EN"
            //    });
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
