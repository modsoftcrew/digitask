using System;
namespace DT.WEB
{
    public partial class BaseDataModel
    {
        public string attributes { get; set; }
        public string details { get; set; }
        public DateTime created_date { get; set; }
        public long created_by { get; set; }
        public DateTime modified_date { get; set; }
        public long modified_by { get; set; }
        public bool is_active { get; set; }
    }
}
