﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DT.WEB
{
    public class MailModel
    {
        public MailModel(string to, string subject, string body, bool isHtml)
        {
            To = to;
            Subject = subject;
            Body = body;
            IsHtml = isHtml;
        }

        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsHtml { get; set; }
    }
}
