﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DT.WEB
{
    public class NotificationModel
    {
        public NotificationModel()
        {
        }

        public NotificationModel(string title, string body, string topic, long creator)
        {
            Title = title;
            Body = body;
            Topic = topic;
            Creator = creator;
        }

        public string Title { get; set; }
        public string Body { get; set; }
        public string Topic { get; set; }
        public long Creator { get; set; }
    }
}
